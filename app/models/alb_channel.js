const mongoose = require('mongoose');

const alb_channelSchema = mongoose.Schema({

    channelName: { type: String, required: true },
    RuleArn: {type: String},
    TargetName: {type: String},
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

alb_channelSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('alblisting', alb_channelSchema, 'alblisting');