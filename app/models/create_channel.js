const mongoose = require('mongoose');

const ChannelCreationSchema = mongoose.Schema({

    channelName: { type: String, required: true },
    creator: { type: String, required: true },
    lookaheadFragments: { type: String, required: true },
    dvrWindowLength: { type: String, required: true },
    archiveSegmentLength: { type: String, required: true },
    archiving: { type: String, required: true },
    archiveLength: { type: String, required: true },
    restartOnEncoderReconnect: { type: String },
    issMinimumFragmentLength: { type: String },
    hlsMinimumFragmentLength: { type: String },
    flag: { type: String },
    terminationPolicy: { type: String },
    hlsNoAudioOnly : {type: String},
    url: { type: String },
    state :{type: String},
    ServerIp: { type: String },
    RuleArn_http: {type: String},
    RuleArn_https:{type: String },
   // channelpriority :{ type: String},
    serverName: { type: String },
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

ChannelCreationSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('primarylistingchannel', ChannelCreationSchema, 'primarylistingchannel');