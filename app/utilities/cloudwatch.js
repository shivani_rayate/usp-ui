var AWS = require('aws-sdk');

const awsConfig = require('../../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

var cloudwatch = new AWS.CloudWatch();

exports.getDashboard = (paramsReq) => {
    return new Promise((resolve, reject) => {
        var params = {
            DashboardName: 'USP-Backup' /* required */
        }
        cloudwatch.getDashboard(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                reject(err)
            }
            else {
                console.log("getDashboard is " + JSON.stringify(data));
                resolve(data)
            }
        }
        )
    });
}


exports.getMetricWidgetImage = (paramsReq) => {
    return new Promise((resolve, reject) => {
        const params = {

            MetricWidget: '{ "metrics": [ [ "AWS/EC2", "CPUUtilization", "InstanceId", "i-0ef13f6dcdf9bd05f", { "id": "m1", "stat": "p50", "label": "Median value", "visible": true, "color": "#dddddd", "yAxis": "left", "period": 300 } ], [ ".", ".", ".", ".", { "id": "m2", "stat": "Average", "label": "Average value", "visible": true, "color": "#cccccc", "yAxis": "left", "period": 300 } ], [ { "expression": "(m1+m2)/2", "id": "e1", "label": "Half way between average and median", "visible": true, "color": "#000000", "yAxis": "left" } ], [ { "expression": "RATE(e1)", "yAxis": "right", "label": "rate of change of the half way point" } ] ] }',
            OutputFormat: 'image/png'


        };
        cloudwatch.getMetricWidgetImage(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                reject(err)
            }
            else {
                //  console.log("getMetricWidgetImage is " + JSON.stringify(data));
                resolve(data)
            }
        }
        )
    });
}


exports.getMetricStatistics = (paramsReq) => {
    return new Promise((resolve, reject) => {
        var params = {
            EndTime: new Date, /* required */
            MetricName: 'NetworkOut', /* required */
            Namespace: 'AWS/EC2', /* required */
            Period: '3600', /* required */
            StartTime: '2020-03-01T23:18:00Z', /* required */
            Dimensions: [
                {
                    Name: 'InstanceId', /* required */
                    Value: 'i-0ef13f6dcdf9bd05f' /* required */
                },
                /* more items */
            ],
            Statistics: [
                'Average',
                /* more items */
            ],
            Unit: 'Bytes'

        };
        cloudwatch.getMetricStatistics(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                reject(err)
            }
            else {
                console.log("getMetricStatistics is " + JSON.stringify(data));
                resolve(data)
            }
        }
        )
    });
}


exports.getMetricData = (paramsReq) => {
    return new Promise((resolve, reject) => {
        var params = {
            EndTime: new Date, /* required */
            MetricDataQueries: [ /* required */
                {
                    Id: 'myRequest', /* required */

                    MetricStat: {
                        Metric: { /* required */
                            Dimensions: [
                                {
                                    Name: 'InstanceId', /* required */
                                    Value: 'i-0ef13f6dcdf9bd05f' /* required */
                                },
                                /* more items */
                            ],
                            MetricName: 'CPUUtilization',
                            Namespace: 'AWS/EC2'
                        },
                        Period: '3600', /* required */
                        Stat: 'p10', /* required */
                        Unit: 'Percent'
                    },

                },
                /* more items */
            ],
            StartTime: '2020-03-01T23:18:00Z', /* required */

            ScanBy: 'TimestampDescending'
        };




        cloudwatch.getMetricData(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                reject(err)
            }
            else {
                console.log("getMetricData is " + JSON.stringify(data));
                resolve(data)
            }
        }
        )
    });
}

exports.getMetricDataNetworkIn = (paramsReq) => {
    return new Promise((resolve, reject) => {
        var params = {
            EndTime: new Date, /* required */
            MetricDataQueries: [ /* required */
                {
                    Id: 'myRequest', /* required */

                    MetricStat: {
                        Metric: { /* required */
                            Dimensions: [
                                {
                                    Name: 'InstanceId', /* required */
                                    Value: 'i-0ef13f6dcdf9bd05f' /* required */
                                },
                                /* more items */
                            ],
                            MetricName: 'NetworkIn',
                            Namespace: 'AWS/EC2'
                        },
                        Period: '3600', /* required */
                        Stat: 'p50', /* required */
                        Unit: 'Bytes'
                    },

                },
                /* more items */
            ],
            StartTime: '2020-03-01T23:18:00Z', /* required */

            ScanBy: 'TimestampDescending'
        };




        cloudwatch.getMetricData(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                reject(err)
            }
            else {
                console.log("getMetricDataNetworkIn is " + JSON.stringify(data));
                resolve(data)
            }
        }
        )
    });
}



exports.getMetricDataNetworkOut = (paramsReq) => {
    return new Promise((resolve, reject) => {
        var params = {
            EndTime: new Date, /* required */
            MetricDataQueries: [ /* required */
                {
                    Id: 'myRequest', /* required */

                    MetricStat: {
                        Metric: { /* required */
                            Dimensions: [
                                {
                                    Name: 'InstanceId', /* required */
                                    Value: 'i-0ef13f6dcdf9bd05f' /* required */
                                },
                                /* more items */
                            ],
                            MetricName: 'NetworkOut',
                            Namespace: 'AWS/EC2'
                        },
                        Period: '3600', /* required */
                        Stat: 'p50', /* required */
                        Unit: 'Bytes'
                    },

                },
                /* more items */
            ],
            StartTime: '2020-03-01T23:18:00Z', /* required */

            ScanBy: 'TimestampDescending'
        };




        cloudwatch.getMetricData(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                reject(err)
            }
            else {
                console.log("getMetricDataNetworkOut is " + JSON.stringify(data));
                resolve(data)
            }
        }
        )
    });
}