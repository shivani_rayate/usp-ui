// Load the SDK for JavaScript
var AWS = require('aws-sdk');

const awsConfig = require('../../config/aws_config')[ENV];

exports.instanceListing = reqParams => new Promise((resolve, reject) => {

  // Set config 
  AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region
  });


  // Create EC2 service object
  var ec2 = new AWS.EC2();

  ec2.describeInstances(reqParams, (err, data) => {
    // console.log("\nIn describe instances:\n");
    // console.log(JSON.stringify(data))

    if (err) {
      console.log(err, err.stack); // an error occurred
      reject(err); // an error occurred
    }
    else {

      //  console.log("\n\n" + JSON.stringify(data) + "\n\n"); // successful response

      resolve(data);
    }
  });

})



exports.Channelslisting = reqParams => new Promise((resolve, reject) => {

  // Set config 
  AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region
  });
  // Create MediaConvert service object
  var medialive = new AWS.MediaLive();
  medialive.listChannels(reqParams, (err, data) => {
    // console.log("\nList Of channels:\n");
    if (err) {
      console.log(err, err.stack); // an error occurred
      reject(err); // an error occurred
    }
    else {
      // console.log("\n\n" + JSON.stringify(data) + "\n\n"); // successful response

      resolve(data);
    }


  });
})


exports.Connectlisting = reqParams => new Promise((resolve, reject) => {

  // Set config 
  AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region
  });
  // Create MediaConnect service object
  var mediaconnect = new AWS.MediaConnect();
  var params = {
  };
  mediaconnect.listFlows(params, function (err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else {
      // console.log(data);   
      resolve(data);        // successful response
    }
  });

});


exports.instancemanagment = reqParams => new Promise((resolve, reject) => {



  // Set config 
  AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region
  });

  //EC2 Start and stop service object

  var ec2 = new AWS.EC2();
  var flag = reqParams.flag;
  var instaneId = reqParams.id;

  var _params = {
    InstanceIds: [ /* required */
      instaneId,
      /* more items */
    ]
  };

  console.log("FLAAGGGGGGGGG For Start  ##### " + flag)

  if (flag === 'false') {

    console.log("FLAAGGGGGGGGG IN IF  ##### " + flag)

    console.log("IM HERE NOW CAME TO START ################,")

    ec2.startInstances(_params, (err, data) => {
      console.log("\nIn Start describe instances:\n");
      if (err) {
        console.log(err, err.stack); // an error occurred
        reject(err); // an error occurred
      }
      else {
        //    console.log("\n\n" + JSON.stringify(data) + "\n\n"); // successful response

        resolve(data);
      }
    })
  }

  else {

    console.log("FLAAGGGGGGGGG For Stop  ##### " + flag)

    console.log("FLAAGGGGGGGGG IN ELSE  ##### " + flag)

    console.log("IM HERE NOW CAME TO STOP ################,")
    ec2.stopInstances(_params, (err, data) => {
      console.log("\nIn Stop describe instances:\n");
      if (err) {
        console.log(err, err.stack); // an error occurred
        reject(err); // an error occurred
      }
      else {
        //      console.log("\n\n" + JSON.stringify(data) + "\n\n"); // successful response

        resolve(data);
      }
    })
  }

})





//Media Connect
exports.connectmanagment = reqParams => new Promise((resolve, reject) => {

  console.log(JSON.stringify(reqParams))
  // Set config 
  AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region
  });

  //Media Connect Start & Stop
  var mediaconnect = new AWS.MediaConnect();
  var FlowArn = reqParams.FlowArn;
  var flag = reqParams.flag;

  var params = {
    FlowArn: FlowArn /* required */
  };

  console.log("FLAAGGGGGGGGG   ##### " + flag)

  if (flag === 'false') {

    console.log("FLAAGGGGGGGGG IN IF  ##### " + flag)

    console.log("IM HERE NOW CAME TO START Media Connect ################,")

    mediaconnect.startFlow(params, function (err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
        reject(err); // an error occurred
      }
      else {
        console.log("\n\n" + JSON.stringify(data) + "\n\n"); // successful response

        resolve(data);
      }
    })
  }

  else {

    console.log("FLAAGGGGGGGGG IN ELSE  ##### " + flag)

    console.log("IM HERE NOW CAME TO STOP Media Connect ################,")
    mediaconnect.stopFlow(params, function (err, data) {
      console.log("\nIn Stop describe Media Connect:\n");
      if (err) {
        console.log(err, err.stack); // an error occurred
        reject(err); // an error occurred
      }
      else {
        console.log("\n\n" + JSON.stringify(data) + "\n\n"); // successful response

        resolve(data);
      }
    })
  }

})



//Media Live Channel

exports.channelmanagment = reqParams => new Promise((resolve, reject) => {


  // Set config 
  AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region
  });

  //Media Live Start and stop service object

  var medialive = new AWS.MediaLive();
  var flag = reqParams.flag;
  var ChannelId = reqParams.ChannelId;

  console.log(JSON.stringify(flag))

  var params = {
    ChannelId: ChannelId /* required */
  };



  console.log("FLAAGGGGGGGGG   ##### " + flag)

  if (flag === 'false') {

    console.log("FLAAGGGGGGGGG IN IF  ##### " + flag)

    console.log("IM HERE NOW CAME TO START MediaLive Channel ################,")

    medialive.startChannel(params, function (err, data) {
      console.log("\nIn Start describe MediaLive Channel:\n");
      if (err) {
        console.log(err, err.stack); // an error occurred
        reject(err); // an error occurred
      }
      else {
        //  console.log("\n\n" + JSON.stringify(data) + "\n\n"); // successful response

        resolve(data);
      }
    })
  }

  else {

    console.log("FLAAGGGGGGGGG IN ELSE  ##### " + flag)

    console.log("IM HERE NOW CAME TO STOP MediaLive################,")
    medialive.stopChannel(params, function (err, data) {
      console.log("\nIn Stop describe MediaLive:\n");
      if (err) {
        console.log(err, err.stack); // an error occurred
        reject(err); // an error occurred
      }
      else {
        console.log("\n\n" + JSON.stringify(data) + "\n\n"); // successful response

        resolve(data);
      }
    })
  }

})
