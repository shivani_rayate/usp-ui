var AWS = require('aws-sdk');

const awsConfig = require('../../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

var route53 = new AWS.Route53();


exports.listResourceRecordSets = (paramsReq) => {
    return new Promise((resolve, reject) => {
        var params = {
            HostedZoneId: 'Z05038341AXC7HWK55NB', /* required */
           StartRecordName: 'suntv-b.skandha.tv',
           StartRecordType:  'CNAME',
           MaxItems: '2'
          };
        route53.listResourceRecordSets(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                reject(err)
            }
            else {
                console.log("listResourceRecordSets is " + JSON.stringify(data));
                resolve(data)
            }
        }
        )
    });
}



exports.changeResourceRecordSets = (paramsReq) => {


    console.log("paramsReq changeResourceRecordSets >> "+JSON.stringify(paramsReq))
    return new Promise((resolve, reject) => {
        var params = {
            ChangeBatch: {
            Changes: [{
                Action: "UPSERT",

               
                ResourceRecordSet: {

                    ResourceRecords: [{
                        Value: paramsReq.ResourceRecordValue
                    }],
                    SetIdentifier: paramsReq.SetIdentifier ,
                    Name :  paramsReq.name,
                    Type : paramsReq.Type,
                    TTL: paramsReq.TTL,
                    Weight : paramsReq.weight,
                }
                 }
            ],},
            HostedZoneId: "Z05038341AXC7HWK55NB"
          
          };

       
        route53.changeResourceRecordSets(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
                reject(err)
            }
            else {
                console.log("listResourceRecordSets is " + JSON.stringify(data));
                resolve(data)
            }
        }
        )
    });
}