const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
const AWS = require('aws-sdk');
const request = require('request');
const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');
global.fetch = require('node-fetch');

// ####################################################
global.navigator = () => null;  // VIMP SOLUTION on >>>  navigator not defined error
// ####################################################

const awsConfig = require('../../../config/aws_config')['development'];


AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});




const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

const poolData = {
    UserPoolId: awsConfig.COGNITO.UserPoolId, // Your user pool id here    
    ClientId: awsConfig.COGNITO.ClientId // Your client id here
};
//console.log('poolData >>>>>> ' + JSON.stringify(poolData))
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

// Authenticate and Set a New Password for a User Created with the AdminCreateUser
// exports.AuthenticateUser = (paramsReq) => {

//     return new Promise((resolve, reject) => {

//         var params = {
//             username: paramsReq.username,
//             password: paramsReq.password,
//             newPassword: paramsReq.newPassword
//         }

//         console.log('params >>>>>> ' + JSON.stringify(params))

//         var authenticationData = {
//             Username: params.username,
//             Password: params.password,
//         };
//         console.log('authenticationData >>>>>> ' + JSON.stringify(authenticationData))

//         var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
//         console.log('authenticationDetails >>>>>> ' + JSON.stringify(authenticationDetails))

//         var userData = {
//             Username: params.username,
//             Pool: userPool
//         };
//         console.log('userData >>>>>> ' + JSON.stringify(userData))

//         var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
//         console.log('userData >>>>>> ' + JSON.stringify(cognitoUser))

//         var attributesData = {
//             email: params.username
//         }
//         console.log('attributesData >>>>>> ' + JSON.stringify(attributesData))

//         cognitoUser.authenticateUser(authenticationDetails, {
//             onSuccess: function (result) {
//                 // User authentication was successful
//                 console.log("######## onSuccess #########") //
//                 console.log('access token + ' + result.getAccessToken().getJwtToken());
//                 console.log('idToken + ' + result.idToken.jwtToken);// User authentication was successful

//                 let obj = {};
//                 obj.status = 200;
//                 obj.message = 'User Authentication Successed';
//                 obj.data = result;
//                 resolve(obj)
//             },

//             onFailure: function (err) {
//                 console.log("######## onFailure #########") //
//                 console.log(err) // User authentication was not successful
//                 let obj = {};
//                 obj.status = 403;
//                 obj.message = err.message;
//                 obj.data = null;
//                 resolve(obj)
//             },

//             newPasswordRequired: function (userAttributes, requiredAttributes) {
//                 console.log("######## newPasswordRequired #########") //
//                 cognitoUser.completeNewPasswordChallenge(params.newPassword, attributesData, this)

//             }
//         })
//     })
// }


// RegisterUser
exports.RegisterUser = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var params = {
            UserPoolId: awsConfig.COGNITO.UserPoolId, /* required */
            Username: paramsReq.email, /* required */
            TemporaryPassword: paramsReq.password,
            DesiredDeliveryMediums: [
                'EMAIL'
            ],
            ForceAliasCreation: false,
            // MessageAction: 'SUPPRESS',
            UserAttributes: [
                {
                    Name: 'email', /* required */
                    Value: paramsReq.email
                },
                {
                    Name: 'name', /* required */
                    Value: paramsReq.name
                }
                /* more items */
            ]
        };

        cognitoidentityserviceprovider.adminCreateUser(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            } else {
                console.log('user data is ' + JSON.stringify(data));
                resolve(data)
            }
        });
    })
};


// ConfirmSignUp
exports.confirmSignUp = (paramsReq) => {
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    return new Promise((resolve, reject) => {
        var params = {
            ClientId: awsConfig.COGNITO.ClientId, /* required */
            ConfirmationCode: paramsReq.ConfirmationCode, /* required */
            Username: paramsReq.Username, /* required */

            // AnalyticsMetadata: {
            //     AnalyticsEndpointId: 'STRING_VALUE'
            // },
            // ClientMetadata: {
            // },
            // ForceAliasCreation: true || false,
            // SecretHash: 'STRING_VALUE',
            // UserContextData: {
            //     EncodedData: 'STRING_VALUE'
            // }

        };
        cognitoidentityserviceprovider.confirmSignUp(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            }
            else {
                console.log(data);           // successful response
                resolve(data)
            }
        });
    })

};




// Login
exports.Login = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
            Username: paramsReq.username,
            Password: paramsReq.password,
        });
        var userData = {
            Username: paramsReq.username,
            Pool: userPool
        };
        var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {

                //console.log('result >>>  + ' + JSON.stringify(result));
                // console.log('access token + ' + result.getAccessToken().getJwtToken());
                // console.log('id token + ' + result.getIdToken().getJwtToken());
                // console.log('refresh token + ' + result.getRefreshToken().getToken());
                let obj = {};
                obj.status = 200;
                obj.message = 'User Authentication Successed';
                obj.data = result;
                resolve(obj)
            },
            onFailure: function (err) {
                console.log(`Login > onFailure > ` + err);
                if (err.message === `Only radix 2, 4, 8, 16, 32 are supported`) {
                    console.log(`Login > RADIX BLOCK `)
                    err.message = `Incorrect username or password.`
                }
                let obj = {};
                obj.status = 403;
                obj.message = err.message;
                obj.data = null;
                resolve(obj)
            },

        });
    })
};



var elbv2 = new AWS.ELBv2();

// Albrule
exports.Albrule = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var params = {
            Actions: [
                {
                    TargetGroupArn: paramsReq.primary_target_arn,
                    Type: "forward"
                }
            ],
            Conditions: [
                {
                    Field: "path-pattern",
                    Values: [
                        `/${paramsReq.channelName}/*`
                    ]
                }
            ],
            ListenerArn: paramsReq.primary_listner_arn,
            Priority: paramsReq.channelpriority
        };

        elbv2.createRule(params, function (err, data) {

            if (err) {
                console.log(err, err.stack);// an error occurred

            } else {
               // console.log(JSON.stringify(data));
                resolve(data)
            }
        }
        )
    })
}



   exports.DeleteRuleARN = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var params = {
            RuleArn:   paramsReq.RuleArn 
             };

           elbv2.deleteRule(params, function(err, data) {
             if (err) {
                 console.log(err, err.stack);// an error occurred
              } 
             else   {
                console.log(JSON.stringify(data));    
                resolve(data)       // successful response 
             }
           });
      
     
    })
}