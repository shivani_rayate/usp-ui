// const etcdConfig = require('./etcd');
// const common = require('../../config/components/common');
const mongoose = require('mongoose');
const config = require('../../config/config.json');
const constants = require('../../config/constant');

const { DATABASE_MONGO_PREFIX } = constants;
// const DATABASE_CONFIG_KEY = constants.DATABASE_CONFIG_KEY;


// deprecation ERROR Fixes
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);


// deprecation ERROR Fixes


module.exports = {
  bootstrap: () => {
    // eslint-disable-next-line max-len
    // let config = common.getValue(DATABASE_CONFIG_KEY, etcdConfig.etcdInstance, {recursive: true}, true);
    const mongodb = config.mongodb["development"];
    const connectionString = `${DATABASE_MONGO_PREFIX}${mongodb.username}:${mongodb.password}@${mongodb.host}:${mongodb.port}/${mongodb.database}`;

    console.log(`connectionString: ${connectionString}`);
    // There was an issue while connecting mongodb with development username and password
    // FOR LOCAL MONGODB CONNECT ONLY, WAS HAVING AUTHORIZATION ISSUE IN LOCALDB
    // const connectionString = 'mongodb://127.0.0.1:27017/gemplex_db';

    mongoose.connect(connectionString, (err, db) => {
      if (err) {
        console.error(err);
        throw new Error('Unable to connect MongoDB');
      }
      console.log(`Connected to MongoDB! ${db}`);
    });
    // TODO: Validate DB Address
  },
  config: null,
};
