
$(function () {

    async function channel_listing() {
        var data_count = 0;
        var data_count2 = 0;
        var data_count3 = 0;
        var randCounter = 0;
        var idle_combine_data = []
        var started_combine_data = []
        var stopped_combine_data = []
        await send_get_request().then((data_channels) => {
            for (var i = 0; i < data_channels.length; i++) {
                channel_Name = data_channels[i].channelName,
                    serverip = data_channels[i].ServerIp
                let params = {
                    channel_Name: channel_Name,
                    serverip: serverip
                }

                send_post_request(params).then((somedata) => {

                    var state = somedata.smil.head;
                    content_state = state[0].meta[1].$.content;

                    if (content_state === 'idle') {
                        idle_combine_data.push({ channeldata: data_channels[data_count + data_count2 + data_count3], state: content_state })
                        data_count++

                    }
                    if (content_state === 'stopped') {
                        stopped_combine_data.push({ channeldata: data_channels[data_count + data_count2 + data_count3], state: content_state })
                        data_count2++

                    }
                    if (content_state === 'started') {
                        started_combine_data.push({ channeldata: data_channels[data_count + data_count2 + data_count3], state: content_state })
                        data_count3++

                    }

                }).then(() => {
                    randCounter++

                    if (randCounter >= data_channels.length) {

                        let count = {
                            idleCount: data_count,
                            stoppedCount: data_count2,
                            startedCount: data_count3
                        }
                        createDonutChart(count)

                    }
                })
                    .catch((error) => console.log("Error Occured" + error))
            }
            var total_channel_count = data_channels.length;

            $('#total-channels').html(total_channel_count);
            $("#total-channels-graph-count").val(total_channel_count);

            $(".total-channels-graph-count").knob({
                readOnly: true,
                width: "80",
                height: "80",
                fgColor: "#f05050",
                bgColor: "#F9B9B9",
                skin: "tron",
                angleOffset: "180",
                thickness: ".15"
            });


        })
            .catch(() => console.log("Error Occured"))

        setTimeout(function () {
            channel_listing()
        }, 10000)


    }

    //Get server Listing 
    function instance_listing() {
        $.ajax({
            url: '/v1/server/instance_listing',
            type: 'GET',
            headers: {
                'Content-Type': "application/json",
            },
            'success': function (data, textStatus, request) {
                if (data) {
                    var _data = data.data;

                    for (var i = 0; i < _data.Reservations.length; i++) {
                        var total_servers = _data.Reservations.length
                        var totalprimary = _data.Reservations[i].Instances[i]

                        if (total_servers != 0) {
                            $("#All-Servers-href").attr("href", "/v1/server");
                        } else {
                            $("#All-Servers-href").attr("href", "#");
                        }
                        $('#total-servers').html(total_servers);
                        $("#total-servers-graph-count").val(total_servers);
                        $(".total-servers-graph-count").knob({
                            readOnly: true,
                            width: "80",
                            height: "80",
                            fgColor: "#C71C22",
                            bgColor: "#F9B9B9",
                            skin: "tron",
                            angleOffset: "180",
                            thickness: ".15"
                        });
                    }
                } else {
                    alert(data.message)
                }
            },
            'error': function (request, textStatus, errorThrown) {
                console.log("ERR " + errorThrown)
            }
        })
        setTimeout(function () {
            instance_listing()
        }, 5000)
    }
    function primaryServerListing() {
        $.get('/v1/server/instance_listing_serevr',
            function (data, status) {
                if (data.status === 200) {

                    _data = data.data
                    for (var i = 0; i < _data.Reservations.length; i++) {
                        var serverprimary_count = _data.Reservations.length;
                        $('#totalprimaryservers').html(serverprimary_count);
                        if (serverprimary_count != 0) {
                            $("#primary-Servers-href").attr("href", "/v1/primaryServerListing");
                        } else {
                            $("#primary-Servers-href").attr("href", "#");
                        }

                        $("#totalprimaryservers-graph-count").val(serverprimary_count);
                        $(".totalprimaryservers-graph-count").knob({
                            readOnly: true,
                            width: "80",
                            height: "80",
                            fgColor: "#73A839",
                            bgColor: "#73a8396e",
                            skin: "tron",
                            angleOffset: "180",
                            thickness: ".15"
                        });
                    }
                } else {
                }
            });
        setTimeout(function () {
            primaryServerListing()
        }, 5000)
    }
    function backupServerListing() {
        $.get('/v1/server/server-listing-backup',
            function (data, status) {
                if (data.status === 200) {

                    _data = data.data
                    for (var i = 0; i < _data.Reservations.length; i++) {
                        var serverbackup_count = _data.Reservations.length;
                        $('#totalbackupservers').html(serverbackup_count);

                        if (serverbackup_count != 0) {
                            $("#backup-Servers-href").attr("href", "/v1/backupServerListing");
                        } else {
                            $("#backup-Servers-href").attr("href", "#");
                        }
                        $("#totalbackupservers-graph-count").val(serverbackup_count);
                        $(".totalbackupservers-graph-count").knob({
                            readOnly: true,
                            width: "80",
                            height: "80",
                            fgColor: "#DD5600",
                            bgColor: "#dd560057",
                            skin: "tron",
                            angleOffset: "180",
                            thickness: ".15"
                        });
                    }
                } else {
                }
            });
        setTimeout(function () {
            backupServerListing()
        }, 5000)
    }


    function backupchannellisting() {

        $.get('/v1/analytics/backupchannel_listing',
            function (data, status) {
                if (data.status === 200) {

                    _data = data.data

                    var backup_channel_count = _data.length;

                    if (backup_channel_count != 0) {
                        $("#backup-channel-href").attr("href", "/v1/backupChannelListing");
                    } else {
                        $("#backup-channel-href").attr("href", "#");
                    }

                    $('#totalbackupchannels').html(backup_channel_count);
                    $("#totalbackupchannels-graph-count").val(backup_channel_count);

                    $(".totalbackupchannels-graph-count").knob({
                        readOnly: true,
                        width: "80",
                        height: "80",
                        fgColor: "rgb(255, 189, 74)",
                        bgColor: "#F0F29C",
                        skin: "tron",
                        angleOffset: "180",
                        thickness: ".15"
                    });

                } else {

                }
            });
        setTimeout(function () {
            backupchannellisting()
        }, 5000)
    }


    function primarychannellisting() {

        $.get('/v1/livestreaming/channel_listing',
            function (data, status) {
                if (data.status === 200) {

                    _data = data.data

                    var primary_channel_count = _data.length;

                    if (primary_channel_count != 0) {
                        $("#in-progress-href").attr("href", "/v1/primaryChannelListing");
                    } else {
                        $("#in-progress-href").attr("href", "#");
                    }
                    $('#totalprimarychannels').html(primary_channel_count);
                    $("#totalprimarychannels-graph-count").val(primary_channel_count);


                    $(".totalprimarychannels-graph-count").knob({
                        readOnly: true,
                        width: "80",
                        height: "80",
                        fgColor: "#5B69BC",
                        bgColor: "#5b69bc52",
                        skin: "tron",
                        angleOffset: "180",
                        thickness: ".15"
                    });

                } else {

                }
            });
        setTimeout(function () {
            primarychannellisting()
        }, 5000)
    }
    primarychannellisting()
    backupchannellisting()
    channel_listing()
    instance_listing()
    primaryServerListing()
    backupServerListing()

})


function createDonutChart(count) {
    try {
        $('#morris-donut-example').empty()

        Morris.Donut({
            element: "morris-donut-example",
            data: [

                { label: "Idle", value: count.idleCount },
                { label: "Stopped", value: count.stoppedCount },
                { label: "Started", value: count.startedCount }
            ],
            resize: !0,
            colors: ["#35b8e0", "#5b69bc", "#ff8acc", "#ffbd4a"],
            backgroundColor: "#fff",
            labelColor: "#000",
            labelFont: "12px"
        })
    } catch (error) {
        console.log(error);
    }
}

post_response = (params) => {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/get_channel_state`, params,
            function (data, status) {
                if (data.status === 200)
                    resolve(data.data)
                else
                    reject()
            })
    })
}


async function send_post_request(params) {
    try {
        const post_resp = await post_response(params)
        return post_resp
    } catch (error) {
        console.log(error);
    }
}

get_response = () => {

    return new Promise((resolve, reject) => {
        $.get('/v1/livestreaming/channel_listing',
            function (data, status) {
                if (data.status === 200)
                    resolve(data.data)
                else
                    reject()
            })
    })
}

async function send_get_request() {
    try {
        const get_resp = await get_response()

        return get_resp
    } catch (error) {
        console.log(error);
    }
}



$(function () {

    async function channelbackup_listing() {
        var data_count_backup = 0;
        var data_count2_backup = 0;
        var data_count3_backup = 0;
        var randCounter_backup = 0;
        var idle_combine_data_backup = []
        var started_combine_data_backup = []
        var stopped_combine_data_backup = []
        await send_get_request_backup().then((data_channels) => {
            for (var i = 0; i < data_channels.length; i++) {
                backup_channel_Name = data_channels[i].channelName,
                    serverip = data_channels[i].backupserverIp
                let params = {
                    backup_channel_Name: backup_channel_Name,
                    serverip: serverip
                }

                send_post_request_backup(params).then((somedata) => {
                    var state = somedata.smil.head;

                    content_state = state[0].meta[1].$.content;

                    if (content_state === 'idle') {
                        idle_combine_data_backup.push({ channeldata: data_channels[data_count_backup + data_count2_backup + data_count3_backup], state: content_state })
                        data_count_backup++


                    }
                    if (content_state === 'stopped') {
                        stopped_combine_data_backup.push({ channeldata: data_channels[data_count_backup + data_count2_backup + data_count3_backup], state: content_state })
                        data_count2_backup++


                    }
                    if (content_state === 'started') {
                        started_combine_data_backup.push({ channeldata: data_channels[data_count_backup + data_count2_backup + data_count3_backup], state: content_state })
                        data_count3_backup++

                    }

                }).then(() => {
                    randCounter_backup++

                    if (randCounter_backup >= data_channels.length) {

                        let count_backup = {
                            idleCount_backup: data_count_backup,
                            stoppedCount_backup: data_count2_backup,
                            startedCount_backup: data_count3_backup
                        }
                        createDonutChart_backup(count_backup)

                    }
                })
                    .catch((error) => console.log("Error Occured" + error))
                setTimeout(function () {
                    channelbackup_listing()
                }, 12000)
            }

        })
    }
    channelbackup_listing()
})


function createDonutChart_backup(count_backup) {

    try {
        $('#morris-donut-example_backup').empty()

        Morris.Donut({
            element: "morris-donut-example_backup",
            data: [

                { label: "Idle", value: count_backup.idleCount_backup },
                { label: "Stopped", value: count_backup.stoppedCount_backup },
                { label: "Started", value: count_backup.startedCount_backup }
            ],
            resize: !0,
            colors: ["#35b8e0", "#5b69bc", "#ff8acc", "#ffbd4a"],
            backgroundColor: "#fff",
            labelColor: "#000",
            labelFont: "12px"
        })
    } catch (error) {
        console.log(error);
    }
}

post_response_backup = (params) => {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/get_backup_state_of_channel`, params,
            function (data, status) {
                if (data.status === 200)
                    resolve(data.data)
                else
                    reject()
            })
    })
}


async function send_post_request_backup(params) {
    try {
        const post_resp = await post_response_backup(params)

        return post_resp
    } catch (error) {
        console.log(error);
    }
}

get_response_backup = () => {
    return new Promise((resolve, reject) => {
        $.get('/v1/livestreaming/channel_name',
            function (data, status) {
                if (data.status === 200)
                    resolve(data.data)
                else
                    reject()
            })
    })
}

async function send_get_request_backup() {
    try {
        const get_resp = await get_response_backup()

        return get_resp
    } catch (error) {
        console.log(error);
    }
}