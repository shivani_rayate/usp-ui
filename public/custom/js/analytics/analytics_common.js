
// Get server Listing 
function instance_listing() {

    $.ajax({
        url: '/v1/server/instance_listing_serevr',
        type: 'GET',
        headers: {
            'Content-Type': "application/json",
        },
        'success': function (data, textStatus, request) {

            if (data) {

                var _data = data.data;
                var Name, State, ID, ARN, InputCount;

                $('#server-Ip').empty();
                $('#serverlistig').empty();
                $('#divcheckbox').empty();
                $('#updatedivcheckbox').empty();
                $('#update-server-Ip').empty();

                var string = '';
                var string2 = '';
                var string3 = '';
                var stringupdate = '';
                var stringbackup = '';
                for (var i = 0; i < _data.Reservations.length; i++) {

                    InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";
                    state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";
                    PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";

                    var InstanceName;
                    for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
                        if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {
                            InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
                        }
                    }
                    string += '<option class="server-name" value=' + PublicIpAddress + ' servername=' + InstanceName + ' >' + InstanceName + '</option>'
                    string2 += '<a valueIp=' + PublicIpAddress + ' href="javascript:void(0);" class="dropdown-item server-listing-button">' + InstanceName + '</a>'
                    string3 += '<div class="checkbox checkbox-success form-check-inline"> <input type="checkbox" class="inlineCheckbox2" serverkeywordname = ' + InstanceName + '  id="inlineCheckbox2" value="option1"> <label for="inlineCheckbox2"> Backup Server </label> </div>'
                    stringupdate += '<option class="server-name" value=' + PublicIpAddress + ' servername=' + InstanceName + ' >' + InstanceName + '</option>'
                    stringbackup += '<div class="checkbox checkbox-success form-check-inline updatebackup"> <input type="checkbox" style="margin-top: 90px;" class="inlineCheckbox21" serverkeywordname = ' + InstanceName + '  id="inlineCheckbox21" value="option1"  > <label for="inlineCheckbox21">Update Backup Server </label> </div>'

                    $('#server-Ip').html(string);
                    $('#serverlistig').html(string2);
                    $('#divcheckbox').append(string3);
                    $('#updatedivcheckbox').append(stringbackup);
                    $('#update-server-Ip').html(stringupdate);

                }
            } else {
                alert(data.message)
            }
        },
        'error': function (request, textStatus, errorThrown) {
            console.log("ERR " + errorThrown)
        }
    })
}

instance_listing()


$(document).on("click", ".inlineCheckbox21", function () {
    var ServerKeyword = $(this).attr('serverkeywordname')
    var BackupServerName = ServerKeyword + `-backup`
    $.get(`/v1/server/server-listing-backup`,
        function (data, status) {
            if (data.status === 200) {
                var _data = data.data;
                // $('#backupserver-div').empty();
                $('#update-backupserver-div').empty();
                // var backup_string = '';
                var update_backup_string = '';
                for (var i = 0; i < _data.Reservations.length; i++) {
                    InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";
                    state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";
                    PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";

                    var InstanceName;
                    for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
                        if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {
                            InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
                        }
                    }

                    let backup_params = {
                        PublicIpAddress: PublicIpAddress,
                        InstanceName: InstanceName,
                        BackupServerName: BackupServerName
                    }

                    if (InstanceName === BackupServerName) {
                        // backup_string += '<option class="server-name" id="backupserver-div"  value=' + PublicIpAddress + ' backupservername=' + InstanceName + ' >' + InstanceName + '</option>'
                        // $('#backupserver-div').append(backup_string);
                        update_backup_string += '<option class="server-name" id="updatebackupserver-div"   value=' + PublicIpAddress + ' backupservername=' + InstanceName + ' >' + InstanceName + '</option>'
                        $('#update-backupserver-div').append(update_backup_string);


                    } else {
                        // $('#backupserver-div').empty();
                        $('#update-backupserver-div').empty();
                       
                    }
                }
                // resolve(data.data)
            } else {
                // reject()
            }
        })
})

// Click Event of Primary Channel
$(document).on("click", ".delete_channel", function (event) {

    var channel_Name = $(this).attr('data-channel-name');
    var serverIp = $(this).attr('data-channel-serverip')

    let params = {
        channel_Name: channel_Name,
        serverip: serverIp
    }

    get_state_of_channel(params).then((channel_Name_content) => {

        var channelState = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";
        let _params = {
            channel_Name: channel_Name,
            channelState: channelState,
            serverIp: serverIp
        }
        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                delete_channel(_params)
            }
        })
    })
})



// Deletion of Primary Channel
function delete_channel(_params) {
    var channel_Name = _params.channel_Name
    var channelState = _params.channelState
    var serverip = _params.serverIp
    let paramsdata = {
        channelName: channel_Name,
        serverip: serverip
    }
    if (channelState === 'idle') {
        $.post('/v1/livestreaming/channel-delete', paramsdata,
            function (data, status) {
                if (data.status === 200) {
                    swal.fire(
                        'Deleted!',
                        'Channel has been deleted',
                        'success'
                    )
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else {
                    toastr.danger('Operation failed')
                }
            });
    } else {
        resetchannel(channel_Name)
    }
    event.preventDefault()
}


// Reset Channel to Server & MongoDB
function resetchannel(channel_Name) {

    var channelName = channel_Name
    let _params = {
        channelName: channelName
    }

    $.post('/v1/livestreaming/update-channel', _params,

        function (data, status) {
            if (data.status === 200) {
                let _data = data.data;
                let update_reset_params = {
                    updatedServerIp: _data.ServerIp,
                    xmlFileName: _data.channelName,
                    UpdatedchannelName: _data.channelName,
                    Updatedcreator: _data.creator,
                    UpdatedlookaheadFragments: _data.lookaheadFragments,
                    UpdateddvrWindowLength: _data.dvrWindowLength,
                    UpdatedarchiveSegmentLength: _data.archiveSegmentLength,
                    Updatedarchiving: _data.archiving,
                    UpdatedarchiveLength: _data.archiveLength,
                    UpdatedrestartOnEncoderReconnect: _data.restartOnEncoderReconnect,
                    UpdatedissMinimumFragmentLength: _data.issMinimumFragmentLength,
                    UpdatedhlsMinimumFragmentLength: _data.hlsMinimumFragmentLength,
                }
                var xmlUpdatedData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language">   <head>   <meta name="clientManifestRelativePath" content="${update_reset_params.xmlFileName}.ismc" />        <meta name="creator" content="${update_reset_params.Updatedcreator}" />        <meta name="lookahead_fragments" content="${update_reset_params.UpdatedlookaheadFragments}" />       <meta name="dvr_window_length" content="${update_reset_params.UpdateddvrWindowLength}" />      <meta name="archive_segment_length" content="${update_reset_params.UpdatedarchiveSegmentLength}" />      <meta name="archiving" content="${update_reset_params.Updatedarchiving}" />   </head>   <body>    <switch>     </switch>    </body> </smil>`;
                let _params = {
                    xmlUpdatedData: xmlUpdatedData,
                    xmlFileName: _data.channelName,
                    UpdatedchannelName: _data.channelName,
                    Updatedcreator: _data.creator,
                    UpdatedlookaheadFragments: _data.lookaheadFragments,
                    UpdateddvrWindowLength: _data.dvrWindowLength,
                    UpdatedarchiveSegmentLength: _data.archiveSegmentLength,
                    Updatedarchiving: _data.archiving,
                    updatedServerIp: _data.ServerIp,
                    UpdatedarchiveLength: _data.UpdatedarchiveLength,
                    UpdatedrestartOnEncoderReconnect: _data.UpdatedrestartOnEncoderReconnect,
                    UpdatedissMinimumFragmentLength: _data.UpdatedissMinimumFragmentLength,
                    UpdatedhlsMinimumFragmentLength: _data.UpdatedhlsMinimumFragmentLength

                }
                $.post('/v1/livestreaming/update-channel-params', _params,

                    function (data, textStatus, request) {
                        if (data.status === 200) {
                            var channel_Name = data.data.channelName
                            var serverIp = data.data.serverIp
                            let params = {
                                channelName: channel_Name,
                                serverip: serverIp
                            }
                            get_state_of_channel(params).then((channel_Name_content) => {

                                var channelState = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";
                                let _params = {
                                    channel_Name: channel_Name,
                                    channelState: channelState,
                                    serverIp: serverIp
                                }
                                delete_channel(_params)
                            })
                        } else {
                            toastr.danger('Operation Failed')
                        }
                    });
            } else {
                toastr.danger('Operation Failed')
            }
        })
}


// get channel data from Mongofunction getdatafrommongo()
$(document).on("click", ".update_channel", function (event) {

    var channelName = $(this).attr('data-channel-name')
    let _params = {
        channelName: channelName
    }
    getDataFromMongo(_params)
})


function getDataFromMongo(_params) {
    $.post('/v1/livestreaming/update-channel', _params,

        function (data, status) {
            if (data.status === 200) {
                let _data = data.data;

                // console.log("updation channel data" + JSON.stringify(_data))
                $('#update-channel-name').val(_data.channelName)
                $('#update-creator').val(_data.creator)
                $('#update-lookahead-fragments').val(_data.lookaheadFragments)
                $('#update-dvr-window-length').val(_data.dvrWindowLength)
                $('#update-archive-segment-length').val(_data.archiveSegmentLength)
                $('#update-archiving').val(_data.archiving)
                $('#update-archive-length').val(_data.archiveLength)
                $('#update-Restart-Encoder-Reconnect').val(_data.restartOnEncoderReconnect)
                $('#update-iss-Minimum-Fragment-Length').val(_data.issMinimumFragmentLength)
                $('#update-hls-Minimum-Fragment-Length').val(_data.hlsMinimumFragmentLength)
                $('#update-server-Ip').val(_data.ServerIp),
                $('option:selected', this).attr(_data.serverName)
                backupserverIp = $('#updatebackupserver-div').val()
                backupserverName = $('#updatebackupserver-div').text().trim()

            } else {
                toastr.error(data.message);

            }
        })

    event.preventDefault()
}


// Updation Channel On Server
$(document).on("click", ".update_channel_parameter", function (event) {

    let params = {
        channelName: $('#update-channel-name').val().trim(),
        creator: $('#update-creator').val().trim(),
        lookaheadFragments: $('#update-lookahead-fragments').val().trim(),
        dvrWindowLength: $('#update-dvr-window-length').val().trim(),
        archiveSegmentLength: $('#update-archive-segment-length').val().trim(),
        archiving: $('#update-archiving').val().trim(),
        archiveLength: $('#update-archive-length').val().trim(),
        restartOnEncoderReconnect: $('#update-Restart-Encoder-Reconnect').val().trim(),
        issMinimumFragmentLength: $('#update-iss-Minimum-Fragment-Length').val().trim(),
        hlsMinimumFragmentLength: $('#update-hls-Minimum-Fragment-Length').val().trim(),
        serverIp: $('#update-server-Ip').val().trim(),
        backupserverIp: $('#updatebackupserver-div').val().trim()

    }
    var xmlUpdatedData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language">   <head>   <meta name="clientManifestRelativePath" content="${params.channelName}.ismc" />        <meta name="creator" content="${params.creator}" />        <meta name="lookahead_fragments" content="${params.lookaheadFragments}" />       <meta name="dvr_window_length" content="${params.dvrWindowLength}" />      <meta name="archive_segment_length" content="${params.archiveSegmentLength}" />      <meta name="archiving" content="${params.archiving}" /> <meta name="archive_length" content="${params.archiveLength}" />  </head>   <body>    <switch>     </switch>    </body> </smil>`;

    let _params = {
        xmlUpdatedData: xmlUpdatedData,
        xmlFileName: params.channelName,
        UpdatedchannelName: params.channelName,
        Updatedcreator: params.creator,
        UpdatedlookaheadFragments: params.lookaheadFragments,
        UpdateddvrWindowLength: params.dvrWindowLength,
        UpdatedarchiveSegmentLength: params.archiveSegmentLength,
        Updatedarchiving: params.archiving,
        UpdatedarchiveLength: params.archiveLength,
        UpdatedrestartOnEncoderReconnect: params.restartOnEncoderReconnect,
        UpdatedissMinimumFragmentLength: params.issMinimumFragmentLength,
        UpdatedhlsMinimumFragmentLength: params.hlsMinimumFragmentLength,
        UpdatedhlsMinimumFragmentLength: params.hlsMinimumFragmentLength,
        updatedServerIp: params.serverIp,
        updatedBackupServerIp: params.backupserverIp,

    }
    primaryupdate(_params)
    backupupdate(_params)
    event.preventDefault()
})


function primaryupdate(_params) {

    $.post('/v1/livestreaming/update-channel-params', _params,

        function (data, textStatus, request) {
            if (data.status === 200) {

                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Channel Data has been Updated!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            } else {
                toastr.danger('Updation Failed')
            }
        });
}


function backupupdate(_params) {

    $.post('/v1/livestreaming/update-backup-channel-params', _params,

        function (data, textStatus, request) {
            if (data.status === 200) {

                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Channel Data has been Updated!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            } else {
                toastr.danger('Updation Failed')
            }
        });
}


var player;
//  Initiate publishing channel Player
function initiatePlayer(params) {
    return new Promise((resolve, reject) => {

        if (player !== undefined) {
            player.reset()
        }

        var options = {
            plugins: {
                // streamrootHls: {
                //     hlsjsConfig: {
                //         smoothQualityChange: true
                //     }
                // },
                controlBar: {
                    progressControl: {
                        //   keepTooltipsInside: true,
                        MouseTimeDisplay: true,
                        PlayProgressBar: true
                    }
                },
                qualityMenu: {
                    useResolutionLabels: true
                }
            }
        };
        player = videojs(params.player_id, options)
        player.src({
            type: 'application/x-mpegURL',
            src: params.url
        });
        // player.controlBar.liveDisplay.show()
        player.qualityMenu();
        player.onloadeddata = function () {
            resolve()
        };
    })
}


// Play Channel Button
$(document).on("click", ".play-video-channelButton", function () {

    var channelName = $(this).attr('data-channel-name');
    var publishingplayurl = $(this).attr('publishing-channel-url')
    var publishing_url_video = publishingplayurl + '/.m3u8'

    // var publishing_url_video = 'http://suntv.skandha.tv/KTVHD/KTVHD.isml/.m3u8'

    var params = {
        player_id: 'channelurlplayer',
        url: publishing_url_video,
        channelName: channelName
    }
    initiatePlayer(params)
})


$(document).on("click", ".channel_details", function () {
    
    $('#channel-listing-div').removeClass("force-hidden")
    $("#body-content").addClass("enlarged");
    $('#channel-listing-div').removeClass("col-md-12")
    $('#channel-listing-div').addClass("col-md-8")
    $('#edit-asset-div').removeClass("force-hidden")

    var channelName = $(this).attr('data-channel-name');

    let channel_details_params = {
        channelName: channelName
    }

    $.post('/v1/livestreaming/update-channel', channel_details_params,
        function (data, status) {
            if (data.status === 200) {
                formatChannelData(data)
            } else {
                toastr.error(data.message);
            }
        })
})

// formate channel data-
function formatChannelData(data) {

    // $('#technical-data').empty();
    var channeldata_div = "";
    var videoBitrate_div = "";
    var channelData = data.data;
    var channelName = channelData.channelName;
    var creator = channelData.creator;
    var lookaheadFragments = channelData.lookaheadFragments;
    var dvrWindowLength = channelData.dvrWindowLength;
    var archiveSegmentLength = channelData.archiveSegmentLength;
    var archiving = channelData.archiving;
    var objectId = channelData.objectId;
    var publishing_url = channelData.url;
    var ServerIp = channelData.ServerIp;
    var restartOnEncoderReconnect = channelData.restartOnEncoderReconnect;
    var issMinimumFragmentLength = channelData.issMinimumFragmentLength;
    var hlsMinimumFragmentLength = channelData.hlsMinimumFragmentLength;
    var params = {
        channelName: channelName,
        serverip: ServerIp
    }
    channel_statistics(params).then((channel_statistics) => {
        var statistics_video = channel_statistics.smil.body[0];
        let videoBitrate = [];
        let audioBitrate = [];

         if (statistics_video.video.length >= 1) {
             alert()

            for (var j = 0; j < statistics_video.video.length; j++) {
                videoSystemBitrate = statistics_video.video[j].$.systemBitrate;
                videoBitrate.push(videoSystemBitrate)
                console.log(JSON.stringify(videoBitrate))
            }
        }
        for (var k = 0; k < statistics_video.audio.length; k++) {
            audioSystemBitrate = statistics_video.audio[k].$.systemBitrate;
            audioBitrate.push(audioSystemBitrate)
        }

        videoBitrate.forEach(function (element, i) {
            var videoBitrate = element ? element : "-";
            videoBitrate_div += `<dt class="col-sm-8"></dt> 
            <dd class="col-sm-4 text-break">${videoBitrate}</dd>`

        })


        channeldata_div += `<div class="col-sm-12 content-detail">
                    <dl class="row">
                 <dt class="col-sm-2">URL</dt> 
                        <dd class="col-sm-10" id="publishingurl">${publishing_url} </dd>
                 <dt class="col-sm-8">Channel Name</dt> 
                        <dd class="col-sm-4">${channelName} </dd> 
                        <dt class="col-sm-8">Video Bitrate</dt> 
                        <dd class="col-sm-4">${videoBitrate_div}</dd>
                 <dt class="col-sm-8">Audio Bitrate</dt> 
                        <dd class="col-sm-4">${audioBitrate}</dd>
                 <dt class="col-sm-8">Creator</dt> 
                        <dd class="col-sm-4">${creator} </dd> 
                 <dt class="col-sm-8">Lookahead Fragments</dt> 
                        <dd class="col-sm-4">${lookaheadFragments} </dd>
                 <dt class="col-sm-8">DVR Window Length</dt> 
                        <dd class="col-sm-4">${dvrWindowLength} </dd> 
                 <dt class="col-sm-8">Archive Segment Length</dt> 
                        <dd class="col-sm-4">${archiveSegmentLength}  </dd>
                 <dt class="col-sm-8">Archiving</dt> 
                        <dd class="col-sm-4">${archiving} </dd>
                 <dt class="col-sm-8">Event Id</dt> 
                        <dd class="col-sm-4">${objectId} </dd>
                 <dt class="col-sm-8">Restart On Encoder Reconnect</dt> 
                        <dd class="col-sm-4">${restartOnEncoderReconnect} </dd>
                 <dt class="col-sm-8">Iss Minimum Fragment Length</dt> 
                        <dd class="col-sm-4">${issMinimumFragmentLength} </dd>
                 <dt class="col-sm-8">Hls Minimum Fragment Length</dt> 
                        <dd class="col-sm-4">${hlsMinimumFragmentLength} </dd>
                    </dl>
                    </div>`

        if (publishing_url === 'undefined') {

            $('#publishingurl').html("No Publishing URL available")

        } else {
            $('#technical-data').html(channeldata_div)
        }
    })
}

// Primary Channel Statistics
function channel_statistics(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/channel_statistics`, params,
            function (data, status) {
                if (data.status === 200) {
                    let _data = data.data;
                    resolve(_data)
                } else {
                    toastr.error(data.message);
                }
            })
    })
}


$(document).on("click", "#close-edit-metadata-panel-btn", function () {
    // remove enlarge sidebar first
  
    $("#body-content").removeClass("enlarged");
    $('#channel-listing-div').addClass("col-md-12")
    $('#edit-asset-div').addClass("force-hidden")
})



function initiateAssetSelection() {
    $("#channel_listing_table tbody tr:first").addClass("active");
    // alert()
}

$(document).on("click", ".channel-overview-tbl-row", function (e) {

    $('.channel-overview-tbl-row').removeClass("active");
    $(this).addClass("active");

    // ########## child rows code ########### //
    var tr = $(this).closest('tr');
    var row = channel_listing_table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    } else {
        var channelName = $(this).attr('data-channel-name')
        getbackupChannel(channelName).then((channelData) => {
            if (channel_listing_table.row('.shown').length) {
                var _tr = $('tr.shown');
                var _row = channel_listing_table.row(_tr);
                _row.child.hide();
                _tr.removeClass('shown');
            }
            formatChannelDetails(channelData).then((channelDetails) => {
                // Open this row
                row.child(channelDetails).show();
                tr.addClass('shown');
            })

        })
    }
})

//Get State Of Channel From Backup Server
function getbackupChannel(channelName) {
    return new Promise((resolve, reject) => {
        $.post('/v1/livestreaming/find-backup-channel',
            {
                channelName: channelName
            },
            function (data, status) {
                if (data.status == 200) {
                    let _data = data.data;

                    resolve(_data)
                } else if (data.status == 400) {
                    reject()
                }
            })
    });
}


function formatChannelDetails(channelData) {
    return new Promise((resolve, reject) => {
        array = channelData;
        if (array !== null) {
            let channel_Name = array.channelName;
            let modified_on = moment(array.created_at).format('lll');
            console.log(modified_on)
            let url = array.url;
            let serverip = array.backupserverIp;
            let serverName = array.backupserverName;
            let flag = array.flag ? array.flag : "-";
            let data_append = '';
            let formatedAssetDetails = '';

            data_append += `<td class="channel-name" style="padding-left: 71px;">${channel_Name}</td>
            <td style="padding-left: 76px;">${modified_on}</td>
            <td  data-channel-serverip=${serverip} style="padding-left:40px">${serverName}</td>
            <td class="${channel_Name}" data-channel-serverip=${serverip} style="padding-left: 74px;"></td>
            <td style="padding-left: 51px;">${flag}</td>
             <td class="asset-action-td pl-5">
              <div class="dropdown" style="padding-left: 124px;"> 
              <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
               <div class="dropdown-menu dropdown-menu-right"><a href="#" data-channel-name=${channel_Name} class="dropdown-item update_channel" data-toggle="modal" data-target="#UpdateModalbox">Edit</a>
                <a href="#" data-channel-name=${channel_Name}  data-channel-serverip=${serverip} class="dropdown-item delete_backup_channel">Delete</a>
                 <a href="#" data-channel-name=${channel_Name} publishing-channel-url=${url} class="dropdown-item backup_channel_details">Details</a>
                 <a href="" data-channel-name=${channel_Name} data-channel-serverip=${serverip}  class="dropdown-item view_source">View Source</a>
                 </div> </div> 
                 </td>
               
                 <td data-toggle="modal" data-target="#play-video-channel" data-channel-name=${channel_Name} publishing-channel-url=${url} style="padding-left: 92px;"><i data-channel-name=${channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-backupChannelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                 </tr>`

            formatedAssetDetails += data_append

            resolve(`<tr class='channel-row' data-channel-name=${channel_Name} data-data="${modified_on}" flag-value=${flag} style='cursor: pointer'>${data_append}`)
            getbackupStateSpecific(array)
        }

    })

}

function getbackupStateSpecific(array) {

    var channel_Name = array.channelName;
    var serverip = array.backupserverIp;
    var params = {
        channel_Name: channel_Name,
        serverip: serverip
    }

    get_backup_state_of_channel(params).then((channel_Name_content) => {
        var state = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";
console.log("state>>> "+state)
        if (state === 'idle') {
            $(`.${channel_Name}`).html('<span class="badge badge-customblue badge" style=" background-color: #097bd3;font-size: 12px; width: 50px;">Idle</span>')
        }

        else if (state === 'starting') {
            $(`.${channel_Name}`).html('<span class="badge badge-purple badge" style="font-size: 12px; width: 50px;">starting</span>')

        } else if (state === 'started') {
            $(`.${channel_Name}`).html('<span class="badge badge-success badge" style="font-size: 12px; width: 50px;">started</span>')

        } else if (state === 'stopping') {
            $(`.${channel_Name}`).html('<span class="badge badge-dark badge" style="font-size: 12px; width: 50px;">stopping</span>')

        } else if (state === 'stopped') {
            $(`.${channel_Name}`).html('<span class="badge badge-danger badge" style="font-size: 12px; width: 50px;">stopped</span>')
        }
        // setTimeout(function () {
        //     getState(array);

        // }, 10000);
    })

    // .catch((err) => {
    //    console.log(err) 
    // })

    // })
}

// Get Backup Channel State From Server
function get_backup_state_of_channel(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/get_backup_state_of_channel`, params,
            function (data, status) {
                if (data.status === 200) {
                    resolve(data.data)
                } else {
                    reject()
                }
            })
    })
}
$(document).on("click", ".refresh", function () {
    channel_listing()
})
