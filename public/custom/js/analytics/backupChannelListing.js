// Get All Channel Listinf From MongoDb
function channel_listing() {
    $.get('/v1/analytics/backupchannel_listing',
        function (data, status) {
            if (data.status == 200) {
                destroyRows();
                appendChannelDatatable(data);
            }
        });
}


// document.ready
$(function () {
    channel_listing()
    instance_listing()
})


function destroyRows() {
    $('#channel_listing_tbody').empty()
    $('#channel_listing_table').DataTable().rows().remove();
    $("#channel_listing_table").DataTable().destroy()
}

function appendChannelDatatable(data) {

    var array = data.data;
    var options_table = "";
  
    array.forEach(function (element, i) {

        var channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
        var url = element.url ? element.url : "-";
        var serverip = element.backupserverIp ? element.backupserverIp : "-";
        var serverName = element.backupserverName ? element.backupserverName : "-"

        options_table += `<tr class="channel-overview-tbl-row" data-data="Any Data"> <td class="index">${i + 1}</td>
        <td class="channel-name">${channel_Name}</td>
          <td>${modified_on}</td>
          <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
          <td class="${channel_Name}"  data-channel-serverip=${serverip}></td>
          
             
               </tr>`

        if (i == array.length - 1) {
            $('#channel_listing_tbody').append(options_table)
            reInitializeDataTable();

            //getState Function
            getState(array);
        }
    })
}


//Get Channel State using state API
function getState(array) {

    array.forEach(function (element, i) {
        var channel_Name = element.channelName ? element.channelName : "-";
        var serverip = element.backupserverIp ? element.backupserverIp : "-"
        var params ={
            channel_Name:channel_Name,
            serverip:serverip
        }
        get_state_of_channel(params).then((channel_Name_content) => {
            var state = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";
          
            if (state === 'idle') {
                $(`.${channel_Name}`).html('<span class="badge badge-customblue badge" style=" background-color: #097bd3;font-size: 12px; width: 50px;">Idle</span>')  
            }
           
            else if (state === 'starting') {
                $(`.${channel_Name}`).html('<span class="badge badge-purple badge" style="font-size: 12px; width: 50px;">starting</span>')

            } else if (state === 'started') {
                $(`.${channel_Name}`).html('<span class="badge badge-success badge" style="font-size: 12px; width: 50px;">started</span>')

            } else if (state === 'stopping') {
                $(`.${channel_Name}`).html('<span class="badge badge-dark badge" style="font-size: 12px; width: 50px;">stopping</span>')

            } else if (state === 'stopped') {
                $(`.${channel_Name}`).html('<span class="badge badge-danger badge" style="font-size: 12px; width: 50px;">stopped</span>')
            }
            // setTimeout(function () {
            //     getState(array);

            // }, 10000);
        })
      
        // .catch((err) => {
        //    console.log(err) 
        // })
       
    })
   
}


// Get State Of Channel Listing From Server
function get_state_of_channel(params) {

    return new Promise((resolve, reject) => {  
        $.post(`/v1/livestreaming/get_channel_state`,params,
            function (data, status) {
                if (data.status === 200) {
                    resolve(data.data)
                } else {
                    reject()
                }
            })
    })
}

var channel_listing_table;
function reInitializeDataTable() {
    $("#channel_listing_table").DataTable().destroy()
    channel_listing_table = $('#channel_listing_table').DataTable({
        // "order": [[2, "desc"]], // for descending order
        // "columnDefs": [
        //     { "width": "40%", "targets": 1 }
        // ]
    })
}



var player;
//  Initiate publishing channel Player
function initiatePlayer(params) {

    return new Promise((resolve, reject) => {


        if (player !== undefined) {
            player.reset()
        }

        var options = {
            plugins: {
                // streamrootHls: {
                //     hlsjsConfig: {
                //         smoothQualityChange: true
                //     }
                // },
                controlBar: {
                    progressControl: {
                    //   keepTooltipsInside: true,
                      MouseTimeDisplay:true,
                      PlayProgressBar:true
                    }
                  },
                qualityMenu: {
                    useResolutionLabels: true
                }
            }
        };
        player = videojs(params.player_id ,options)
        player.src({
            type: 'application/x-mpegURL',
            src: params.url
        });
       // player.controlBar.liveDisplay.show()
        player.qualityMenu();
        player.onloadeddata = function () {
            resolve()
        };

    })

}



// formate channel daata-
function formatChannelData(data) {

    // $('#technical-data').empty();
    var channeldata_div = "";
    var videoBitrate_div = "";
    var channelData = data.data;
    var channelName = channelData.channelName;
    var creator = channelData.creator;
    var lookaheadFragments = channelData.lookaheadFragments;
    var dvrWindowLength = channelData.dvrWindowLength;
    var archiveSegmentLength = channelData.archiveSegmentLength;
    var archiving = channelData.archiving;
    var objectId = channelData.objectId;
    var publishing_url = channelData.url;
    var ServerIp = channelData.ServerIp;
    var restartOnEncoderReconnect = channelData.restartOnEncoderReconnect;
    var issMinimumFragmentLength = channelData.issMinimumFragmentLength;
    var hlsMinimumFragmentLength = channelData.hlsMinimumFragmentLength;
     var params = {
        channelName:channelName,
        serverip:ServerIp
    }
    channel_statistics(params).then((channel_statistics) => {
        var statistics_video = channel_statistics.smil.body[0];
        let videoBitrate = [];
        let audioBitrate = [];

        if (statistics_video.video.length === 0) {
            statistics_video = null
        }
        else if (statistics_video.video.length >= 1) {
           
        for (var j = 0; j < statistics_video.video.length; j++) {
            videoSystemBitrate = statistics_video.video[j].$.systemBitrate;
            videoBitrate.push(videoSystemBitrate)
        }
    }
        for (var k = 0; k < statistics_video.audio.length; k++) {
            audioSystemBitrate = statistics_video.audio[k].$.systemBitrate;
            audioBitrate.push(audioSystemBitrate)
        }

        videoBitrate.forEach(function (element, i) {
            var videoBitrate = element ? element : "-";
           // console.log("videoBitrate"+JSON.stringify(videoBitrate))

            videoBitrate_div += `<dt class="col-sm-8"></dt> 
            <dd class="col-sm-4 text-break">${videoBitrate}</dd>`

        })
      

        channeldata_div += `<div class="col-sm-12 content-detail">
                    <dl class="row">
                 <dt class="col-sm-2">URL</dt> 
                        <dd class="col-sm-10 text-break" id="publishingurl">${publishing_url} </dd>
                 <dt class="col-sm-8">Channel Name</dt> 
                        <dd class="col-sm-4">${channelName} </dd> 
                        <dt class="col-sm-8">Video Bitrate</dt> 
                        <dd class="col-sm-4">${videoBitrate_div}</dd>
                 <dt class="col-sm-8">Audio Bitrate</dt> 
                        <dd class="col-sm-4">${audioBitrate}</dd>
                 <dt class="col-sm-8">Creator</dt> 
                        <dd class="col-sm-4">${creator} </dd> 
                 <dt class="col-sm-8">Lookahead Fragments</dt> 
                        <dd class="col-sm-4">${lookaheadFragments} </dd>
                 <dt class="col-sm-8">DVR Window Length</dt> 
                        <dd class="col-sm-4">${dvrWindowLength} </dd> 
                 <dt class="col-sm-8">Archive Segment Length</dt> 
                        <dd class="col-sm-4">${archiveSegmentLength}  </dd>
                 <dt class="col-sm-8">Archiving</dt> 
                        <dd class="col-sm-4">${archiving} </dd>
                 <dt class="col-sm-8">Event Id</dt> 
                        <dd class="col-sm-4">${objectId} </dd>
                 <dt class="col-sm-8">Restart On Encoder Reconnect</dt> 
                        <dd class="col-sm-4">${restartOnEncoderReconnect} </dd>
                 <dt class="col-sm-8">Iss Minimum Fragment Length</dt> 
                        <dd class="col-sm-4">${issMinimumFragmentLength} </dd>
                 <dt class="col-sm-8">Hls Minimum Fragment Length</dt> 
                        <dd class="col-sm-4">${hlsMinimumFragmentLength} </dd>
                    </dl>
                    </div>`

        if (publishing_url === 'undefined') {

            $('#publishingurl').html("No Publishing URL available")

        } else {
            $('#technical-data').html(channeldata_div)
        }
    })
}

function channel_statistics(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/channel_statistics`,params,
            function (data, status) {
                if (data.status === 200) {
                    let _data = data.data;
                    resolve(_data)
                } else {
                    toastr.error(data.message);
                }
            })
    })
}


// close-edit-metadata-panel
$(document).on("click", "#close-edit-metadata-panel-btn", function () {
    // remove enlarge sidebar first
    $("#body-content").removeClass("enlarged");
    $('#channel-listing-div').addClass("col-md-12")
    $('#edit-asset-div').addClass("force-hidden")
})  
