
function appendbackupServer(_data) {
    var Name, State, ID, ARN, InputCount;
    var index = 1;

    $('#ec2_backuptbody').empty();
   
    

    var string = '';
    var Uspversion = '1.10.18'

    for (var i = 0; i < _data.Reservations.length; i++) {

        InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";

        state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";

        PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";

        var InstanceName;

        for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
          
            if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {
               
                    InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
                }
              
                // InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
           
           
         

        }

     string += '<tr id="' + InstanceId + '"> <td>' + index + '</td> <td class="channel-name">' + InstanceName + '</td> <td>'+ Uspversion +'</td> <td>' + PublicIpAddress + '</td> <td>' + state + '</td> </tr>'

        index++
       
        $('#ec2_backuptable').DataTable().rows().remove();
        $("#ec2_backuptable").DataTable().destroy()
        $('#ec2_backuptbody').html(string);
       
        $('#ec2_backuptable').DataTable().destroy()
        $('#ec2_backuptable').DataTable({

        });
  

    }
   


    // string2 += '<a valueIp=' + PublicIpAddress + ' href="javascript:void(0);" class=" server-listing-primary"' + InstanceName + '>' + InstanceName_primary + '</a>'
    // $('#serverlistig_primary').html(string2);

}

function instance_listing() {
    $.ajax({
        url: '/v1/server/server-listing-backup',
        type: 'GET',
        headers: {
            'Content-Type': "application/json",
        },
        'success': function (data, textStatus, request) {

            if (data) {

                var _data = data.data;
                destroyRowsServer()
                appendbackupServer(_data)
            } else {
                alert(data.message)
            }
        },

        'error': function (request, textStatus, errorThrown) {

            console.log("ERR " + errorThrown)

        }

    })
}
instance_listing();

function destroyRowsServer() {
    $('#ec2_tbody').empty()
    $('#ec2_table').DataTable().rows().remove();
    $("#ec2_table").DataTable().destroy()
}
