function channel_listing() {

    return new Promise((resolve, reject) => {
        $.get('/v1/analytics/backupchannel_listing',
            function (data, status) {
                if (data.status == 200) {
                 destroyRows();
                    appendidelBackupChannelDatatable(data);
                    resolve(data)

                }
            });
    })
}

channel_listing()
function destroyRows() {
    $('#idelbackup_state_tbody').empty()
    $('#idelbackup_state_table').DataTable().rows().remove();
    $("#idelbackup_state_table").DataTable().destroy()

}

function appendidelBackupChannelDatatable(data) {

    var array = data.data;
    var options_table = "";

    array.forEach(function (element, i) {

        var channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
       // var url = element.url ? element.url : "-";
        var serverip = element.backupserverIp ? element.backupserverIp : "-";
        var serverName = element.backupserverName ? element.backupserverName : "-";
        var state = element.state ? element.state : "-"

            if (state === 'idle') {
                state = '<span class="badge badge-customblue badge" style=" background-color: #097bd3;font-size: 12px; width: 50px;">Idle</span>'
         options_table += `<tr class='channel-overview-tbl-row'  data-channel-name=${channel_Name} data-data="${modified_on}" style='cursor: pointer'> <td class="index">${i + 1}</td>
         <td class="channel-name">${channel_Name}</td>
           <td>${modified_on}</td>
           <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
           <td class="${channel_Name}"  data-channel-serverip=${serverip}>${state}</td>    
                </tr>`
                
            }
            if (i == array.length - 1) {
                $('#idelbackup_state_tbody').append(options_table)
                    reInitializeDataTable()
            }
        })
 
      
}




var channel_listing_table;
function reInitializeDataTable() {
    return new Promise((resolve, reject) => {
        $("#idelbackup_state_table").DataTable().destroy()
        channel_listing_table = $('#idelbackup_state_table').DataTable({})
        resolve()
    })
}


// function get_state_of_channel(params) {

//     return new Promise((resolve, reject) => {
//         $.post(`/v1/idleChannelListing/get_channel_state`, params,
//             function (data, status) {
//                 if (data.status === 200) {
//                     resolve(data.data)
//                 } else {
//                     reject()
//                 }
//             })
//     })
// }
