function channel_listing() {

    return new Promise((resolve, reject) => {
        $.get('/v1/startChannelListing/channel_listing',
            function (data, status) {
                if (data.status == 200) {
                    destroyRows();
                    appendStartedChannelDatatable(data);
                    resolve(data)

                }
            });
    })
}

channel_listing()
function destroyRows() {
    $('#start_tbody').empty()
    $('#start_table').DataTable().rows().remove();
    $("#start_table").DataTable().destroy()
}


function appendStartedChannelDatatable(data) {
    var array = data.data;
    var options_table = "";

    array.forEach(function (element, i) {

        var channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
        var url = element.url ? element.url : "-";
        var serverip = element.ServerIp ? element.ServerIp : "-";
        var serverName = element.serverName ? element.serverName : "-";
        var flag = element.flag ? element.flag : "-";
        var state = element.state ? element.state : "-";

        if (state === 'started') {
            state = '<span class="badge badge-success badge" style="font-size: 12px; width: 50px;">started</span>';
            options_table += `<tr class='channel-overview-tbl-row'  data-channel-name=${channel_Name} data-data="${modified_on}"  style='cursor: pointer' > <td class="index">${i + 1}</td>
           <td class="channel-name" >${channel_Name}</td>
          <td>${modified_on}</td>
          <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
          <td class="${channel_Name}"  data-channel-serverip=${serverip}>${state}</td>         
              
               </tr>`
        }
        if (i == array.length - 1) {
            $('#start_tbody').append(options_table)
            reInitializeDataTable()
        }
    })
    // })
}

var channel_listing_table;
function reInitializeDataTable() {
    return new Promise((resolve, reject) => {
        $("#start_table").DataTable().destroy()
        channel_listing_table = $('#start_table').DataTable({
            // "order": [[2, "desc"]], // for descending order
            // "columnDefs": [
            //     { "width": "40%", "targets": 1 }
            // ]
        })
        resolve()
    })
}


function get_state_of_channel(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/startChannelListing/get_channel_state`, params,
            function (data, status) {
                if (data.status === 200) {
                    resolve(data.data)
                } else {
                    reject()
                }
            })
    })
}


$(document).on("click", ".view_source", function (event) {

    $('#getquery').removeClass("force-hidden")
    $("#body-content").addClass("enlarged");
    // $('#channel-listing-div').removeClass("col-md-12")
    $('#channel-listing-div').addClass("force-hidden")
    // $('#edit-asset-div').removeClass("force-hidden")

    var channelName = $(this).attr('data-channel-name')
    var serverIp = $(this).attr('data-channel-serverip')
    let params = {
        channelName: channelName,
        serverIp: serverIp
    }
    view_source(params)
})
function view_source(params) {
    return new Promise((resolve, reject) => {
        $.post('/v1/viewsource/view_source', params,
            function (data, status) {
                if (data.status == 200) {
                    var _data = data.data

                    getquery(_data)
                    resolve(_data)
                }
            })
    }),
        setTimeout(function () {
            // alert()
            view_source(params);
        }, 6000);
}
function getquery(_data) {
    var getdata = '';
    getdata += `<div>${JSON.stringify(_data)}</div>`
    $('#getquerydata').html(getdata)
}


$(document).on("click", "#getdataclose", function () {
    // remove enlarge sidebar first
    $("#body-content").removeClass("enlarged");
    $('#channel-listing-div').removeClass("force-hidden")
    $('#getquery').addClass("force-hidden")
})
