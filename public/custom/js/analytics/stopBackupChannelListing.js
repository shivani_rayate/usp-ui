function channel_listing() {

    return new Promise((resolve, reject) => {
        $.get('/v1/analytics/backupchannel_listing',
            function (data, status) {
                if (data.status == 200) {
                    destroyRows();
                    appendStoppedBackupChannelDatatable(data);
                    resolve(data)
                }
            });
    })
}


channel_listing()
function destroyRows() {
    $('#stoppedbackup_tbody').empty()
    $('#stoppedbackup_table').DataTable().rows().remove();
    $("#stoppedbackup_table").DataTable().destroy()
}

function  appendStoppedBackupChannelDatatable(data) {

    var array = data.data;
    var options_table = "";

    array.forEach(function (element, i) {

        var channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
        var url = element.url ? element.url : "-";
        var serverip = element.backupserverIp ? element.backupserverIp : "-";
        var serverName = element.backupserverName ? element.backupserverName : "-";
        var state = element.state ? element.state : "-"
       

       
        // get_state_of_channel(params).then((channel_Name_content) => {
        //     var state = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";

            if (state === 'stopped') {

                state = '<span class="badge badge-danger badge" style="font-size: 12px; width: 50px;">stopped</span>'
               
                

                    options_table += `<tr class='channel-overview-tbl-row'  data-channel-name=${channel_Name} data-data="${modified_on}"  style='cursor: pointer'> <td class="index">${i + 1}</td>
         <td class="channel-name">${channel_Name}</td>
           <td>${modified_on}</td>
           <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
           <td class="${channel_Name}"  data-channel-serverip=${serverip}>${state}</td>
        
          
                
                </tr>`
                
            }
            if (i == array.length - 1) {
                $('#stoppedbackup_tbody').append(options_table)
                reInitializeDataTable()
            }
        })
  //  })
}

var channel_listing_table;
function reInitializeDataTable() {
    return new Promise((resolve, reject) => {
        $("#stoppedbackup_table").DataTable().destroy()
        channel_listing_table = $('#stoppedbackup_table').DataTable({
            // "order": [[2, "desc"]], // for descending order
            // "columnDefs": [
            //     { "width": "40%", "targets": 1 }
            // ]
        })
        resolve()
    })
}

function get_state_of_channel(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/stopChannelListing/get_channel_state`, params,
            function (data, status) {
                if (data.status === 200) {
                    resolve(data.data)
                } else {
                    reject()
                }
            })
    })
}

