function channel_listing() {

    return new Promise((resolve, reject) => {
        $.get('/v1/stopChannelListing/channel_listing',
            function (data, status) {
                if (data.status == 200) {
                    destroyRows();
                    appendStoppedChannelDatatable(data);
                    resolve(data)
                }
            });
    })
}


channel_listing()
function destroyRows() {
    $('#stopped_tbody').empty()
    $('#stopped_table').DataTable().rows().remove();
    $("#stopped_table").DataTable().destroy()
}

function appendStoppedChannelDatatable(data) {

    var array = data.data;
    var options_table = "";

    array.forEach(function (element, i) {

        var channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
        var url = element.url ? element.url : "-";
        var serverip = element.ServerIp ? element.ServerIp : "-";
        var serverName = element.serverName ? element.serverName : "-";
        var flag = element.flag ? element.flag : "-";
        var state = element.state ? element.state : "-";

            if (state === 'stopped') {

                state = '<span class="badge badge-danger badge" style="font-size: 12px; width: 50px;">stopped</span>'      
              options_table += `<tr class='channel-overview-tbl-row'  data-channel-name=${channel_Name} data-data="${modified_on}"  style='cursor: pointer' > <td class="index">${i + 1}</td>
           <td class="channel-name" >${channel_Name}</td>
          <td>${modified_on}</td>
          <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
          <td class="${channel_Name}"  data-channel-serverip=${serverip}>${state}</td>
               </tr>`
            }
            if (i == array.length - 1) {
                $('#stopped_tbody').append(options_table)
                reInitializeDataTable()
            }
        })
   // })
}

var channel_listing_table;
function reInitializeDataTable() {
    return new Promise((resolve, reject) => {
        $("#stopped_table").DataTable().destroy()
        channel_listing_table = $('#stopped_table').DataTable({
            // "order": [[2, "desc"]], // for descending order
            // "columnDefs": [
            //     { "width": "40%", "targets": 1 }
            // ]
        })
        resolve()
    })
}

function get_state_of_channel(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/stopChannelListing/get_channel_state`, params,
            function (data, status) {
                if (data.status === 200) {
                    resolve(data.data)
                } else {
                    reject()
                }
            })
    })
}

