

$(document).on("click", ".server-refresh", function () {
    instance_listing()
})


function instance_listing() {
    $.ajax({
        url: '/v1/server/instance_listing',
        type: 'GET',
        headers: {
            'Content-Type': "application/json",
        },
        'success': function (data, textStatus, request) {

            if (data) {
console.log(JSON.stringify(data))
                var _data = data.data;
                destroyRowsServer()
                appendServer(_data)
               // appendServer_backup(_data)
            } else {
                alert(data.message)
            }
        },

        'error': function (request, textStatus, errorThrown) {

            console.log("ERR " + errorThrown)

        }

    })
}
instance_listing();

function destroyRowsServer() {
    $('#ec2_tbody').empty()
    $('#ec2_table').DataTable().rows().remove();
    $("#ec2_table").DataTable().destroy()
}


function appendServer(_data) {
    var Name, State, ID, ARN, InputCount;
    var index = 1;

    $('#ec2_tbody').empty();
    $('#serverlistig_primary').empty();
    $('#serverlistig1').empty();
   // $('#backup-update-server-Ip').empty();
    // $('#serverlistig_backup').empty();

    var string = '';
    var string1 = '';
    var string2 = '';
    var string3 = '';

    //var backupstring= '';

    for (var i = 0; i < _data.Reservations.length; i++) {

        InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";

        state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";

        PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";

        var InstanceName;
        var Uspversion = '1.10.18'
        for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
            if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {
                InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
            }
            if (_data.Reservations[i].Instances[0].Tags[j].Key === "primary") {
                InstanceName_primary = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";

            }
           else if (_data.Reservations[i].Instances[0].Tags[j].Key === "backup") {
                InstanceName_backup = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
               // console.log("InstanceName_backup " + JSON.stringify(InstanceName_backup))
               
                
            }

        }

     string += '<tr id="' + InstanceId + '"> <td>' + index + '</td> <td class="channel-name">' + InstanceName + '</td> <td>'+ Uspversion +'</td><td>' + PublicIpAddress + '</td> <td>' + state + '</td> </tr>'
     string1 += '<a valueIp=' + PublicIpAddress + ' href="javascript:void(0);" class="dropdown-item server-listing">' + InstanceName + '</a>'
     $('#serverlistig1').html(string1);
     index++
       
        $('#ec2_table').DataTable().rows().remove();
        $("#ec2_table").DataTable().destroy()
        $('#ec2_tbody').html(string);
       
        $('#ec2_table').DataTable().destroy()
        $('#ec2_table').DataTable({

        });
  

    }
    string3 += '<a  style="padding-left: 20px;" valueIp=' + PublicIpAddress + ' href="javascript:void(0);" class="server-listing-backup"' + InstanceName + '>' + InstanceName_backup + '</a>'
    $('#serverlistig_backup').html(string3);
   
   

    string2 += '<a style="padding-left: 20px;" valueIp=' + PublicIpAddress + ' href="javascript:void(0);" class="server-listing-primary"' + InstanceName + '>' + InstanceName_primary + '</a>'
    $('#serverlistig_primary').html(string2);

}

$(document).on("click", ".server-listing-primary", function () {

    $.get('/v1/server/instance_listing_serevr',

        function (data, status) {
            if (data.status === 200) {
               destroyRowsServer()
                _data = data.data
              console.log("server-listing-primary"+ JSON.stringify(_data))
                appendServer(_data)

            } else {
                toastr.danger('Operation failed.')
            }
        })
})

$(document).on("click", ".server-listing-backup", function () {

    $.get('/v1/server/server-listing-backup',

        function (data, status) {
            if (data.status === 200) {
                destroyRowsServer()
                _data = data.data
                console.log("server-listing-backup"+ JSON.stringify(_data))
               appendServer(_data)

            } else {
                toastr.danger('Operation failed.')
            }
        })
})


$(document).on("click", ".server-listing", function () {

    var ServerIp = $(this).attr('valueIp')
    // let Ip_params = {
    //     ServerIp: ServerIp
    // }
    alert(JSON.stringify(ServerIp))
    $.get('/v1/server/instance_listing', 

        function (data, status) {
            if (data.status === 200) {
                destroyRowsServer()
                _data = data.data   
                appendServer(_data)

            } else {
                toastr.danger('Operation failed.')
            }
        })
})