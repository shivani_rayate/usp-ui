// $("#seeAnotherField").change(function() {

//     if ($(this).val() == "arn:aws:elasticloadbalancing:ap-south-1:494321523184:listener/app/mcapture-testing/a76c214bcbe70552/2a047919258eab74") {
//       $('#otherFieldDiv').show();
//       $('#otherField').attr('required', '');
//       $('#otherField').attr('data-error', 'This field is required.');
//       $('#otherFieldDiv1').hide();
//       $('#otherField1').removeAttr('required');
//       $('#otherField1').removeAttr('data-error');
//     } else if($(this).val() == "arn:aws:elasticloadbalancing:ap-south-1:705991774343:listener/app/USP-Live-ALB/94ff270fb4cac75b/2362ce98bc36956c"){
//     $('#otherFieldDiv1').show();
//       $('#otherField1').attr('required', '');
//       $('#otherField1').attr('data-error', 'This field is required.');
//       $('#otherFieldDiv').hide();
//       $('#otherField').removeAttr('required');
//       $('#otherField').removeAttr('data-error');

//       } else {
//       $('#otherFieldDiv').hide();
//       $('#otherField').removeAttr('required');
//       $('#otherField').removeAttr('data-error');
//       $('#otherFieldDiv1').hide();
//       $('#otherField1').removeAttr('required');
//       $('#otherField1').removeAttr('data-error');

//     }
//   });
//   $("#seeAnotherField").trigger("change");

// Get All Channel Listing From MongoDb
function alb_channel_listing() {

    return new Promise((resolve, reject) => {
        $.get('/v1/alb/alb_channel_listing',
            function (data, status) {
                if (data.status == 200) {
                    destroyRows();
                    appendChannelDatatable(data);
                    resolve(data)

                }
            });
    })
}


function alb_channel_listing1(createParams) {

    return new Promise((resolve, reject) => {
        $.post('/v1/alb/alb_channel_listing1',
            createParams,
            function (data, status) {
                if (data.status == 200) {

                    resolve(data)

                }
            });
    })
}


function destroyRows() {
    $('#abl_listing_tbody').empty()
    $('#alb_listing_table').DataTable().rows().remove();
    $("#alb_listing_table").DataTable().destroy()
}


$("#channel-creation-form").submit(function (event) {

    var channelName = $('#channel-name').val().trim();

    if (channelName === "GeminiMusic" || channelName === "GeminiTVHD" || channelName === "GeminiLife" || channelName === "KushiTV" || channelName === "SunBangla" || channelName === "GeminiMusicHD" || channelName === "GeminiComedy" || channelName === "GeminiMovies") {
        var target = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Primary-4/b02eccd44b8b0772"

        var TargetArnName = "Production-USP-Primary-4"

    } else if (channelName === "SunMusic" || channelName === "KTV" || channelName === "ChuttiTV" || channelName === "SunLife" || channelName === "GeminiTV" || channelName === "GeminiMoviesHD" || channelName === "SunMusicHD" || channelName === "SunNews") {
        var target = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Primary-3/23b8c81a57ce2341"

        var TargetArnName = "Production-USP-Primary-3"

    } else if (channelName === "SunTV" || channelName === "SunTVHD" || channelName === "KochuTV" || channelName === "UdayaComedy" || channelName === "SuryaTVHD" || channelName === "SuryaMusic" || channelName === "SuryaComedy" || channelName === "SuryaMovies") {
        var target = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Primary-2/49ed0c428db43ef7"

        var TargetArnName = "Production-USP-Primary-2"
    }
    else if (channelName === "UdayaTVHD" || channelName === "AdithyaTV" || channelName === "KTVHD" || channelName === "UdayaMovies" || channelName === "UdayaMusic" || channelName === "ChintuTV" || channelName === "UdayaTV" || channelName === "SuryaTV") {
        var target = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Primary-1/77528d7936df42ef"

        var TargetArnName = "Production-USP-Primary-1"

    }


    let createParams = {
        channelName: channelName,
        Listener: "arn:aws:elasticloadbalancing:ap-south-1:705991774343:listener/app/USP-Live-ALB/94ff270fb4cac75b/2362ce98bc36956c",
        targetArn: target,
        TargetName: TargetArnName
    }


    alb_channel_listing1(createParams).then((channel_listing) => {

        var array = channel_listing.data;

        if (array === undefined || array == null || array.length <= 0) {

            create_channel_on_alb(createParams)

        } else {
            var channelNameComapre = array[0].channelName;
           
            if (createParams.channelName === channelNameComapre) {

                toastr.warning('Channel Name has been already present')
            }
        }
    })
    event.preventDefault()

})


function create_channel_on_alb(createParams) {

    $.post('/v1/alb/create_channel_on_alb', createParams,
        function (data, status) {
            if (data.status === 200) {

                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Channel has been Successfully Created on ALB!',
                    showConfirmButton: false,
                    timer: 1500
                })
                setTimeout(function () {
                    window.location.reload();
                }, 2000);


            } else {
                toastr.danger('Operation failed')
            }
        });
}



$(function () {
    alb_channel_listing()
})
function appendChannelDatatable(data) {

    var array = data.data;
    var options_table = "";

    array.forEach(function (element, i) {

        var channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
        var userRole = $('.pro-user-role').attr("data-userRole")
        var RuleArn_http = element.RuleArn ? element.RuleArn : "-";
        var TargetName = element.TargetName ? element.TargetName : "-";

        if (userRole === 'Admin' || userRole === 'Operator') {

            options_table += `<tr class='channel-overview-tbl-row'   > <td class="index">${i + 1}</td>
               <td class="channel-name" >${channel_Name}</td>
                <td>${modified_on}</td>
                <td>${TargetName}</td>
              <td><a data-arn="${RuleArn_http}"  data-channel="${channel_Name}" class='far fa-trash-alt alb_channel_delete'  href="#"></a></td>
                  
                   </tr>`

        }
        else if (userRole === 'Monitoring') {


            options_table += `<tr class='channel-overview-tbl-row'   > <td class="index">${i + 1}</td>
               <td class="channel-name" >${channel_Name}</td>
                <td>${modified_on}</td>
                <td>${TargetName}</td>
              <td style="display:none"><a data-arn="${RuleArn_http}" class='far fa-trash-alt alb_channel_delete'  href="#"></a></td>
                  
                   </tr>`

        }

        if (i == array.length - 1) {
            $('#abl_listing_tbody').append(options_table)
            reInitializeDataTable()
        }
    })
}

var alb_listing_table;
function reInitializeDataTable() {
    return new Promise((resolve, reject) => {
        $("#alb_listing_table").DataTable().destroy()
        alb_listing_table = $('#alb_listing_table').DataTable({
        })
        resolve()
    })
}



$(document).on("click", ".alb_channel_delete", function (event) {


    var RuleArn_http = $(this).attr('data-arn');
    var channelName = $(this).attr('data-channel');

    var _deleteParams = {
        RuleArn_http: RuleArn_http,

    }
    
    swal.fire({
        title: `${channelName},\n Are you sure?`,
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            delete_primaryChannel_from_alb(_deleteParams)
        }
    })
})


// delete_primaryChannel_from_alb
function delete_primaryChannel_from_alb(_deleteParams) {

    $.post('/v1/alb/delete_primaryChannel_from_alb', _deleteParams,
        function (data, status) {
            if (data.status === 200) {
                swal.fire(
                    'Deleted!',
                    'Channel has been deleted On ALB',
                    'success'
                )
                setTimeout(function () {
                    window.location.reload();
                }, 2000);


            } else {
                toastr.danger('Operation failed')
            }
        });

    event.preventDefault()
}

