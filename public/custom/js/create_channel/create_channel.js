

// Get All Channel Listing From MongoDb
function channel_listing() {
    return new Promise((resolve, reject) => {
        $.get('/v1/livestreaming/channel_listing',
            function (data, status) {
                if (data.status == 200) {
                    destroyRows();
                    appendChannelDatatable(data);
                    resolve(data)

                }
            });
    })
}

// Primary Channel Listing On Click Event
$(document).on("click", ".primary", function (e) {
    channel_listing()
})

var PrimaryServere = ''
// Get server Listing 
function instance_listing() {
    $.ajax({
        url: '/v1/server/instance_listing_serevr',
        type: 'GET',
        headers: {
            'Content-Type': "application/json",
        },
        'success': function (data, textStatus, request) {
            if (data) {
                var _data = data.data;
                var Name, State, ID, ARN, InputCount;
                $('#server-Ip').empty();
                $('#serverlistig').empty();
                //  $('#divcheckbox').empty();
                $('#updatedivcheckbox').empty();
                $('#update-server-Ip').empty();
                var string = '';
                var string2 = '';
                // var string3 = '';
                var stringupdate = '';
                //  var stringbackup = '';
                for (var i = 0; i < _data.Reservations.length; i++) {
                    InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";
                    state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";
                    PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";
                    var InstanceName;
                    for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
                        if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {
                            InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
                        }
                    }
                    string += '<option class="server-name" value=' + PublicIpAddress + ' servername=' + InstanceName + ' >' + InstanceName + '</option>'
                    string2 += '<a valueIp=' + PublicIpAddress + ' href="javascript:void(0);" class="dropdown-item server-listing-button">' + InstanceName + '</a>'
                    // string3 += '<div class="checkbox checkbox-success form-check-inline"> <input type="checkbox" name="fname" class="checkboxbackup" id="checkboxbackup" serverkeywordname=' + InstanceName + ' onclick="check()" value="option1"> <label for="inlineCheckbox2"> Backup Server </label> </div>'
                    stringupdate += '<option class="server-name" value=' + PublicIpAddress + ' servername=' + InstanceName + ' >' + InstanceName + '</option>'
                    //  stringbackup += '<div class="checkbox checkbox-success form-check-inline updatebackup"> <input type="checkbox" style="margin-top: 90px;" class="inlineCheckbox21" serverkeywordname= ' + InstanceName + ' id="inlineCheckbox21" value="option1"  > <label for="inlineCheckbox21">Update Backup Server </label> </div>'
                    $('#server-Ip').html(string);
                    $('#serverlistig').html(string2);
                    //  $('#divcheckbox').html(string3);
                    //  $('#updatedivcheckbox').html(stringbackup);
                    $('#update-server-Ip').html(stringupdate);
                    primaryInstanceName = InstanceName;
                }
            } else {
                // alert(data.message)
            }
        },
        'error': function (request, textStatus, errorThrown) {
            console.log("ERR " + errorThrown)
        }
    })
}
instance_listing()

// checkbox code date:14/05/2020 
function check() {
    if (document.getElementById("checkboxbackup").checked) {

        var ServerKeyword = $("#server-Ip option:selected").text();
        var BackupServerName = ServerKeyword.replace("Primary", "Backup");

        $.get(`/v1/server/server-listing-backup`,
            function (data, status) {
                if (data.status === 200) {
                    var _data = data.data;
                    $('#backupserver-div').empty();
                    $('#update-backupserver-div').empty();
                    var backup_string = '';

                    for (var i = 0; i < _data.Reservations.length; i++) {
                        InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";
                        state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";
                        PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";
                        var InstanceName;
                        for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
                            if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {
                                InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
                            }
                        }
                        let backup_params = {
                            PublicIpAddress: PublicIpAddress,
                            InstanceName: InstanceName,
                            BackupServerName: BackupServerName
                        }

                        if (InstanceName === BackupServerName) {
                            backup_string += '<option class="server-name" id="backupserver-div"  value=' + PublicIpAddress + ' backupservername=' + InstanceName + ' >' + InstanceName + '</option>'
                            $('#backupserver-div').append(backup_string);

                        } else {
                            //  $('#update-backupserver-div').empty();
                        }
                    }
                    // resolve(data.data)
                } else {
                    // reject()
                }
            })
        $('#backupOnCheck').removeClass("force-hidden");
    } else {
        $('#backupserver-div').empty();
        $('#backupserver-div').html('Empty');
        var abcd = $('#backupserver-div').html();
        $("#backupOnCheck").addClass("force-hidden");
    }
}

var terminationvariable = "Disable";
function checktermination() {

    if (document.getElementById('termination-checkbox').checked) {
        terminationvariable = "Enable";

    } else {
        terminationvariable = "Disable";

    }
}



// $(document).ready(function(){
// Channel Creation Click Event 
$("#channel-creation-form").submit(function (event) {

    var backupserverIpTextboxValue = $('#backupserver-div').text().trim();

    var serverName = $('option:selected', this).attr('servername');
    if (serverName === "Production-USP-Primary-1") {
        PrimarytargetGroup = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Primary-1/77528d7936df42ef"
    }
    else if (serverName === "Production-USP-Primary-2") {
        PrimarytargetGroup = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Primary-2/49ed0c428db43ef7"
    }
    else if (serverName === "Production-USP-Primary-3") {
        PrimarytargetGroup = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Primary-3/23b8c81a57ce2341"
    }
    else if (serverName === "Production-USP-Primary-4") {
        PrimarytargetGroup = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Primary-4/b02eccd44b8b0772"
    }


    if (backupserverIpTextboxValue === 'Empty') {
        let params = {
            serverIp: $('#server-Ip').val().trim(),
            backupserverIp: $('#backupserver-div').text().trim(),
            backupserverName: $('#backupserver-div').text().trim(),
            channelName: $('#channel-name').val().trim(),
            creator: $('#creator').val().trim(),
            lookaheadFragments: $('#lookahead-fragments').val().trim(),
            dvrWindowLength: $('#dvr-window-length').val().trim(),
            archiveSegmentLength: $('#archive-segment-length').val().trim(),
            archiving: $('#archiving').val().trim(),
            archiveLength: $('#archivelength').val().trim(),
            issMinimumFragmentLength: $('#iss_minimum_fragment_length').val().trim(),
            hlsMinimumFragmentLength: $('#hls_minimum_fragment_length').val().trim(),
            hlsNoAudioOnly: $('#hls_no_audio_only').val().trim(),
            terminationPolicy: terminationvariable,
            state: "idle"
        }

        var xmlData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language"> <head> <meta name="clientManifestRelativePath" content="${params.channelName}.ismc" /> <meta name="creator" content="${params.creator}" /> <meta name="lookahead_fragments" content="${params.lookaheadFragments}" /> <meta name="dvr_window_length" content="${params.dvrWindowLength}" /> <meta name="archive_segment_length" content="${params.archiveSegmentLength}" /> <meta name="archiving" content="${params.archiving}" /> <meta name="archive_length" content="${params.archiveLength}" /> <meta name="restart_on_encoder_reconnect" content="true" /> <meta name="iss_minimum_fragment_length" content="${params.issMinimumFragmentLength}" /> <meta name="hls_minimum_fragment_length" content="${params.hlsMinimumFragmentLength}" /> <meta name="hls_no_audio_only" content="${params.hlsNoAudioOnly}" /> </head> <body> <switch> </switch> </body> </smil>`;
        var _flag = $(this).find("#checkboxbackup").is(":checked");
        var restartOnEncoderReconnect = 'true'.toString();
        let _params = {
            serverIp: params.serverIp,
            serverName: serverName,
            PrimarytargetGroup: PrimarytargetGroup,
            backupserverIp: params.backupserverIp,
            backupserverName: params.backupserverName,
            xmlData: xmlData,
            xmlFileName: params.channelName,
            channelName: params.channelName,
            creator: params.creator,
            lookaheadFragments: params.lookaheadFragments,
            dvrWindowLength: params.dvrWindowLength,
            archiveSegmentLength: params.archiveSegmentLength,
            archiving: params.archiving,
            archiveLength: params.archiveLength,
            restartOnEncoderReconnect: restartOnEncoderReconnect,
            issMinimumFragmentLength: params.issMinimumFragmentLength,
            hlsMinimumFragmentLength: params.hlsMinimumFragmentLength,
            hlsNoAudioOnly: params.hlsNoAudioOnly,
            _flag: _flag.toString(),
            terminationPolicy: params.terminationPolicy,
            state: params.state.toString()

        }

        channel_listing().then((channel_listing) => {
            var array = channel_listing.data;
            for (var i = 0; i < array.length; i++) {
                var channel_Name = array[i].channelName ? array[i].channelName : "-";

                if (_params.channelName === channel_Name) {
                    toastr.warning('Channel Name has been already present')
                }
            }

            create_Channel_On_Primary(_params)
        })
    }
    else {
        var backupserverName = $('#backupserver-div').text().trim();
        let params = {
            serverIp: $('#server-Ip').val().trim(),
            serverName: $('option:selected', this).attr('servername'),
            backupserverIp: $('#backupserver-div').val().trim(),
            channelName: $('#channel-name').val().trim(),
            creator: $('#creator').val().trim(),
            lookaheadFragments: $('#lookahead-fragments').val().trim(),
            dvrWindowLength: $('#dvr-window-length').val().trim(),
            archiveSegmentLength: $('#archive-segment-length').val().trim(),
            archiving: $('#archiving').val().trim(),
            archiveLength: $('#archivelength').val().trim(),
            issMinimumFragmentLength: $('#iss_minimum_fragment_length').val().trim(),
            hlsMinimumFragmentLength: $('#hls_minimum_fragment_length').val().trim(),
            hlsNoAudioOnly: $('#hls_no_audio_only').val().trim(),
            terminationPolicy: terminationvariable,
            state: "idle"
        }
        if (backupserverName === "Production-USP-Backup-1") {
            BackupTargetGroup = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Backup-1/775f9e30bb380446"
        } else if (backupserverName === "Production-USP-Backup-2") {
            BackupTargetGroup = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Backup-2/224303d0e6b3b5b8"
        } else if (backupserverName === "Production-USP-Backup-3") {
            BackupTargetGroup = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Backup-3/e1a7b3d7d734f70f"
        } else if (backupserverName === "Production-USP-Backup-4") {
            BackupTargetGroup = "arn:aws:elasticloadbalancing:ap-south-1:705991774343:targetgroup/Production-USP-TG-Backup-4/250d8d42a9e2970f"
        }

        // var xmlData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language"> <head> <meta name="clientManifestRelativePath" content="${params.channelName}.ismc" /> <meta name="creator" content="${params.creator}" /> <meta name="lookahead_fragments" content="${params.lookaheadFragments}" /> <meta name="dvr_window_length" content="${params.dvrWindowLength}" /> <meta name="archive_segment_length" content="${params.archiveSegmentLength}" /> <meta name="archiving" content="${params.archiving}" /> <meta name="archive_length" content="${params.archiveLength}" /> <meta name="restart_on_encoder_reconnect" content="true" /> <meta name="iss_minimum_fragment_length" content="${params.issMinimumFragmentLength}" /> <meta name="hls_minimum_fragment_length" content="${params.hlsMinimumFragmentLength}" /> </head> <body> <switch> </switch> </body> </smil>`;
        var xmlData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language"> <head> <meta name="clientManifestRelativePath" content="${params.channelName}.ismc" /> <meta name="creator" content="${params.creator}" /> <meta name="lookahead_fragments" content="${params.lookaheadFragments}" /> <meta name="dvr_window_length" content="${params.dvrWindowLength}" /> <meta name="archive_segment_length" content="${params.archiveSegmentLength}" /> <meta name="archiving" content="${params.archiving}" /> <meta name="archive_length" content="${params.archiveLength}" /> <meta name="restart_on_encoder_reconnect" content="true" /> <meta name="iss_minimum_fragment_length" content="${params.issMinimumFragmentLength}" /> <meta name="hls_minimum_fragment_length" content="${params.hlsMinimumFragmentLength}" /> <meta name="hls_no_audio_only" content="${params.hlsNoAudioOnly}" /> </head> <body> <switch> </switch> </body> </smil>`;
        var _flag = $(this).find("#checkboxbackup").is(":checked");
        var restartOnEncoderReconnect = 'true'.toString();
        let _params = {
            serverIp: params.serverIp,
            serverName: params.serverName,
            PrimarytargetGroup: PrimarytargetGroup,
            backupserverIp: params.backupserverIp,
            backupserverName: backupserverName,
            xmlData: xmlData,
            xmlFileName: params.channelName,
            channelName: params.channelName,
            creator: params.creator,
            lookaheadFragments: params.lookaheadFragments,
            dvrWindowLength: params.dvrWindowLength,
            archiveSegmentLength: params.archiveSegmentLength,
            archiving: params.archiving,
            archiveLength: params.archiveLength,
            restartOnEncoderReconnect: restartOnEncoderReconnect,
            issMinimumFragmentLength: params.issMinimumFragmentLength,
            hlsMinimumFragmentLength: params.hlsMinimumFragmentLength,
            hlsNoAudioOnly: params.hlsNoAudioOnly,
            _flag: _flag.toString(),
            terminationPolicy: params.terminationPolicy,
            state: params.state.toString(),
            BackupTargetGroup: BackupTargetGroup
        }

        channel_listing().then((channel_listing) => {
            var array = channel_listing.data;
            for (var i = 0; i < array.length; i++) {
                var channel_Name = array[i].channelName ? array[i].channelName : "-";

                if (_params.channelName === channel_Name) {
                    toastr.warning('Channel Name has been already present')
                }
            }
            create_Channel_On_Primary(_params)
            create_Channel_On_Backup(_params)
        })
    }

    event.preventDefault()
})



$("#channeldeleteProtectionform").submit(function (event) {

    let params = {
        Termination_Policy: $('#Termination_Policy').val().trim(),
    }
    let _params = {
        Termination_Policy: params.Termination_Policy,
    }
    event.preventDefault()
})

function stateWiseEmailSend(params) {

    $.post('/v1/livestreaming/stateWiseEmailSend',
        params,
        function (data, status) {
            if (data.status === 200) {
            } else {
                toastr.danger('Email failed.')
            }

        });
}


// Channel Creation On Primary Server
function create_Channel_On_Primary(_params) {

    $.post('/v1/livestreaming/create_Channel_On_Primary', _params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                _data = data.data;

                createdChannelName = _data.message.channelName;
                var serverIp = _data.message.ServerIp

                params = {
                    createdChannelName: createdChannelName,
                    serverIp: serverIp,
                    description: ` channel has been successfully created and state is Idle`
                }
                whatsapp(params)
                SlackMsg(params)
                stateWiseEmailSend(params)

                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Channel has been Successfully Created!',
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function () {
                    window.location.reload();
                }, 3000);
                let userparams = {
                    createdChannelName: createdChannelName,
                    activity: 'Create Channel On Primary'
                }
                userAcitivity(userparams)


            } else {
                toastr.danger('Creation failed.')
            }
        })
}

// Channel Creation On Backup Server
function create_Channel_On_Backup(_params) {

    $.post('/v1/livestreaming/create_Channel_On_Backup', _params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                _data = data.data
                createdChannelName = _data.message.channelName
                _flag = 'true'

                let userparams = {
                    createdChannelName: createdChannelName,
                    activity: 'Create Channel On Backup'
                }
                userAcitivity(userparams)

                var params = {
                    createdChannelName: createdChannelName,
                    description: ` channel has been successfully created and state is Idle`
                }
                whatsapp(params)
                SlackMsg(params)
                stateWiseEmailSend(params)

                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Channel has been Successfully Created!',
                    showConfirmButton: false,
                    timer: 1500
                })
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            } else {
                toastr.danger('Creation failed.')
            }
        })
}



function instance_backup_listing() {

    $.ajax({
        url: '/v1/server/server-listing-backup',
        type: 'GET',
        headers: {
            'Content-Type': "application/json",
        },
        'success': function (data, textStatus, request) {

            if (data) {

                var _data = data.data;
                var Name, State, ID, ARN, InputCount;

                $('#backup-server-Ip').empty();

                var backup_string = '';

                for (var i = 0; i < _data.Reservations.length; i++) {

                    InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";
                    state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";
                    PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";
                    var InstanceName;
                    for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
                        if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {

                            InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
                        }
                    }
                    backup_string += '<option class="server-name" value=' + PublicIpAddress + ' backupservername=' + InstanceName + ' >' + InstanceName + '</option>'
                    $('#backup-server-Ip').html(backup_string);
                }

            } else {
                console.log(data.message)
            }
        },
        'error': function (request, textStatus, errorThrown) {
            console.log("ERR " + errorThrown)
        }
    })
}
instance_backup_listing();

$(document).on("click", ".backup_Channel", function (e) {
    var channelName = $(this).attr('data-channel-name')
    let _params = {
        channelName: channelName
    }
    getDataFromMongoForBackup(_params)
})


var primaryname = '';
var primaryip = '';
var termination = '';
function getDataFromMongoForBackup(_params) {
    $.post('/v1/livestreaming/update-channel', _params,
        function (data, status) {
            if (data.status === 200) {
                let _data = data.data;
                // $('#backup-server-Ip').val().trim(),
                $('#backup-channel-name').val(_data.channelName)
                $('#backup-creator').val(_data.creator)
                $('#backup-lookahead-fragments').val(_data.lookaheadFragments)
                $('#backup-dvr-window-length').val(_data.dvrWindowLength)
                $('#backup-archive-segment-length').val(_data.archiveSegmentLength)
                $('#backup-archiving').val(_data.archiving)
                $('#backup-archive-length').val(_data.archiveLength)
                $('#backup-restart_on_encoder_reconnect').val(_data.restartOnEncoderReconnect)
                $('#backup-iss_minimum_fragment_length').val(_data.issMinimumFragmentLength)
                $('#backup-hls_minimum_fragment_length').val(_data.hlsMinimumFragmentLength)
                $('#backup-hls_no_audio_only').val(_data.hlsNoAudioOnly),
                    $('#backupDeletion-Protection').val(_data.terminationPolicy)
                primaryname = _data.serverName;
                termination = _data.terminationPolicy

                var BackupServerName = primaryname.replace("Primary", "Backup");
                backupservername = $('#backupservernmae').val(BackupServerName)

                if (_data.terminationPolicy === 'Enable') {

                    options_backuptermination = ""
                    options_backuptermination += `<label class="switch">
                <input type="checkbox" checked  onclick="checkterminationBackupupdation()" id='updationBackupterminationcheckbox' disabled>
                    <span class="slider round" ></span>
                  </label>`

                    $('#updateBackupToggle').html(options_backuptermination)
                } else {

                    options_backuptermination = ""
                    options_backuptermination += `<label class="switch">
                    <input type="checkbox" onclick="checkterminationBackupupdation()" id='updationBackupterminationcheckbox' disabled>
                      <span class="slider round" ></span>
                    </label>`

                    $('#updateBackupToggle').html(options_backuptermination)

                }
            } else {
                toastr.error(data.message);
            }
        })
    event.preventDefault()
}


var terminationUpdationvariable = "";
function checkterminationupdation() {

    if (document.getElementById('updationtermination-checkbox').checked) {
        terminationUpdationvariable = "Enable";
    } else {
        terminationUpdationvariable = "Disable";

    }
}


// var terminationbackupUpdationvariable = "Disable";
// function checkterminationBackupupdation() {

//     if (document.getElementById('updationBackupterminationcheckbox').checked) {

//         terminationbackupUpdationvariable = "Enable";
//         alert("I m if")

//     } else {
//         terminationbackupUpdationvariable = "Disable";
//         alert("i m in else")

//     }
// }

// Create Channel On Backup Server And MongoDb
$("#backup-channel-creation-form").submit(function (event) {

    var name = primaryname;
    var BackupServerName = name.replace("Primary", "Backup");
    $.get(`/v1/server/server-listing-backup`,
        function (data, status) {
            if (data.status === 200) {
                var _data = data.data;
                for (var i = 0; i < _data.Reservations.length; i++) {
                    InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";
                    state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";
                    PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";
                    var InstanceName;
                    for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
                        if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {
                            InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
                        }
                    }
                    if (InstanceName === BackupServerName) {
                        var vip = PublicIpAddress;
                        var params = {
                            channelName: $('#backup-channel-name').val().trim(),
                            creator: $('#backup-creator').val().trim(),
                            lookaheadFragments: $('#backup-lookahead-fragments').val().trim(),
                            dvrWindowLength: $('#backup-dvr-window-length').val().trim(),
                            archiveSegmentLength: $('#backup-archive-segment-length').val().trim(),
                            archiving: $('#backup-archiving').val().trim(),
                            archiveLength: $('#backup-archive-length').val().trim(),
                            issMinimumFragmentLength: $('#backup-iss_minimum_fragment_length').val().trim(),
                            hlsMinimumFragmentLength: $('#backup-hls_minimum_fragment_length').val().trim(),
                            hlsNoAudioOnly: $('#backup-hls_no_audio_only').val().trim(),
                            terminationPolicy: termination,
                            state: "idle"
                        }

                        var xmlData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language"> <head> <meta name="clientManifestRelativePath" content="${params.channelName}.ismc" /> <meta name="creator" content="${params.creator}" /> <meta name="lookahead_fragments" content="${params.lookaheadFragments}" /> <meta name="dvr_window_length" content="${params.dvrWindowLength}" /> <meta name="archive_segment_length" content="${params.archiveSegmentLength}" /> <meta name="archiving" content="${params.archiving}" /> <meta name="archive_length" content="${params.archiveLength}" /> <meta name="restart_on_encoder_reconnect" content="true" /> <meta name="iss_minimum_fragment_length" content="${params.issMinimumFragmentLength}" /> <meta name="hls_minimum_fragment_length" content="${params.hlsMinimumFragmentLength}" /> </head> <body> <switch> </switch> </body> </smil>`;
                        var restartOnEncoderReconnect = 'true'.toString();
                        let _params = {
                            backupserverIp: vip,
                            backupserverName: BackupServerName,
                            xmlData: xmlData,
                            xmlFileName: params.channelName,
                            channelName: params.channelName,
                            creator: params.creator,
                            lookaheadFragments: params.lookaheadFragments,
                            dvrWindowLength: params.dvrWindowLength,
                            archiveSegmentLength: params.archiveSegmentLength,
                            archiving: params.archiving,
                            archiveLength: params.archiveLength,
                            restartOnEncoderReconnect: restartOnEncoderReconnect,
                            issMinimumFragmentLength: params.issMinimumFragmentLength,
                            hlsMinimumFragmentLength: params.hlsMinimumFragmentLength,
                            terminationPolicy: params.terminationPolicy,
                            hlsNoAudioOnly: params.hlsNoAudioOnly,
                            state: params.state.toString()

                        }

                        $.post('/v1/livestreaming/create_Channel_On_Backup', _params,

                            function (data, textStatus, request) {
                                if (data.status === 200) {
                                    _data = data.data
                                    createdChannelName = _data.message.channelName
                                    _flag = 'true'

                                    let userparams = {
                                        createdChannelName: createdChannelName,
                                        activity: 'Create Channel On Backup'
                                    }
                                    userAcitivity(userparams)

                                    var params = {
                                        createdChannelName: createdChannelName,
                                        description: ` channel has been successfully created & state is Idle`
                                    }
                                    whatsapp(params)
                                    SlackMsg(params)
                                    stateWiseEmailSend(params)

                                    let _params = {
                                        _flag: _flag.toString(),
                                        xmlFileName: createdChannelName
                                    }
                                    primaryupdateonmongo(_params);
                                    Swal.fire({
                                        position: 'centre',
                                        type: 'success',
                                        title: 'Channel has been Successfully Created!',
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 2000);
                                } else {
                                    toastr.danger('Creation failed.')
                                }
                            })
                    }
                }
            }
        })
    event.preventDefault()
})



// Update flag in Mongo After Create channel on Primary
function primaryupdateonmongo(_params) {
    $.post('/v1/livestreaming/primaryupdateonmongo', _params,
        function (data, textStatus, request) {
            if (data.status === 200) {
            } else {
                toastr.danger('Updation Failed')
            }
        });
}


// Get State Of Channel Listing From Server
function get_state_of_channel(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/get_channel_state`, params,
            function (data, status) {

                if (data.status === 200) {

                    resolve(data.data)

                } else {
                    reject()
                }
            })
    })
}

// document.ready
$(function () {
    channel_listing()
    instance_listing()
})

$(document).on("click", ".server-listing-button", function () {

    var ServerIp = $(this).attr('valueIp')
    let Ip_params = {
        ServerIp: ServerIp
    }
    $.post('/v1/livestreaming/channel-listing-byIp', Ip_params,
        function (data, status) {
            if (data.status === 200) {
                destroyRows()
                appendChannelDatatable(data)
            } else {
                toastr.danger('Operation failed.')
            }
        })
})

$(document).on("click", ".refresh", function () {
    channel_listing()
})


function destroyRows() {
    $('#channel_listing_tbody').empty()
    $('#channel_listing_table').DataTable().rows().remove();
    $("#channel_listing_table").DataTable().destroy()
}


function appendChannelDatatable(data) {

    var array = data.data;
    var options_table = "";

    array.forEach(function (element, i) {

        var channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
        var url = element.url ? element.url : "-";
        var serverip = element.ServerIp ? element.ServerIp : "-";
        var serverName = element.serverName ? element.serverName : "-"
        var flag = element.flag ? element.flag : "-"
        var Termination_Policy = element.terminationPolicy ? element.terminationPolicy : "-";
        var RuleArn_http = element.RuleArn_http ? element.RuleArn_http : "-";
        var RuleArn_https = element.RuleArn_https ? element.RuleArn_https : "-";
        var userRole = $('.pro-user-role').attr("data-userRole")

        if (userRole === 'Monitoring') {

            if (flag === 'false') {

                options_table += `<tr class='channel-overview-tbl-row'  data-channel-name=${channel_Name} data-data="${modified_on}" flag-value=${flag} style='cursor: pointer' > <td class="index">${i + 1}</td>
               <td class="channel-name" >${channel_Name}</td>
              <td>${modified_on}</td>
              <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
              <td class="${channel_Name}"  data-channel-serverip=${serverip}></td>
              <td>${flag}</td>
              <td class="asset-action-td">
                <div class="dropdown"> 
                <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
                 <div class="dropdown-menu dropdown-menu-right">
               
                   <a href="#" data-channel-name=${channel_Name} publishing-channel-url=${url} class="dropdown-item channel_details">Details</a>
                  
                   </div> </div> 
                   </td>
            
                   <td data-toggle="modal" data-target="#play-video-channel" data-channel-name=${channel_Name} publishing-channel-url=${url}><i data-channel-name=${channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-channelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                   </tr>`

            }
            else {

                options_table += `<tr class='channel-overview-tbl-row'  data-channel-name=${channel_Name} data-data="${modified_on}" flag-value=${flag} style='cursor: pointer'> <td class="index">${i + 1}</td>
             <td class="channel-name">${channel_Name}</td>
               <td>${modified_on}</td>
               <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
               <td class="${channel_Name}"  data-channel-serverip=${serverip}></td>
               <td>${flag}</td>
               <td class="asset-action-td">
               <div class="dropdown"> 
               <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
                <div class="dropdown-menu dropdown-menu-right">
              
                  <a href="#" data-channel-name=${channel_Name} publishing-channel-url=${url} class="dropdown-item channel_details">Details</a>
                 
                  </div> </div> 
                  </td>
             
                    <td data-toggle="modal" data-target="#play-video-channel" data-channel-name=${channel_Name} publishing-channel-url=${url}><i data-channel-name=${channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-channelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                    </tr>`
            }

        } else if (userRole === 'Operator' || userRole === 'Admin') {

            if (flag === 'false') {

                options_table += `<tr class='channel-overview-tbl-row'  data-channel-name=${channel_Name} data-data="${modified_on}" flag-value=${flag} style='cursor: pointer' > <td class="index">${i + 1}</td>
               <td class="channel-name" >${channel_Name}</td>
              <td>${modified_on}</td>
              <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
              <td class="${channel_Name}"  data-channel-serverip=${serverip}></td>
              <td>${flag}</td>
               <td class="asset-action-td">
                <div class="dropdown"> 
                <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
                 <div class="dropdown-menu dropdown-menu-right" >
                 <a href="#" data-channel-name=${channel_Name} class="dropdown-item update_channel" data-toggle="modal" data-target="#UpdateModalbox">Edit</a>
                  <a href="#" data-channel-name=${channel_Name}  data-channel-serverip=${serverip} data-channel-termination-policy=${Termination_Policy} data-channel-ruleArn_http=${RuleArn_http} data-channel-ruleArn_https=${RuleArn_https} class="dropdown-item delete_channel">Delete</a>
                   <a href="#" data-channel-name=${channel_Name} publishing-channel-url=${url} class="dropdown-item channel_details">Details</a>
                   <a href="#" data-channel-name=${channel_Name} publishing-channel-url=${url} class="dropdown-item backup_Channel" data-toggle="modal" data-target="#backupchannel">Create Backup Channel</a>
                   </div> </div> 
                   </td>
                   <td data-toggle="modal" data-target="#play-video-channel" data-channel-name=${channel_Name} publishing-channel-url=${url}><i data-channel-name=${channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-channelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                   </tr>`

            }
            else {

                options_table += `<tr class='channel-overview-tbl-row'  data-channel-name=${channel_Name} data-data="${modified_on}" flag-value=${flag} style='cursor: pointer'> <td class="index">${i + 1}</td>
             <td class="channel-name">${channel_Name}</td>
               <td>${modified_on}</td>
               <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
               <td class="${channel_Name}"  data-channel-serverip=${serverip}></td>
               <td>${flag}</td>
                <td class="asset-action-td">
                 <div class="dropdown"> 
                 <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
                  <div class="dropdown-menu dropdown-menu-right" ><a href="#" data-channel-name=${channel_Name} class="dropdown-item update_channel" data-toggle="modal" data-target="#UpdateModalbox">Edit</a>
                   <a href="#" data-channel-name=${channel_Name}  data-channel-serverip=${serverip} data-channel-termination-policy=${Termination_Policy}  data-channel-ruleArn_http=${RuleArn_http} data-channel-ruleArn_https=${RuleArn_https} class="dropdown-item delete_channel">Delete</a>
                    <a href="#" data-channel-name=${channel_Name} publishing-channel-url=${url} class="dropdown-item channel_details">Details</a>
 
                    </div> </div> 
                    </td>
                    <td data-toggle="modal" data-target="#play-video-channel" data-channel-name=${channel_Name} publishing-channel-url=${url}><i data-channel-name=${channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-channelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                    </tr>`
            }
        }
        if (i == array.length - 1) {
            $('#channel_listing_tbody').append(options_table)
            reInitializeDataTable().then(() => {
                initiateAssetSelection();
            })
        }
    })
}


$(document).on("click", "#getdataclose", function () {
    // remove enlarge sidebar first
    $("#body-content").removeClass("enlarged");
    $('#channel-listing-div').removeClass("force-hidden")
    $('#getquery').addClass("force-hidden")
})

var channel_listing_table;
function reInitializeDataTable() {
    return new Promise((resolve, reject) => {
        $("#channel_listing_table").DataTable().destroy()
        channel_listing_table = $('#channel_listing_table').DataTable({
        })
        resolve()
    })
}


function initiateAssetSelection() {
    $("#channel_listing_table tbody tr:first").addClass("active");
}

$(document).on("click", ".channel-overview-tbl-row", function (e) {

    $('.channel-overview-tbl-row').removeClass("active");
    $(this).addClass("active");

    // ########## child rows code ########### //
    var tr = $(this).closest('tr');
    var row = channel_listing_table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    } else {
        var channelName = $(this).attr('data-channel-name')
        getbackupChannel(channelName).then((channelData) => {
            if (channel_listing_table.row('.shown').length) {
                var _tr = $('tr.shown');
                var _row = channel_listing_table.row(_tr);
                _row.child.hide();
                _tr.removeClass('shown');
            }
            formatChannelDetails(channelData).then((channelDetails) => {
                // Open this row
                row.child(channelDetails).show();
                tr.addClass('shown');
            })

        })
    }
})


//Get State Of Channel From Backup Server
function getbackupChannel(channelName) {
    return new Promise((resolve, reject) => {
        $.post('/v1/livestreaming/find-backup-channel',
            {
                channelName: channelName
            },
            function (data, status) {
                if (data.status == 200) {
                    let _data = data.data;

                    resolve(_data)
                } else if (data.status == 400) {
                    reject()
                }
            })
    });
}


// Append Select backup Channel
function formatChannelDetails(channelData) {
    return new Promise((resolve, reject) => {
        array = channelData;
        if (array !== null) {
            let backup_channel_Name = array.channelName;
            let modified_on = moment(array.created_at).format('lll');
            let url = array.url;
            let serverip = array.backupserverIp;
            let serverName = array.backupserverName;
            let flag = array.flag ? array.flag : "-";
            var Termination_Policy = array.terminationPolicy ? array.terminationPolicy : "-";
            var RuleArn_http = array.RuleArn_http ? array.RuleArn_http : "-";
            var RuleArn_https = array.RuleArn_https ? array.RuleArn_https : "-";
            let data_append = '';
            let formatedAssetDetails = '';
            var userRole = $('.pro-user-role').attr("data-userRole")

            if (userRole === 'Monitoring') {

                data_append += `<td class="channel-name" style="padding-left: 66px;">${backup_channel_Name}</td>
                <td style="padding-left: 68px;">${modified_on}</td>
                <td  data-channel-serverip=${serverip} style="padding-left: 70px;">${serverName}</td>
                <td id="${backup_channel_Name}" data-channel-serverip=${serverip} style="padding-left: 68px;"></td>
                <td style="padding-left: 37px;">${flag}</td>
                 <td class="asset-action-td pl-5" >
                  <div class="dropdown" style="padding-left: 98px;"> 
                  <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
                   <div class="dropdown-menu dropdown-menu-right">
                  
                     <a href="#" data-channel-name=${backup_channel_Name} publishing-channel-url=${url} class="dropdown-item backup_channel_details">Details</a>
                     </div> </div> 
                     </td>
                     <td data-toggle="modal" data-target="#play-video-channel" data-channel-name=${backup_channel_Name} publishing-channel-url=${url} style="padding-left: 79px;"><i data-channel-name=${backup_channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-backupChannelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                     </tr>`

            } else if (userRole === 'Operator' || userRole === 'Admin') {

                data_append += `<td class="channel-name" style="padding-left: 66px;">${backup_channel_Name}</td>
                <td style="padding-left: 68px;">${modified_on}</td>
                <td  data-channel-serverip=${serverip} style="padding-left: 70px;">${serverName}</td>
                <td id="${backup_channel_Name}" data-channel-serverip=${serverip} style="padding-left: 68px;"></td>
                <td style="padding-left: 37px;">${flag}</td>
                 <td class="asset-action-td pl-5" >
                  <div class="dropdown" style="padding-left: 98px;"> 
                  <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
                   <div class="dropdown-menu dropdown-menu-right"><a href="#" data-channel-name=${backup_channel_Name} class="dropdown-item update_channel" data-toggle="modal" data-target="#UpdateModalbox">Edit</a>
                    <a href="#" data-channel-name=${backup_channel_Name}  data-channel-serverip=${serverip} data-channel-termination-policy=${Termination_Policy} data-channel-ruleArn_http=${RuleArn_http} data-channel-ruleArn_https=${RuleArn_https} class="dropdown-item delete_backup_channel">Delete</a>
                     <a href="#" data-channel-name=${backup_channel_Name} publishing-channel-url=${url} class="dropdown-item backup_channel_details">Details</a>
                     </div> </div> 
                     </td>
                   
                     <td data-toggle="modal" data-target="#play-video-channel" data-channel-name=${backup_channel_Name} publishing-channel-url=${url} style="padding-left: 79px;"><i data-channel-name=${backup_channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-backupChannelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                     </tr>`
            }
            formatedAssetDetails += data_append
            resolve(`<tr class='channel-row' data-channel-name=${backup_channel_Name} data-data="${modified_on}" flag-value=${flag} style='cursor: pointer'>${data_append}`)
        }
    })
}



// Backup Channel Listin On Click Event
$(document).on("click", ".backup", function (e) {
    $.get('/v1/livestreaming/channel_name',
        function (data, status) {
            if (data.status == 200) {
                destroyRows();
                appendBackup_ChannelDatatable(data)
            }
        });
})


// Append Backup Channel 
function appendBackup_ChannelDatatable(data) {

    var array = data.data;
    var options_table = "";
    array.forEach(function (element, i) {

        var backup_channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
        var url = element.url ? element.url : "-";
        var serverip = element.backupserverIp ? element.backupserverIp : "-";
        var serverName = element.backupserverName ? element.backupserverName : "-"
        var flag = element.flag ? element.flag : "-";
        var Termination_Policy = element.terminationPolicy ? element.terminationPolicy : "-";
        var RuleArn_http = element.RuleArn_http ? element.RuleArn_http : "-";
        var RuleArn_https = element.RuleArn_https ? element.RuleArn_https : "-";
        var userRole = $('.pro-user-role').attr("data-userRole")

        if (userRole === 'Monitoring') {

            options_table += `<tr class='channel-overview-tbl-row' data-channel-name=${backup_channel_Name} data-data="${modified_on}" > <td class="index">${i + 1}</td>
            <td class="channel-name">${backup_channel_Name}</td>
              <td>${modified_on}</td>
              <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
              <td id="${backup_channel_Name}"  data-channel-serverip=${serverip}></td>
              <td>${flag}</td>
               <td class="asset-action-td">
                <div class="dropdown"> 
                <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
                <div class="dropdown-menu dropdown-menu-right">
                  
                   <a href="#" data-channel-name=${backup_channel_Name} publishing-channel-url=${url} class="dropdown-item backup_channel_details">Details</a>
                   </div> </div> 
                   </td>
                   <td  data-toggle="modal" data-target="#play-video-channel" data-channel-name=${backup_channel_Name} publishing-channel-url=${url}><i data-channel-name=${backup_channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-backupChannelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                   </tr>`


        } else if (userRole === 'Admin' || userRole === 'Operator') {

            options_table += `<tr class='channel-overview-tbl-row' data-channel-name=${backup_channel_Name} data-data="${modified_on}" > <td class="index">${i + 1}</td>
            <td class="channel-name">${backup_channel_Name}</td>
              <td>${modified_on}</td>
              <td class="${serverip}"  data-channel-serverip=${serverip}>${serverName}</td>
              <td id="${backup_channel_Name}"  data-channel-serverip=${serverip}></td>
              <td>${flag}</td>
               <td class="asset-action-td">
                <div class="dropdown"> 
                <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false"> <i style="font-size:20px" class="fe-settings noti-icon"></i> </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" data-channel-name=${backup_channel_Name} class="dropdown-item update_channel" data-toggle="modal" data-target="#UpdateModalbox">Edit</a>
                   <a href="#" data-channel-name=${backup_channel_Name}  data-channel-serverip=${serverip}  data-channel-termination-policy=${Termination_Policy}  data-channel-ruleArn_http=${RuleArn_http} data-channel-ruleArn_https=${RuleArn_https} class="dropdown-item delete_backup_channel">Delete</a>
                   <a href="#" data-channel-name=${backup_channel_Name} publishing-channel-url=${url} class="dropdown-item backup_channel_details">Details</a>
                   </div> </div> 
                   </td>
                   <td  data-toggle="modal" data-target="#play-video-channel" data-channel-name=${backup_channel_Name} publishing-channel-url=${url}><i data-channel-name=${backup_channel_Name} publishing-channel-url=${url} class="mdi mdi-play-circle-outline play-video-backupChannelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i></td> 
                   </tr>`
        }

        if (i == array.length - 1) {
            $('#channel_listing_tbody').html(options_table)
            reInitializeDataTable();
        }
    })
}


// Get Backup Channel State From Server
function get_backup_state_of_channel(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/get_backup_state_of_channel`, params,
            function (data, status) {
                if (data.status === 200) {
                    resolve(data.data)
                } else {
                    reject()
                }
            })
    })
}


// Click Event Of Backup Delete
$(document).on("click", ".delete_backup_channel", function (event) {

    var channel_Name = $(this).attr('data-channel-name');
    var serverIp = $(this).attr('data-channel-serverip');
    var Termination_Policy = $(this).attr('data-channel-termination-policy');
    var RuleArn_http = $(this).attr('data-channel-ruleArn_http')
    var RuleArn_https = $(this).attr('data-channel-ruleArn_https')

    let params = {
        backup_channel_Name: channel_Name,
        serverip: serverIp,
        Termination_Policy: Termination_Policy
    }

    get_backup_state_of_channel(params).then((channel_Name_content) => {
        var channelState = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";

        let _params = {
            channel_Name: channel_Name,
            channelState: channelState,
            serverIp: serverIp,
            Termination_Policy: Termination_Policy,
            RuleArn_http: RuleArn_http,
            RuleArn_https: RuleArn_https
        }
        if (_params.Termination_Policy === 'Disable') {
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    delete_backup_channel(_params)
                }
            })
        } else if (_params.Termination_Policy === 'Enable') {

            Swal.fire({
                type: 'info',
                title: 'Deletion Protection',
                text: 'These channel have Deletion Protection and will not be terminated. Use the Change Deletion Protection option from the Actions menu to allow deletion of these channel!',
            })

        }
    })
})



// Deletion of Backup Channel
function delete_backup_channel(_params) {

    var backup_channel_Name = _params.channel_Name
    var channelState = _params.channelState
    var serverip = _params.serverIp
    var RuleArn_http = _params.RuleArn_http
    var RuleArn_https = _params.RuleArn_https

    let paramsdata = {
        channelName: backup_channel_Name,
        serverip: serverip,
        RuleArn_http: RuleArn_http,
        RuleArn_https: RuleArn_https
    }
    if (channelState === 'idle') {
        $.post('/v1/livestreaming/backup-channel-delete', paramsdata,
            function (data, status) {
                if (data.status === 200) {
                    swal.fire(
                        'Deleted!',
                        'Channel has been deleted',
                        'success'
                    )
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                    let userparams = {
                        createdChannelName: backup_channel_Name,
                        activity: 'Delete backup channel'
                    }
                    userAcitivity(userparams)
                    var params = {
                        createdChannelName: backup_channel_Name,
                        description: 'backup channel deleted successfully'
                    }
                    whatsapp(params)
                    SlackMsg(params)
                    channel_listing().then((channel_listing) => {
                        var array = channel_listing.data;
                        for (var i = 0; i < array.length; i++) {
                            var channel_Name_primary = array[i].channelName ? array[i].channelName : "-";
                            if (channel_Name_primary === backup_channel_Name) {
                                _flag = 'false'
                                let _params = {
                                    _flag: _flag.toString(),
                                    xmlFileName: channel_Name_primary
                                }
                                primaryupdateonmongo(_params)
                            }
                        }
                    })

                } else {
                    toastr.danger('Operation failed')
                }
            });
    } else {
        resetbackupchannel(backup_channel_Name)
    }
    event.preventDefault()
}


// Reset Channel to Server & MongoDB
function resetbackupchannel(backup_channel_Name) {
    var channelName = backup_channel_Name
    let _params = {
        channelName: channelName
    }
    $.post('/v1/livestreaming/update-backup-channel', _params,
        function (data, status) {
            if (data.status === 200) {
                let _data = data.data;
                let update_reset_params = {
                    updatedServerIp: _data.backupserverIp,
                    xmlFileName: _data.channelName,
                    UpdatedchannelName: _data.channelName,
                    Updatedcreator: _data.creator,
                    UpdatedlookaheadFragments: _data.lookaheadFragments,
                    UpdateddvrWindowLength: _data.dvrWindowLength,
                    UpdatedarchiveSegmentLength: _data.archiveSegmentLength,
                    Updatedarchiving: _data.archiving,
                    UpdatedarchiveLength: _data.archiveLength,
                    UpdatedrestartOnEncoderReconnect: _data.restartOnEncoderReconnect,
                    UpdatedissMinimumFragmentLength: _data.issMinimumFragmentLength,
                    UpdatedhlsMinimumFragmentLength: _data.hlsMinimumFragmentLength,
                    UpdatedhlsNoAudioOnly: _data.hlsNoAudioOnly
                }

                var xmlUpdatedData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language">   <head>   <meta name="clientManifestRelativePath" content="${update_reset_params.xmlFileName}.ismc" />        <meta name="creator" content="${update_reset_params.Updatedcreator}" />        <meta name="lookahead_fragments" content="${update_reset_params.UpdatedlookaheadFragments}" />       <meta name="dvr_window_length" content="${update_reset_params.UpdateddvrWindowLength}" />      <meta name="archive_segment_length" content="${update_reset_params.UpdatedarchiveSegmentLength}" />      <meta name="archiving" content="${update_reset_params.Updatedarchiving}" />  <meta name="event_id" content="${update_reset_params.xmlFileName}" /> </head>   <body>    <switch>     </switch>    </body> </smil>`;
                let _params = {
                    xmlUpdatedData: xmlUpdatedData,
                    xmlFileName: _data.channelName,
                    UpdatedchannelName: _data.channelName,
                    Updatedcreator: _data.creator,
                    UpdatedlookaheadFragments: _data.lookaheadFragments,
                    UpdateddvrWindowLength: _data.dvrWindowLength,
                    UpdatedarchiveSegmentLength: _data.archiveSegmentLength,
                    Updatedarchiving: _data.archiving,
                    UpdatedBackupServerIp: _data.backupserverIp,
                    UpdatedarchiveLength: _data.archiveLength,
                    UpdatedrestartOnEncoderReconnect: _data.restartOnEncoderReconnect,
                    UpdatedissMinimumFragmentLength: _data.issMinimumFragmentLength,
                    UpdatedhlsMinimumFragmentLength: _data.hlsMinimumFragmentLength,
                    UpdatedhlsNoAudioOnly: _data.hlsNoAudioOnly
                }

                $.post('/v1/livestreaming/update-backup-channel-params', _params,

                    function (data, textStatus, request) {
                        if (data.status === 200) {
                            var backup_channel_Name = data.data.channelName;
                            var serverIp = data.data.backupserverIp;
                            var RuleArn_http = data.data.RuleArn_http;
                            var RuleArn_https = data.data.RuleArn_https;

                            let params = {
                                backup_channel_Name: backup_channel_Name,
                                serverip: serverIp
                            }

                            get_backup_state_of_channel(params).then((channel_Name_content) => {

                                var channelState = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";
                                let _params = {
                                    channel_Name: backup_channel_Name,
                                    channelState: channelState,
                                    serverIp: serverIp,
                                    RuleArn_http: RuleArn_http,
                                    RuleArn_https: RuleArn_https
                                }
                                delete_backup_channel(_params)
                            })
                        } else {
                            toastr.danger('Operation Failed')
                        }
                    });
            } else {
                toastr.danger('Operation Failed')
            }
        })
}


// Click Event of Primary Channel
$(document).on("click", ".delete_channel", function (event) {

    var backup_channel_Name = $(this).attr('data-channel-name');
    var serverIp = $(this).attr('data-channel-serverip');
    var Termination_Policy = $(this).attr('data-channel-termination-policy')
    var RuleArn_http = $(this).attr('data-channel-ruleArn_http')
    var RuleArn_https = $(this).attr('data-channel-ruleArn_https')

    let params = {
        channel_Name: backup_channel_Name,
        serverip: serverIp,
        Termination_Policy: Termination_Policy
    }

    get_state_of_channel(params).then((channel_Name_content) => {

        var channelState = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";
        let _params = {
            channel_Name: backup_channel_Name,
            channelState: channelState,
            serverIp: serverIp,
            Termination_Policy: Termination_Policy,
            RuleArn_http: RuleArn_http,
            RuleArn_https: RuleArn_https
        }

        if (_params.Termination_Policy === 'Disable') {
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    delete_channel(_params)
                }
            })
        } else if (_params.Termination_Policy === 'Enable') {
            Swal.fire({
                type: 'info',
                title: 'Deletion Protection',
                text: 'These channel have Deletion Protection and will not be terminated. Use the Change Deletion Protection option from the Actions menu to allow deletion of these channel!',

            })

        }
    })
})

post_response = (params) => {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/channel-Listing_by_TerminationPolicy`, params,
            function (data, status) {
                if (data.status === 200)
                    resolve(data.data)
                else
                    reject()
            })
    })
}


async function send_post_request(params) {
    try {
        const post_resp = await post_response(params)
        return post_resp
    } catch (error) {
        console.log(error);
    }
}


// Deletion of Primary Channel
function delete_channel(_params) {

    var channel_Name = _params.channel_Name
    var channelState = _params.channelState
    var serverip = _params.serverIp
    var RuleArn_http = _params.RuleArn_http
    var RuleArn_https = _params.RuleArn_https

    let paramsdata = {
        channelName: channel_Name,
        serverip: serverip,
        RuleArn_http: RuleArn_http,
        RuleArn_https: RuleArn_https
    }
    if (channelState === 'idle') {

        $.post('/v1/livestreaming/channel-delete', paramsdata,
            function (data, status) {
                if (data.status === 200) {
                    swal.fire(
                        'Deleted!',
                        'Channel has been deleted',
                        'success'
                    )
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                    let userparams = {
                        createdChannelName: channel_Name,
                        activity: 'Delete primary channel'
                    }
                    userAcitivity(userparams)
                    var params = {
                        createdChannelName: channel_Name,
                        description: 'primary channel deleted successfully'
                    }
                    whatsapp(params)
                    SlackMsg(params)
                } else {
                    toastr.danger('Operation failed')
                }
            });
    } else {
        resetchannel(channel_Name)
    }
    event.preventDefault()
}


// Reset Channel to Server & MongoDB
function resetchannel(channel_Name) {
    var channelName = channel_Name
    let _params = {
        channelName: channelName
    }
    $.post('/v1/livestreaming/update-channel', _params,

        function (data, status) {
            if (data.status === 200) {
                let _data = data.data;
                let update_reset_params = {
                    updatedServerIp: _data.ServerIp,
                    xmlFileName: _data.channelName,
                    UpdatedchannelName: _data.channelName,
                    Updatedcreator: _data.creator,
                    UpdatedlookaheadFragments: _data.lookaheadFragments,
                    UpdateddvrWindowLength: _data.dvrWindowLength,
                    UpdatedarchiveSegmentLength: _data.archiveSegmentLength,
                    Updatedarchiving: _data.archiving,
                    UpdatedarchiveLength: _data.archiveLength,
                    UpdatedrestartOnEncoderReconnect: _data.restartOnEncoderReconnect,
                    UpdatedissMinimumFragmentLength: _data.issMinimumFragmentLength,
                    UpdatedhlsMinimumFragmentLength: _data.hlsMinimumFragmentLength,
                    UpdatedhlsNoAudioOnly: _data.hlsNoAudioOnly
                }
                var xmlUpdatedData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language">   <head>   <meta name="clientManifestRelativePath" content="${update_reset_params.xmlFileName}.ismc" />        <meta name="creator" content="${update_reset_params.Updatedcreator}" />        <meta name="lookahead_fragments" content="${update_reset_params.UpdatedlookaheadFragments}" />       <meta name="dvr_window_length" content="${update_reset_params.UpdateddvrWindowLength}" />      <meta name="archive_segment_length" content="${update_reset_params.UpdatedarchiveSegmentLength}" />      <meta name="archiving" content="${update_reset_params.Updatedarchiving}" />  <meta name="event_id" content="${update_reset_params.xmlFileName}" />  </head>   <body>    <switch>     </switch>    </body> </smil>`;
                let _params = {
                    xmlUpdatedData: xmlUpdatedData,
                    xmlFileName: _data.channelName,
                    UpdatedchannelName: _data.channelName,
                    Updatedcreator: _data.creator,
                    UpdatedlookaheadFragments: _data.lookaheadFragments,
                    UpdateddvrWindowLength: _data.dvrWindowLength,
                    UpdatedarchiveSegmentLength: _data.archiveSegmentLength,
                    Updatedarchiving: _data.archiving,
                    UpdatedServerIp: _data.ServerIp,
                    UpdatedarchiveLength: _data.UpdatedarchiveLength,
                    UpdatedrestartOnEncoderReconnect: _data.UpdatedrestartOnEncoderReconnect,
                    UpdatedissMinimumFragmentLength: _data.UpdatedissMinimumFragmentLength,
                    UpdatedhlsMinimumFragmentLength: _data.UpdatedhlsMinimumFragmentLength,
                    UpdatedhlsNoAudioOnly: _data.hlsNoAudioOnly

                }
                $.post('/v1/livestreaming/update-channel-params', _params,

                    function (data, textStatus, request) {
                        if (data.status === 200) {
                            var channel_Name = data.data.channelName;
                            var serverIp = data.data.ServerIp;
                            var RuleArn_http = data.data.RuleArn_http;
                            var RuleArn_https = data.data.RuleArn_https;

                            let params = {
                                channel_Name: channel_Name,
                                serverip: serverIp
                            }
                            get_state_of_channel(params).then((channel_Name_content) => {

                                var channelState = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";
                                let _params = {
                                    channel_Name: channel_Name,
                                    channelState: channelState,
                                    serverIp: serverIp,
                                    RuleArn_http: RuleArn_http,
                                    RuleArn_https: RuleArn_https
                                }

                                delete_channel(_params)

                            })
                        } else {
                            toastr.danger('Operation Failed')
                        }
                    });
            } else {
                toastr.danger('Operation Failed')
            }
        })
}

// get channel data from Mongofunction getdatafrommongo()
$(document).on("click", ".update_channel", function (event) {

    var channelName = $(this).attr('data-channel-name')
    let _params = {
        channelName: channelName
    }
    getDataFromMongo(_params)
})

// Get select Channel data for updation
function getDataFromMongo(_params) {
    $.post('/v1/livestreaming/update-channel', _params,
        function (data, status) {
            if (data.status === 200) {
                let _data = data.data;
                var terminationPolicy = _data.terminationPolicy
                $('#update-channel-name').val(_data.channelName)
                $('#update-creator').val(_data.creator)
                $('#update-lookahead-fragments').val(_data.lookaheadFragments)
                $('#update-dvr-window-length').val(_data.dvrWindowLength)
                $('#update-archive-segment-length').val(_data.archiveSegmentLength)
                $('#update-archiving').val(_data.archiving)
                $('#update-archive-length').val(_data.archiveLength)
                $('#update-Restart-Encoder-Reconnect').val(_data.restartOnEncoderReconnect)
                $('#update-iss-Minimum-Fragment-Length').val(_data.issMinimumFragmentLength)
                $('#update-hls-Minimum-Fragment-Length').val(_data.hlsMinimumFragmentLength)
                $('#update-hls-No-Audio-Only').val(_data.hlsNoAudioOnly)
                $('#update-server-Ip').val(_data.ServerIp)
                $('#updateDeletion-Protection').val(_data.terminationPolicy)
                $('option:selected', this).attr(_data.serverName)
                backupserverIp = $('#updatebackupserver-div').val()
                backupserverName = $('#updatebackupserver-div').text().trim()
                if (terminationPolicy === 'Enable') {

                    options_termination = ""
                    options_termination += `<label class="switch">
                <input type="checkbox" checked  onclick="checkterminationupdation()" id='updationtermination-checkbox'>
                    <span class="slider round"></span>
                  </label>`

                    $('#updateToggle').html(options_termination)
                } else {

                    options_termination = ""
                    options_termination += `<label class="switch">
                    <input type="checkbox" onclick="checkterminationupdation()" id='updationtermination-checkbox'>
                      <span class="slider round"></span>
                    </label>`

                    $('#updateToggle').html(options_termination)

                }
            } else {
                toastr.error(data.message);
            }
        })
    event.preventDefault()
}

// Update Channel On both Server
var primaryInstanceName = '';
$(document).on("click", ".update_channel_parameter_both", function () {

    var name1 = $("#update-server-Ip option:selected").text();
    var BackupServerName = name1.replace("Primary", "Backup");

    $.get(`/v1/server/server-listing-backup`,
        function (data, status) {
            if (data.status === 200) {
                var _data = data.data;
                for (var i = 0; i < _data.Reservations.length; i++) {
                    InstanceId = _data.Reservations[i].Instances[0].InstanceId ? _data.Reservations[i].Instances[0].InstanceId : "-";
                    state = _data.Reservations[i].Instances[0].State.Name ? _data.Reservations[i].Instances[0].State.Name : "-";
                    PublicIpAddress = _data.Reservations[i].Instances[0].PublicIpAddress ? _data.Reservations[i].Instances[0].PublicIpAddress : "-";
                    var InstanceName;
                    for (var j = 0; j < _data.Reservations[i].Instances[0].Tags.length; j++) {
                        if (_data.Reservations[i].Instances[0].Tags[j].Key === "Name") {
                            InstanceName = _data.Reservations[i].Instances[0].Tags[j].Value ? _data.Reservations[i].Instances[0].Tags[j].Value : "-";
                        }
                    }

                    if (InstanceName === BackupServerName) {
                        var ip = PublicIpAddress

                        var params = {
                            channelName: $('#update-channel-name').val().trim(),
                            creator: $('#update-creator').val().trim(),
                            lookaheadFragments: $('#update-lookahead-fragments').val().trim(),
                            dvrWindowLength: $('#update-dvr-window-length').val().trim(),
                            archiveSegmentLength: $('#update-archive-segment-length').val().trim(),
                            archiving: $('#update-archiving').val().trim(),
                            archiveLength: $('#update-archive-length').val().trim(),
                            restartOnEncoderReconnect: $('#update-Restart-Encoder-Reconnect').val().trim(),
                            issMinimumFragmentLength: $('#update-iss-Minimum-Fragment-Length').val().trim(),
                            hlsMinimumFragmentLength: $('#update-hls-Minimum-Fragment-Length').val().trim(),
                            hlsNoAudioOnly: $('#update-hls-No-Audio-Only').val().trim(),
                            serverIp: $('#update-server-Ip').val().trim(),
                            terminationPolicy: terminationUpdationvariable,
                            backupIp: ip
                        }

                        var xmlUpdatedData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language">   <head>   <meta name="clientManifestRelativePath" content="${params.channelName}.ismc" />        <meta name="creator" content="${params.creator}" />        <meta name="lookahead_fragments" content="${params.lookaheadFragments}" />       <meta name="dvr_window_length" content="${params.dvrWindowLength}" />      <meta name="archive_segment_length" content="${params.archiveSegmentLength}" />      <meta name="archiving" content="${params.archiving}" /> <meta name="archive_length" content="${params.archiveLength}" /> <meta name="restart_on_encoder_reconnect" content="${params.restartOnEncoderReconnect}" /> <meta name="iss_minimum_fragment_length" content="${params.issMinimumFragmentLength}" /> <meta name="hls_minimum_fragment_length" content="${params.hlsMinimumFragmentLength}" /> <meta name="hls_no_audio_only" content="${params.hlsNoAudioOnly}" /> </head>   <body>    <switch>     </switch>    </body> </smil>`;
                        let _params = {
                            xmlUpdatedData: xmlUpdatedData,
                            xmlFileName: params.channelName,
                            UpdatedchannelName: params.channelName,
                            Updatedcreator: params.creator,
                            UpdatedlookaheadFragments: params.lookaheadFragments,
                            UpdateddvrWindowLength: params.dvrWindowLength,
                            UpdatedarchiveSegmentLength: params.archiveSegmentLength,
                            Updatedarchiving: params.archiving,
                            UpdatedarchiveLength: params.archiveLength,
                            UpdatedrestartOnEncoderReconnect: params.restartOnEncoderReconnect,
                            UpdatedissMinimumFragmentLength: params.issMinimumFragmentLength,
                            UpdatedhlsMinimumFragmentLength: params.hlsMinimumFragmentLength,
                            UpdatedServerIp: params.serverIp,
                            UpdatedhlsNoAudioOnly: params.hlsNoAudioOnly,
                            UpdatedBackupServerIp: params.backupIp,
                            updateterminationPolicy: params.terminationPolicy
                        }

                        primaryupdate(_params)
                        backupupdate(_params)
                    }
                }
            }
        })
    event.preventDefault()
})

// Updation Channel On  Primary Server
$(document).on("click", ".update_channel_parameter", function (event) {
    var params = {
        channelName: $('#update-channel-name').val().trim(),
        creator: $('#update-creator').val().trim(),
        lookaheadFragments: $('#update-lookahead-fragments').val().trim(),
        dvrWindowLength: $('#update-dvr-window-length').val().trim(),
        archiveSegmentLength: $('#update-archive-segment-length').val().trim(),
        archiving: $('#update-archiving').val().trim(),
        archiveLength: $('#update-archive-length').val().trim(),
        restartOnEncoderReconnect: $('#update-Restart-Encoder-Reconnect').val().trim(),
        issMinimumFragmentLength: $('#update-iss-Minimum-Fragment-Length').val().trim(),
        hlsMinimumFragmentLength: $('#update-hls-Minimum-Fragment-Length').val().trim(),
        hlsNoAudioOnly: $('#update-hls-No-Audio-Only').val().trim(),
        serverIp: $('#update-server-Ip').val().trim(),
        terminationPolicy: terminationUpdationvariable
    }
    var xmlUpdatedData = `<?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language">   <head>   <meta name="clientManifestRelativePath" content="${params.channelName}.ismc" />        <meta name="creator" content="${params.creator}" />        <meta name="lookahead_fragments" content="${params.lookaheadFragments}" />       <meta name="dvr_window_length" content="${params.dvrWindowLength}" />      <meta name="archive_segment_length" content="${params.archiveSegmentLength}" />      <meta name="archiving" content="${params.archiving}" /> <meta name="archive_length" content="${params.archiveLength}" /> <meta name="restart_on_encoder_reconnect" content="${params.restartOnEncoderReconnect}" /> <meta name="iss_minimum_fragment_length" content="${params.issMinimumFragmentLength}" /> <meta name="hls_minimum_fragment_length" content="${params.hlsMinimumFragmentLength}" /> <meta name="hls_no_audio_only" content="${params.hlsNoAudioOnly}" />  </head>   <body>    <switch>     </switch>    </body> </smil>`;

    let _params = {
        xmlUpdatedData: xmlUpdatedData,
        xmlFileName: params.channelName,
        UpdatedchannelName: params.channelName,
        Updatedcreator: params.creator,
        UpdatedlookaheadFragments: params.lookaheadFragments,
        UpdateddvrWindowLength: params.dvrWindowLength,
        UpdatedarchiveSegmentLength: params.archiveSegmentLength,
        Updatedarchiving: params.archiving,
        UpdatedarchiveLength: params.archiveLength,
        UpdatedrestartOnEncoderReconnect: params.restartOnEncoderReconnect,
        UpdatedissMinimumFragmentLength: params.issMinimumFragmentLength,
        UpdatedhlsMinimumFragmentLength: params.hlsMinimumFragmentLength,
        UpdatedServerIp: params.serverIp,
        updateterminationPolicy: params.terminationPolicy,
        UpdatedhlsNoAudioOnly: params.hlsNoAudioOnly

    }
    primaryupdate(_params)
    event.preventDefault()
})

// update on primary Server and MongoDb
function primaryupdate(_params) {

    $.post('/v1/livestreaming/update-channel-params', _params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                var araay = data.data;
                var channel_Name = araay.channelName;
                var params = {
                    createdChannelName: channel_Name,
                    description: 'channel has been successfully updated'
                }
                whatsapp(params)
                SlackMsg(params)
                stateWiseEmailSend(params);

                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Channel Data has been Updated!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
                let userparams = {
                    createdChannelName: channel_Name,
                    activity: 'Update primary channel'
                }
                userAcitivity(userparams)


            } else {
                toastr.danger('Updation Failed')
            }
        });
}

// Update on Backup Server and MongoDb
function backupupdate(_params) {

    $.post('/v1/livestreaming/update-backup-channel-params', _params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                var araay = data.data;
                var channel_Name = araay.channelName;
                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Channel Data has been Updated!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
                let userparams = {
                    createdChannelName: channel_Name,
                    activity: 'Update backup channel'
                }
                userAcitivity(userparams)
                let params = {
                    createdChannelName: channel_Name,
                    description: 'channel has been successfully updated'
                }
                whatsapp(params)
                stateWiseEmailSend(params)
            } else {
                toastr.danger('Updation Failed')
            }
        });
}


$(document).on("click", ".channel_listing", function (event) {

    $.get('/v1/livestreaming/get-listing',
        function (data, status) {
            if (data.status === 200) {
                toastr.success(' channel_listing successfully');
            } else {
                toastr.danger(' channel_listing failed')
            }
        });
})

var player;
//  Initiate publishing channel Player
function initiatePlayer(params) {
    return new Promise((resolve, reject) => {

        if (player !== undefined) {
            player.reset()
        }

        var options = {
            plugins: {
                // streamrootHls: {
                //     hlsjsConfig: {
                //         smoothQualityChange: true
                //     }
                // },
                controlBar: {
                    progressControl: {
                        //   keepTooltipsInside: true,
                        MouseTimeDisplay: true,
                        PlayProgressBar: true
                    }
                },
                qualityMenu: {
                    useResolutionLabels: true
                }
            }
        };
        player = videojs(params.player_id, options)
        player.src({
            type: 'application/x-mpegURL',
            src: params.url
        });
        // player.controlBar.liveDisplay.show()
        player.qualityMenu();
        player.onloadeddata = function () {
            resolve()
        };
    })
}

// Play Channel Button
$(document).on("click", ".play-video-channelButton", function () {

    var channelName = $(this).attr('data-channel-name');
    var publishingplayurl = $(this).attr('publishing-channel-url')
    var publishing_url_video = publishingplayurl + '/.m3u8'

    // var publishing_url_video = 'http://suntv.skandha.tv/KTVHD/KTVHD.isml/.m3u8'

    var params = {
        player_id: 'channelurlplayer',
        url: publishing_url_video,
        channelName: channelName
    }
    initiatePlayer(params)
})

// Reset Modal box
$('#exampleModalScrollable').on('hidden.bs.modal', function () {
    $('#exampleModalScrollable form')[0].reset();
});

// Initiate Backup Player
function initiateBackupPlayer(params) {
    return new Promise((resolve, reject) => {

        if (player !== undefined) {
            player.reset()
        }

        var options = {
            plugins: {
                // streamrootHls: {
                //     hlsjsConfig: {
                //         smoothQualityChange: true
                //     }
                // },
                controlBar: {
                    progressControl: {
                        //   keepTooltipsInside: true,
                        MouseTimeDisplay: true,
                        PlayProgressBar: true
                    }
                },
                qualityMenu: {
                    useResolutionLabels: true
                }
            }
        };
        player = videojs(params.player_id, options)
        player.src({
            type: 'application/x-mpegURL',
            src: params.url
        });
        // player.controlBar.liveDisplay.show()
        player.qualityMenu();
        player.onloadeddata = function () {
            resolve()
        };
    })
}

// Play Channel Button
$(document).on("click", ".play-video-backupChannelButton", function () {

    var channelName = $(this).attr('data-channel-name');
    var publishingplayurl = $(this).attr('publishing-channel-url')
    var publishing_url_video = publishingplayurl + '/.m3u8'

    // var publishing_url_video = 'http://suntv.skandha.tv/KTVHD/KTVHD.isml/.m3u8'

    var _params = {
        player_id: 'channelurlplayer',
        url: publishing_url_video,
        channelName: channelName
    }
    initiateBackupPlayer(_params)
})

// Select  Primary Channel details
$(document).on("click", ".channel_details", function () {

    $('#channel-details-div').removeClass("force-hidden")
    $("#body-content").addClass("enlarged");
    $('#channel-listing-div').removeClass("col-md-12")
    $('#channel-listing-div').addClass("col-md-8")
    $('#edit-asset-div').removeClass("force-hidden")
    var channelName = $(this).attr('data-channel-name');

    let channel_details_params = {
        channelName: channelName
    }

    $.post('/v1/livestreaming/update-channel', channel_details_params,
        function (data, status) {
            if (data.status === 200) {
                formatChannelData(data)

            } else {
                toastr.error(data.message);
            }
        })
})

// formated Primary Channel data
function formatChannelData(data) {

    // $('#technical-data').empty();
    var channeldata_div = "";
    var videoBitrate_div = "";
    var channelData = data.data;
    var channelName = channelData.channelName;
    var creator = channelData.creator;
    var lookaheadFragments = channelData.lookaheadFragments;
    var dvrWindowLength = channelData.dvrWindowLength;
    var archiveSegmentLength = channelData.archiveSegmentLength;
    var archiving = channelData.archiving;
    var archiveLength = channelData.archiveLength;
    var publishing_url = channelData.url;
    var ServerIp = channelData.ServerIp;
    var restartOnEncoderReconnect = channelData.restartOnEncoderReconnect;
    var issMinimumFragmentLength = channelData.issMinimumFragmentLength;
    var hlsMinimumFragmentLength = channelData.hlsMinimumFragmentLength;
    var hlsNoAudioOnly = channelData.hlsNoAudioOnly;
    var params = {
        channel_Name: channelName,
        serverip: ServerIp
    }
    get_state_of_channel(params).then((channel_Name_content) => {
        var channelState = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";

        if (channelState !== 'idle') {

            channel_statistics(params).then((channel_statistics) => {

                var statistics_video = channel_statistics.smil.body[0];
                let videoBitrate = [];
                let audioBitrate = [];

                if (statistics_video.video.length >= 1) {

                    for (var j = 0; j < statistics_video.video.length; j++) {
                        videoSystemBitrate = statistics_video.video[j].$.systemBitrate;
                        videoBitrate.push(videoSystemBitrate)
                    }
                }
                for (var k = 0; k < statistics_video.audio.length; k++) {
                    audioSystemBitrate = statistics_video.audio[k].$.systemBitrate;
                    audioBitrate.push(audioSystemBitrate)
                }

                videoBitrate.forEach(function (element, i) {
                    var videoBitrate = element ? element : "-";
                    videoBitrate_div += `<dt class="col-sm-8"></dt> 
                   <dd class="col-sm-4 text-break">${videoBitrate}</dd>`
                })

                channeldata_div += `<div class="col-sm-12 content-detail">
               <dl class="row">
            <dt class="col-sm-3">URL</dt> 
                   <dd class="col-sm-9 text-break" id="publishingurl">${publishing_url} </dd>
            <dt class="col-sm-8">Channel Name</dt> 
                   <dd class="col-sm-4">${channelName} </dd> 
                   <dt class="col-sm-8">Video Bitrate</dt> 
                   <dd class="col-sm-4">${videoBitrate_div}</dd>
            <dt class="col-sm-8">Audio Bitrate</dt> 
                   <dd class="col-sm-4">${audioBitrate}</dd>
            <dt class="col-sm-8">Creator</dt> 
                   <dd class="col-sm-4">${creator} </dd> 
            <dt class="col-sm-8">Lookahead Fragments</dt> 
                   <dd class="col-sm-4">${lookaheadFragments} </dd>
            <dt class="col-sm-8">DVR Window Length</dt> 
                   <dd class="col-sm-4">${dvrWindowLength} </dd> 
            <dt class="col-sm-8">Archive Segment Length</dt> 
                   <dd class="col-sm-4">${archiveSegmentLength}  </dd>
            <dt class="col-sm-8">Archiving</dt> 
                   <dd class="col-sm-4">${archiving} </dd>
                   <dt class="col-sm-8">Archiving Length</dt> 
                   <dd class="col-sm-4">${archiveLength} </dd>
            <dt class="col-sm-8">Restart On Encoder Reconnect</dt> 
                   <dd class="col-sm-4">${restartOnEncoderReconnect} </dd>
            <dt class="col-sm-8">Iss Minimum Fragment Length</dt> 
                   <dd class="col-sm-4">${issMinimumFragmentLength} </dd>
            <dt class="col-sm-8">Hls Minimum Fragment Length</dt> 
                   <dd class="col-sm-4">${hlsMinimumFragmentLength} </dd>
            <dt class="col-sm-8">Hls No Audio Only</dt> 
                   <dd class="col-sm-4">${hlsNoAudioOnly} </dd>
               </dl>
               </div>`

                if (publishing_url === 'undefined') {

                    $('#publishingurl').html("No Publishing URL available")

                } else {
                    $('#technical-data').html(channeldata_div)
                }

            })
        }
        else {

            channeldata_div += `<div class="col-sm-12 content-detail">
            <dl class="row">
         <dt class="col-sm-3">URL</dt> 
                <dd class="col-sm-9 text-break" id="publishingurl">${publishing_url} </dd>
         <dt class="col-sm-8">Channel Name</dt> 
                <dd class="col-sm-4">${channelName} </dd> 
         <dt class="col-sm-8">Creator</dt> 
                <dd class="col-sm-4">${creator} </dd> 
         <dt class="col-sm-8">Lookahead Fragments</dt> 
                <dd class="col-sm-4">${lookaheadFragments} </dd>
         <dt class="col-sm-8">DVR Window Length</dt> 
                <dd class="col-sm-4">${dvrWindowLength} </dd> 
         <dt class="col-sm-8">Archive Segment Length</dt> 
                <dd class="col-sm-4">${archiveSegmentLength}  </dd>
         <dt class="col-sm-8">Archiving</dt> 
                <dd class="col-sm-4">${archiving} </dd>
                <dt class="col-sm-8">Archiving Length</dt> 
                <dd class="col-sm-4">${archiveLength} </dd>
         <dt class="col-sm-8">Restart On Encoder Reconnect</dt> 
                <dd class="col-sm-4">${restartOnEncoderReconnect} </dd>
         <dt class="col-sm-8">Iss Minimum Fragment Length</dt> 
                <dd class="col-sm-4">${issMinimumFragmentLength} </dd>
         <dt class="col-sm-8">Hls Minimum Fragment Length</dt> 
                <dd class="col-sm-4">${hlsMinimumFragmentLength} </dd>
        
         <dt class="col-sm-8">Hls No Audio Only</dt> 
                <dd class="col-sm-4">${hlsNoAudioOnly} </dd>
            </dl>
            </div>`

            if (publishing_url === 'undefined') {

                $('#publishingurl').html("No Publishing URL available")

            } else {
                $('#technical-data').html(channeldata_div)
            }
        }
    })
}


// Selected Backup Channel Details
$(document).on("click", ".backup_channel_details", function () {

    $('#channel-details-div').removeClass("force-hidden")
    $("#body-content").addClass("enlarged");
    $('#channel-listing-div').removeClass("col-md-12")
    $('#channel-listing-div').addClass("col-md-8")
    $('#edit-asset-div').removeClass("force-hidden")

    var channelName = $(this).attr('data-channel-name');

    let channel_details_params = {
        channelName: channelName
    }

    $.post('/v1/livestreaming/update-backup-channel', channel_details_params,
        function (data, status) {
            if (data.status === 200) {
                formatbackupChannelData(data)
            } else {
                toastr.error(data.message);
            }
        })
})

// formated Backup Channel data
function formatbackupChannelData(data) {

    var channeldata_div = "";
    var videoBitrate_div = "";
    var channelData = data.data;
    var channelName = channelData.channelName;
    var creator = channelData.creator;
    var lookaheadFragments = channelData.lookaheadFragments;
    var dvrWindowLength = channelData.dvrWindowLength;
    var archiveSegmentLength = channelData.archiveSegmentLength;
    var archiving = channelData.archiving;
    var archiveLength = channelData.archiveLength;
    var objectId = channelData.objectId;
    var publishing_url = channelData.url;
    var ServerIp = channelData.backupserverIp;
    var restartOnEncoderReconnect = channelData.restartOnEncoderReconnect;
    var issMinimumFragmentLength = channelData.issMinimumFragmentLength;
    var hlsMinimumFragmentLength = channelData.hlsMinimumFragmentLength;
    var hlsNoAudioOnly = channelData.hlsNoAudioOnly;

    var params = {
        backup_channel_Name: channelName,
        serverip: ServerIp
    }
    get_backup_state_of_channel(params).then((channel_Name_content) => {
        var channelState = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";
        if (channelState !== 'idle') {
            backup_channel_statistics(params).then((channel_statistics) => {

                var statistics_video = channel_statistics.smil.body[0];
                let videoBitrate = [];
                let audioBitrate = [];

                if (statistics_video.video.length >= 1) {

                    for (var j = 0; j < statistics_video.video.length; j++) {
                        videoSystemBitrate = statistics_video.video[j].$.systemBitrate;
                        videoBitrate.push(videoSystemBitrate)
                    }
                }
                for (var k = 0; k < statistics_video.audio.length; k++) {
                    audioSystemBitrate = statistics_video.audio[k].$.systemBitrate;
                    audioBitrate.push(audioSystemBitrate)
                }
                videoBitrate.forEach(function (element, i) {
                    var videoBitrate = element ? element : "-";
                    videoBitrate_div += `<dt class="col-sm-8"></dt> 
                   <dd class="col-sm-4 text-break">${videoBitrate}</dd>`
                })
                channeldata_div += `<div class="col-sm-12 content-detail">
               <dl class="row">
            <dt class="col-sm-3">URL</dt> 
                   <dd class="col-sm-9 text-break" id="publishingurl">${publishing_url} </dd>
            <dt class="col-sm-8">Channel Name</dt> 
                   <dd class="col-sm-4">${channelName} </dd> 
            <dt class="col-sm-8">Video Bitrate</dt> 
                   <dd class="col-sm-4">${videoBitrate_div}</dd>
            <dt class="col-sm-8">Audio Bitrate</dt> 
                   <dd class="col-sm-4">${audioBitrate}</dd>
            <dt class="col-sm-8">Creator</dt> 
                   <dd class="col-sm-4">${creator} </dd> 
            <dt class="col-sm-8">Lookahead Fragments</dt> 
                   <dd class="col-sm-4">${lookaheadFragments} </dd>
            <dt class="col-sm-8">DVR Window Length</dt> 
                   <dd class="col-sm-4">${dvrWindowLength} </dd> 
            <dt class="col-sm-8">Archive Segment Length</dt> 
                   <dd class="col-sm-4">${archiveSegmentLength}  </dd>
            <dt class="col-sm-8">Archiving</dt> 
                   <dd class="col-sm-4">${archiving} </dd>
            <dt class="col-sm-8">Archiving Length</dt> 
                   <dd class="col-sm-4">${archiveLength} </dd>
            <dt class="col-sm-8">Event Id</dt> 
                   <dd class="col-sm-4">${objectId} </dd>
            <dt class="col-sm-8">Restart On Encoder Reconnect</dt> 
                   <dd class="col-sm-4">${restartOnEncoderReconnect} </dd>
            <dt class="col-sm-8">Iss Minimum Fragment Length</dt> 
                   <dd class="col-sm-4">${issMinimumFragmentLength} </dd>
            <dt class="col-sm-8">Hls Minimum Fragment Length</dt> 
                   <dd class="col-sm-4">${hlsMinimumFragmentLength} </dd>
            <dt class="col-sm-8">Hls No Audio Only</dt> 
                   <dd class="col-sm-4">${hlsNoAudioOnly} </dd>
               </dl>
               </div>`

                if (publishing_url === 'undefined') {

                    $('#publishingurl').html("No Publishing URL available")

                } else {
                    $('#technical-data').html(channeldata_div)
                }
            })
        }
        else {

            channeldata_div += `<div class="col-sm-12 content-detail">
            <dl class="row">
         <dt class="col-sm-3">URL</dt> 
                <dd class="col-sm-9 text-break" id="publishingurl">${publishing_url} </dd>
         <dt class="col-sm-8">Channel Name</dt> 
                <dd class="col-sm-4">${channelName} </dd> 
         <dt class="col-sm-8">Creator</dt> 
                <dd class="col-sm-4">${creator} </dd> 
         <dt class="col-sm-8">Lookahead Fragments</dt> 
                <dd class="col-sm-4">${lookaheadFragments} </dd>
         <dt class="col-sm-8">DVR Window Length</dt> 
                <dd class="col-sm-4">${dvrWindowLength} </dd> 
         <dt class="col-sm-8">Archive Segment Length</dt> 
                <dd class="col-sm-4">${archiveSegmentLength}  </dd>
         <dt class="col-sm-8">Archiving</dt> 
                <dd class="col-sm-4">${archiving} </dd>
         <dt class="col-sm-8">Archiving Length</dt> 
                <dd class="col-sm-4">${archiveLength} </dd>
         <dt class="col-sm-8">Event Id</dt> 
                <dd class="col-sm-4">${objectId} </dd>
         <dt class="col-sm-8">Restart On Encoder Reconnect</dt> 
                <dd class="col-sm-4">${restartOnEncoderReconnect} </dd>
         <dt class="col-sm-8">Iss Minimum Fragment Length</dt> 
                <dd class="col-sm-4">${issMinimumFragmentLength} </dd>
         <dt class="col-sm-8">Hls Minimum Fragment Length</dt> 
                <dd class="col-sm-4">${hlsMinimumFragmentLength} </dd>
         <dt class="col-sm-8">Hls No Audio Only</dt> 
                   <dd class="col-sm-4">${hlsNoAudioOnly} </dd>
               </dl>
            </dl>
            </div>`

            if (publishing_url === 'undefined') {

                $('#publishingurl').html("No Publishing URL available")

            } else {
                $('#technical-data').html(channeldata_div)
            }
        }
    })
}

// Primary Channel Statistics
function channel_statistics(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/channel_statistics`, params,
            function (data, status) {
                if (data.status === 200) {
                    let _data = data.data;
                    resolve(_data)
                } else {
                    toastr.error(data.message);
                }
            })
    })
}

// Backup Channel Statistics
function backup_channel_statistics(params) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/livestreaming/backup_channel_statistics`, params,
            function (data, status) {
                if (data.status === 200) {
                    let _data = data.data;
                    resolve(_data)
                } else {
                    toastr.error(data.message);
                }
            })
    })
}

// close-edit-metadata-panel
$(document).on("click", "#close-edit-metadata-panel-btn", function () {
    // remove enlarge sidebar first
    $("#body-content").removeClass("enlarged");
    $('#channel-listing-div').addClass("col-md-12")
    $('#edit-asset-div').addClass("force-hidden")
})

// User Activity
function userAcitivity(_params) {
    $.getJSON("http://api.ipify.org?format=json", function (data) {
        var ip_address = data.ip
        var createdChannelName = _params.createdChannelName;
        var activity = _params.activity;
        let userparams = {
            createdChannelName: createdChannelName,
            activity: activity,
            ip_address: ip_address
        }
        $.post('/v1/livestreaming/userAcitivity', userparams,

            function (data, textStatus, request) {
                if (data.status === 200) {

                } else {
                    toastr.danger('userAcitivity Failed')
                }
            });
    })
}

// #############################################################################################################

function iterate_channel_listing() {
    return new Promise((resolve, reject) => {
        $.get('/v1/livestreaming/channel_listing',
            function (data, status) {
                if (data.status == 200) {
                    resolve(data)
                }
            });
    })
}


// Primary after changing state give notifiaction & stored in MongoDb
async function getServerStatus() {
    iterate_channel_listing().then(async (channel_listing) => {
        var array = channel_listing.data;

        // below code is for status 

        for (const item of array) {

            var channel_Name = item.channelName ? item.channelName : "-";
            var serverip = item.ServerIp ? item.ServerIp : "-";

            var params = {
                channel_Name: channel_Name,
                serverip: serverip
            }

            await get_state_of_channel(params).then((channel_Name_content) => {

                var _state = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";

                // below shows state
                if (_state === 'idle') {
                    $(`.${channel_Name}`).html('<span class="badge badge-customblue badge" style=" background-color: #097bd3;font-size: 12px; width: 50px;">Idle</span>')
                }
                else if (_state === 'starting') {
                    $(`.${channel_Name}`).html('<span class="badge badge-purple badge" style="font-size: 12px; width: 50px;">starting</span>')

                } else if (_state === 'started') {
                    $(`.${channel_Name}`).html('<span class="badge badge-success badge" style="font-size: 12px; width: 50px;">started</span>')

                } else if (_state === 'stopping') {
                    $(`.${channel_Name}`).html('<span class="badge badge-dark badge" style="font-size: 12px; width: 50px;">stopping</span>')

                } else if (_state === 'stopped') {
                    $(`.${channel_Name}`).html('<span class="badge badge-danger badge" style="font-size: 12px; width: 50px;">stopped</span>')
                }

                // below code for db and notification
                if (item.state !== _state) {
                    // show notification
                    toastr.info(`${item.channelName} primary channel ${_state}`);

                    // update state into db
                    const _params = {
                        channel_Name: item.channelName,
                        _state: _state

                    }
                    $.post('/v1/livestreaming/updateStateInMongoDb', _params,
                        function (data, status) {
                            if (data.status == 200) {
                                var channel_Name = data.data.channelName
                                var state = data.data.state
                                var params = {
                                    createdChannelName: `The ${channel_Name}`,
                                    description: `primary channel status is ${state}`
                                }
                                whatsapp(params)
                                SlackMsg(params)
                            } else {
                                //console.log("update mongo for change state failed")
                            }
                        })
                }
            })
        }
    })

    setTimeout(function () {
        getServerStatus();
    }, 10000)
}
getServerStatus();

function iterate_backup_channel_listing() {
    return new Promise((resolve, reject) => {
        $.get('/v1/livestreaming/channel_name',
            function (data, status) {
                if (data.status == 200) {
                    resolve(data)
                }
            });
    })
}


// Backup after changing state give notifiaction & stored in MongoDb
async function getBackupServerStatus() {
    iterate_backup_channel_listing().then(async (iterate_backup_channel_listing) => {

        var array = iterate_backup_channel_listing.data;
        // below code is for status 
        for (const item of array) {

            var backup_channel_Name = item.channelName ? item.channelName : "-";
            var serverip = item.backupserverIp ? item.backupserverIp : "-";

            var params = {
                backup_channel_Name: backup_channel_Name,
                serverip: serverip
            }

            await get_backup_state_of_channel(params).then((channel_Name_content) => {

                var backup_state = channel_Name_content.smil ? channel_Name_content.smil.head[0].meta[1].$.content : "-";

                // below shows state
                if (backup_state === 'idle') {
                    $(`#${backup_channel_Name}`).html('<span class="badge badge-customblue badge" style=" background-color: #097bd3;font-size: 12px; width: 50px;">Idle</span>')
                }
                else if (backup_state === 'starting') {
                    $(`#${backup_channel_Name}`).html('<span class="badge badge-purple badge" style="font-size: 12px; width: 50px;">starting</span>')

                } else if (backup_state === 'started') {
                    $(`#${backup_channel_Name}`).html('<span class="badge badge-success badge" style="font-size: 12px; width: 50px;">started</span>')

                } else if (backup_state === 'stopping') {
                    $(`#${backup_channel_Name}`).html('<span class="badge badge-dark badge" style="font-size: 12px; width: 50px;">stopping</span>')

                } else if (backup_state === 'stopped') {
                    $(`#${backup_channel_Name}`).html('<span class="badge badge-danger badge" style="font-size: 12px; width: 50px;">stopped</span>')
                }

                // below code for db and notification
                if (item.state !== backup_state) {
                    // show notification
                    toastr.info(`${item.channelName} backup channel ${backup_state}`);

                    // update state into db
                    const _params = {
                        channel_Name: item.channelName,
                        _state: backup_state
                    }
                    $.post('/v1/livestreaming/updateBackupStateInMongoDb', _params,
                        function (data, status) {
                            if (data.status == 200) {
                                console.log(JSON.stringify(data))
                                var channel_Name = data.data.channelName
                                var state = data.data.state
                                var params = {
                                    createdChannelName: `The ${channel_Name}`,
                                    description: `backup-channel state is ${state}`
                                }
                                whatsapp(params)
                                SlackMsg(params)
                            } else {
                                //console.log("update mongo for change state failed")
                            }
                        })
                }
            })
        }
    })
    setTimeout(function () {
        getBackupServerStatus();
    }, 10000)
}
getBackupServerStatus();


function whatsapp(params) {
    var channelName = params.createdChannelName;
    var description = params.description;
    var arr = ["+918149582220", "+919819253242", "+919619621098", "+917709988970", "+918180939713", "+918007338762", "+919870121612", "+919867591221", "+918898818382", "+917777074363", "+917798334109", "+917710952597", "+919930292635", "+919763180255", "+919324239418", "+917038771512"]

    for (var i = 0; i < arr.length; i++) {
        var number = arr[i];
        var whatparams = {
            number: number,
            channelName: channelName,
            description: description
        }
        $.post('/v1/livestreaming/whatsapp', whatparams,
            function (data, status) {
                if (data.status == 200) {
                    // resolve(data)

                }
            });
    }
}


function SlackMsg(params) {
    var channelName = params.createdChannelName;
    var description = params.description;
    var slackParams = {
        channelName: channelName,
        description: description
    }
    $.post('/v1/livestreaming/SlackMsg', slackParams,
        function (data, status) {
            if (data.status == 200) {
                console.log(JSON.stringify(data))

            }
        });
}

