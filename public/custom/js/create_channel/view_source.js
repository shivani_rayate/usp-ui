$(document).on("click", ".view", function (e) {
    var ServerIp = $("#view_source").val()
    var channel_Name = $("#view_source option:selected").text()
    let params = {
        channel_Name: channel_Name,
        ServerIp: ServerIp
    }
    view_source(params)
})


// function view_source(params) {
//     return new Promise((resolve, reject) => {
//         $.post('/v1/viewsource/view_source', params,
//             function (data, status) {
//                 if (data.status == 200) {
//                     var _data = data.data
//                     format(_data)
//                     resolve(_data)
//                 }
//             })
//     }),
//         setTimeout(function () {
//             view_source(params);
//         }, 6000);
// }


function view() {
    channel_listing().then((channel_listing) => {
        console.log(JSON.stringify(channel_listing.data))
        var array = channel_listing.data
        var channelname = '';
        array.forEach(function (element, i) {
            var ServerIp = element.ServerIp
            var channelName = element.channelName
            channelname += `<option  value="${ServerIp}">${channelName}</option>`
        })
        $('#view_source').append(channelname);
    })
}
view()


function format(_data) {
    console.log(JSON.stringify(_data))
    $('#form_tbody').empty();
    var option = '';
    option += ` '<tr><td>${JSON.stringify(_data)}</td>`
    $('#form_tbody').append(option);
}