
$(function () {


    $.get('/v1/route53/listResourceRecordSets',
   // alert("listResourceRecordSets"),
   
        function (data, textStatus, request) {
            if (data.status === 200) {
                _data = data.data.ResourceRecordSets
               appendlistResourceRecord(_data)
             console.log("listResourceRecordSets >>"+JSON.stringify(_data))
              
            } else {
                toastr.danger('Creation failed.')
            }
        })
   

})
 


function  appendlistResourceRecord(array) {
    var array = array;
    // var abc = JSON.parse(array)
    var options_table = "";
    
    array.forEach(function (element, i) {
        var Name = element.Name ? element.Name : "";
        var SetIdentifier = element.SetIdentifier ? element.SetIdentifier : "";
        var Type = element.Type ? element.Type : "";
        var Weight = element.Weight ? element.Weight : "0";
        var TTL = element.TTL ? element.TTL : "";
        var ResourceRecordValue = element.ResourceRecords[0].Value ? element.ResourceRecords[0].Value : "";
        options_table += `<tr class="users-tbl-row asset-row">
       
        <td class="name">${i+1}</td>
        <td class="name">${Name}</td>
        <td class="name">${Type}</td>
        <td class="name">${SetIdentifier}</td>
        <td class="name">${Weight}</td>
        <td class="name">${TTL}</td>
        <td class="name" ResourceRecordValue='${ResourceRecordValue}' Weight='${Weight}' SetIdentifier='${SetIdentifier}' Type='${Type}' name='${Name}'  TTL='${TTL}'><i class="mdi-24px mdi mdi-update update_parameter" data-toggle="modal"data-toggle="modal" data-target="#migrateEventModal"></i></td>`;


   if (i == array.length - 1) {

            
            $('#resourceRecord_listing_tbody').append(options_table);
            reInitialiClientSheetListTable()
        }
    })
}


function destroyRowsxllist() {
    $('#resourceRecord_listing_tbody').empty()
    $('#resourceRecord_listing_table').DataTable().rows().remove();
    $("#resourceRecord_listing_table").DataTable().destroy()
}

function reInitialiClientSheetListTable() {
    $("#resourceRecord_listing_table").DataTable().destroy()
    xllist_table = $('#resourceRecord_listing_table').DataTable({
        //"order": [[1, "desc"]], // for descending order
       
    })

    
}

var SetIdentifier = ''
var ResourceRecordValue = ''
var Type = ''
var name = ''
var TTL = ''
$(document).on("click", ".update_parameter", function () {


    var params ={
        Weight : $(this).parents('td').attr('Weight'),
        SetIdentifier : $(this).parents('td').attr('SetIdentifier'),
        ResourceRecordValue : $(this).parents('td').attr('ResourceRecordValue'),
        Type : $(this).parents('td').attr('Type'),
        name : $(this).parents('td').attr('name'),
        TTL : $(this).parents('td').attr('TTL'),

    }

//alert("params >>"+JSON.stringify(params))
SetIdentifier = params.SetIdentifier
ResourceRecordValue = params.ResourceRecordValue
Type = params.Type
name = params.name
TTL = params.TTL
$('#weight').val(params.Weight)


})


//$(document).on("click", "#parameter_Update", function () {


$("#update-parameter").submit(function (event){
   
var params = {
   
    weight :  $('#weight').val().trim(),
    SetIdentifier : SetIdentifier,
    ResourceRecordValue : ResourceRecordValue,
    Type : Type,
    name : name,
    TTL : TTL
}

//alert(">>"+JSON.stringify(params))
$.post('/v1/route53/changeResourceRecordSets',params,
// alert("listResourceRecordSets"),

     function (data, textStatus, request) {
         if (data.status === 200) {
            
            toastr.success('Successfully Updated');
            setTimeout(function () {
                window.location.href = '/v1/route53'
            }, 2000);
           
         } else {
             toastr.danger('Creation failed.')
         }
     })

     event.preventDefault()
})
