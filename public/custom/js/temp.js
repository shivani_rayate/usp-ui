var mergedAssetPlayer;
var previewAssetPlayer;
var audioTrackList;
var audioPlayer = document.getElementById("audio-player");


// Initiate Preview Asset Player
function initiatePreviewAssetPlayer(params){

    return new Promise((resolve, reject) => {
        if (previewAssetPlayer !== undefined) {
            previewAssetPlayer.reset()
        }
        previewAssetPlayer = videojs(params.player_id)
        previewAssetPlayer.src({
            type: 'video/mp4',
            src: params.url
        });
        previewAssetPlayer.load();
        resolve()
    })
}


// get all s3 bucket Assets from dynamoDB
function listS3_PreviewAssets() {
    $.get('/v1/video/listS3_PreviewAssets',
        function (data, status) {
            if (data.status == 200) {
                // console.log("S3 Listing VIDEOS: " + JSON.stringify(data))
                destroyRows();
                appendVideosDatatable(data);
            }
        });
}



// document.ready
$(function () {
    listS3_PreviewAssets()

    // initialize jQuery sortable plugin
    $("#sortable").sortable();
    $("#sortable").disableSelection();

    $("#sortable_available_audio_tracks").sortable();
    $("#sortable_available_audio_tracks").disableSelection();


    // check event for save a copy asset
    $('#save_a_copy_asset_name').hide();
    $('#save_copy_check').change(function () {
        if ($(this).is(":checked")) {
            $('#save_a_copy_asset_name').show();
        } else {
            $('#save_a_copy_asset_name').hide();
        }
    });

})


// Refresh data table
$(document).on("click", "#table-refresh-btn", function () {
    listS3_PreviewAssets()
})


function destroyRows() {
    $('#processing_s3_assets_tbody').empty()
    $('#processing_s3_assets_table').DataTable().rows().remove();
    $("#processing_s3_assets_table").DataTable().destroy()
}

// for formatting bytes to readable file size
function formatBytes(x) {
    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let l = 0, n = parseInt(x, 10) || 0;

    while (n >= 1024 && ++l) {
        n = n / 1024;
    }
    //include a decimal point and a tenths-place digit if presenting 
    //less than ten of KB or greater units
    return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
}


function appendVideosDatatable(data) {

    var array = data.data.Items;
    var options_table = '';
    var preiview_languages = '';

    //console.log(JSON.stringify(array))

    array.forEach(function (element, i) {

        var assetid = element.assetid ? element.assetid : "";
        var asset_name = element.file_name ? element.file_name : "";
        var job_id = element.multi_lang_jobid ? element.multi_lang_jobid : "";
        var modified_on = element.creation_time ? moment(element.creation_time).format('lll') : "";
        var preview_status = element.status ? element.status : "";
        var merged_status = element.multi_lang_job_status ? element.multi_lang_job_status : ""
        var asset_size = '';
        var isLanguageUploaded = element.audio_info ? true : false;
        var audio_info = element.audio_info ? JSON.stringify(element.audio_info) : null;
        var video_path = element.video_path ? element.video_path : null;
        var preview_url = element.preview_url ? element.preview_url : null;
        var imdb_data_status = element.imdb_data_status ? element.imdb_data_status : "";
        let _imdb_data_status;


        var mediaInfo = (element.content_metadata != null) ? JSON.parse(element.content_metadata) : null;


        // extracted media info
        var video_meta_data = [];
        var audio_meta_data = [];

        if (mediaInfo != null && mediaInfo.media != null) {

            var mediaInfoTracks = mediaInfo.media.track ? mediaInfo.media.track : null;
            if (mediaInfoTracks !== null && mediaInfoTracks.length >= 1) {
                for (var j = 0; j < mediaInfoTracks.length; j++) {

                    var mediaType = mediaInfo.media.track[j]['@type'];
                    if (mediaType === 'General') {
                        asset_size = mediaInfo.media.track[j].FileSize;
                        asset_size = formatBytes(asset_size)
                    }
                    if (mediaType === 'Video') {
                        video_meta_data = JSON.stringify({
                            "Height": mediaInfo.media.track[j].Height,
                            "Width": mediaInfo.media.track[j].Width,
                            "BitRate": mediaInfo.media.track[j].BitRate,
                        })
                    }

                    if (mediaType === 'Audio') {
                        audio_meta_data.push({
                            'Title': mediaInfo.media.track[j].Title,
                            'Language': mediaInfo.media.track[j].Language,
                            'typeorder': mediaInfo.media.track[j]['@typeorder']
                        });
                    }
                }
            }
        }

        if (preview_status === 'COMPLETE') {
            preview_status = '<span class="badge badge-success">Available</span>'
        } else if (preview_status === 'PROGRESSING') {
            preview_status = '<span class="badge badge-purple">Processing</span>'
        } else {
            preview_status = '<span class="badge badge-danger">Error</span>'
        }


        if (merged_status === `COMPLETE`) {
            _merged_status = '<span class="badge badge-success">Available</span>'
        } else if (merged_status === 'PROGRESSING') {
            _merged_status = '<span class="badge badge-purple">Processing</span>'
        } else {
            _merged_status = '<span class="badge badge-pink">Pending</span>'
        }
        if (imdb_data_status === `Pending`) {
            var params = {
                asset_id: assetid,
                name: asset_name
            }
            getImdbData(params);
            _imdb_data_status = 'Pending'
        } else if (imdb_data_status === 'Unavailable' || imdb_data_status === "") {
            _imdb_data_status = 'Unavailable'
        } else if (imdb_data_status === 'Available') {
            _imdb_data_status = 'Available'
        }

        options_table += `<tr class="processing-list-asset-tbl-row asset-row" id=${assetid} data-asset_name =${asset_name} 
        data-job_id=${job_id} data-merged_status=${merged_status} data-isLanguageUploaded=${isLanguageUploaded} data-audio_meta_data='${JSON.stringify(audio_meta_data)}' data-audio_info=${audio_info} data-video_path=${video_path} data-video_meta_data=${video_meta_data}>
            <td class="">${(i + 1)}</td>
            <td class="asset-name">${asset_name}</td>
            <td class="asset-modified-on">${modified_on}</td>
            <td class="asset-preview">${preview_status}</td>
            <td class="asset-size">${asset_size}</td>
            <td class="asset-status" id="${job_id}">${_merged_status}</td>
            <td class="asset-action-td" id=${assetid} data-asset-name=${asset_name} data-imdb_data_status=${_imdb_data_status}> <div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item edit-metadata" data-imdb-data-status=${imdb_data_status} data-asset-name=${asset_name}>Edit</a> <a href="#" class="dropdown-item transcode-asset">Transcode</a><a href="#" class="dropdown-item create-meta-data" data-s3path=${preview_url}>Create Meta Data</a><a href="#" class="dropdown-item">Delete</a> 
            </td>`

        if (i == (array.length - 1)) {
            $('#processing_s3_assets_tbody').append(options_table)

            // add active class to first row
            $("#processing_s3_assets_table tbody tr:first").addClass("active");

            reInitializeDataTable()

            // check for Media Convert Status (BETA)
            //checkMediaConvertStatus();
        }

    })
}


var list_s3_assets_table;
function reInitializeDataTable() {
    $("#processing_s3_assets_table").DataTable().destroy()
    list_s3_assets_table = $('#processing_s3_assets_table').DataTable({
        //"order": [[2, "desc"]], // for descending order
        "columnDefs": [
            { "width": "35%", "targets": 1 }
        ]
    })
    // add refresh button to data table
    $('<button class="btn btn-secondary btn-sm waves-effect table-refresh-btn mr-3" id="table-refresh-btn"> <i class="mdi mdi-refresh mr-1"></i> <span>Refresh</span> </button>').prependTo("#processing_s3_assets_table_filter");
}



// Toggle right panel of TRANSCODE
$(document).on("click", ".transcode-asset", function () {
    
    // enlarge sidebar first
    $("#body-content").addClass("enlarged");
    // close Edit panel first
    $('#edit-asset-div').addClass("force-hidden")
    $('#listS3-preview-div').removeClass("col-md-12")
    $('#listS3-preview-div').addClass("col-md-7")
    $('#transcode-asset-div').removeClass("force-hidden")

    // asset title
    var asset_name = $(this).closest('tr').children('.asset-name').text();
    var asset_id = $(this).closest('tr').attr('id');

    $('#transcode-asset-title').html(asset_name)
    $('#transcode-asset-div').attr('data-asset_id', asset_id);

    // get media info from dynamodb
    getMediaInfo({ asset_id: asset_id })

})

// close right panel of TRANSCODE
$(document).on("click", "#close-transcode-asset-panel-btn", function () {
    // remove enlarge sidebar first
    $("#body-content").removeClass("enlarged");

    $('#listS3-preview-div').removeClass("col-md-7")
    $('#listS3-preview-div').addClass("col-md-12")
    $('#transcode-asset-div').addClass("force-hidden")
})



// Toggle right panel of EDIT
$(document).on("click", ".edit-metadata", function () {

    // enlarge sidebar first
    $("#body-content").addClass("enlarged");

    // close Transcode panel first
    $('#transcode-asset-div').addClass("force-hidden")

    $('#listS3-preview-div').removeClass("col-md-12")
    $('#listS3-preview-div').addClass("col-md-7")
    $('#edit-asset-div').removeClass("force-hidden")

    // asset title
    var asset_name = $(this).closest('tr').children('.asset-name').text();
    var asset_id = $(this).closest('tr').attr('id');

    $('#edit-asset-title').html(asset_name)
    $('#edit-asset-div').attr('data-asset_id', asset_id);


    var imdbDataStatus = $(this).attr("data-imdb-data-status")
    if (imdbDataStatus === 'Available') {
        $.get('/v1/video/findMetaData-imdb/' + asset_id,
            function (data, status) {

                if (data.status === 200) {
                    $('#technical-data').empty();
                    formatMetaData(data.data)
                } else {
                    $('#technical-data').empty();
                    $('#technical-data').append('<div class="">No media information available</div>');
                }
            })
    } else {
        $('#technical-data').empty();
        $('#technical-data').append('<div class="">No media information available</div>');
    }
})


// close-edit-metadata-panel
$(document).on("click", "#close-edit-metadata-panel-btn", function () {
    // remove enlarge sidebar first
    $("#body-content").removeClass("enlarged");

    $('#listS3-preview-div').removeClass("col-md-7")
    $('#listS3-preview-div').addClass("col-md-12")
    $('#edit-asset-div').addClass("force-hidden")
})



//update-IMD metadata status into dynamoDB
function updateImdbStatusIntoDynamoDB(params) {

    $.post('/v1/video/updateImdbStatus',
        params,
        function (data, status) {

            if (data.status === 200) {
                toastr.success(data.message);

                console.log(JSON.stringify(data.data))
            } else {
                toastr.error(data.message);
            }
        })

}



// GET IMDB
function getImdbData(params) {

    $.post('/v1/video/getImdbData',
        params,
        function (data, status) {
            // console.log(JSON.stringify(data))
            if (data.status === 200) {
                var _params = {
                    asset_id: params.asset_id,
                    imdb_data_status: 'Available'
                }
                updateImdbStatusIntoDynamoDB(_params)

            } else {
                console.log("IMDB data fetch failed")
                var _params = {
                    asset_id: params.asset_id,
                    imdb_data_status: 'Unavailable'
                }
                updateImdbStatusIntoDynamoDB(_params)

            }
        })
}


// GET MEDIA INFO
function getMediaInfo(params) {

    $.ajax({
        url: '/v1/video/listS3ContentMetadatatDynamodb',
        type: 'POST',
        data: JSON.stringify(params),
        headers: {
            'Content-Type': "application/json",
        },
        'success': function (data, textStatus, request) {
            if (data.status === 200) {
                formatMediaInfoData(data.data)

            } else {
                toastr.error(data.message);
            }
        },
        'error': function (request, textStatus, errorThrown) {
            console.log("ERR " + errorThrown)
        }
    })
}


function bytesToSize(bytes, seperator = "") {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
    if (bytes == 0) return 'n/a'
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
    if (i === 0) return `${bytes}${seperator}${sizes[i]}`
    return `${(bytes / (1024 ** i)).toFixed(1)}${seperator}${sizes[i]}`
}


// to convert minutes into req format
function TimeFormat() {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
}


// IMDB Meta-data formatting
function formatMetaData(data) {

    $('#technical-data').empty();
    var metadata_div = "";
    var mediaData = data.metadata;
    console.log("meta-data imdb > " + JSON.stringify(mediaData))

    var title = mediaData.title;
    var _year_data = mediaData._year_data;
    var rated = mediaData.rated;
    var released = mediaData.released;
    var runtime = mediaData.runtime;
    var genres = mediaData.genres;
    var director = mediaData.director;
    var writer = mediaData.writer;
    var actors = mediaData.actors;
    var plot = mediaData.plot;
    var languages = mediaData.languages;
    var country = mediaData.country;
    var awards = mediaData.awards;
    var poster = mediaData.poster;
    var ratings = mediaData.ratings;
    var production = mediaData.production;


    if (production === 'N/A' || imdb_data_status === "") {
        production = 'Not Available'
    }

    metadata_div += `<div class="row">
                   <div class="col-sm-6">
                   <img class="w-100" style="border-radius:5px; height:135px;" src="${poster}"></div>
                   <div class="col-sm-6">
                   <dl class="row">
                   <dt class="col-sm-1"></dt> 
                        <dd class="col-sm-9" style="font-size:25px;color:#fff;font-weight:300">${title} </dd> 
                    <dt class="col-sm-6">${runtime}</dt> 
                        <dd class="col-sm-6">${_year_data} </dd>
                   <dt class="col-sm-3">Director</dt> 
                        <dd class="col-sm-9" style="color:#439efb;">${director} </dd> 
                    <dt class="col-sm-3">Genres</dt> 
                        <dd class="col-sm-9" style="color:#fff;">${genres}  </dd>
                    <dt class="col-sm-4">Released</dt> 
                        <dd class="col-sm-8">${moment(released.creation_time).format('lll')}</dd> 
                    </dl>
                    </div>
                                       <div class="col-sm-12">
                                       <dl class="row">
                                       <dt class="col-sm-3">Writer</dt> 
                                       <dd class="col-sm-9">
                                       ${writer}  </dd>
                                       
                                       <dt class="col-sm-3">Actors</dt> 
                                       <dd class="col-sm-9">
                                       ${actors}  </dd>

                                       <dt class="col-sm-3">Languages</dt> 
                                       <dd class="col-sm-9">
                                       ${languages}  </dd>

                                       <dt class="col-sm-3">Country</dt> 
                                       <dd class="col-sm-9">
                                       ${country}  </dd>

                                       <dt class="col-sm-3">Ratings</dt> 
                                       <dd class="col-sm-9">
                                       ${JSON.stringify(ratings)}</dd>

                                       <dt class="col-sm-3">Production</dt> 
                                       <dd class="col-sm-9">
                                       ${production}  </dd>
                   
                                       </dl>
                                       </div>
                                       </div>
                   </div>`

    if (mediaData != null) {
        $('#technical-data').html(metadata_div)
    } else {
        $('#technical-data').html("No media information available")
    }
}


// for formatting media info
function formatMediaInfoData(metaData) {

    var mediaData = metaData.Item.content_metadata;
    var mediaInfo = (mediaData != null) ? JSON.parse(mediaData.S) : null;
    if (mediaInfo != null && mediaInfo.media != null) {
        var mediaInfoTracks = mediaInfo.media.track ? mediaInfo.media.track : null;
        if (mediaInfoTracks !== null && mediaInfoTracks.length >= 1) {
            var metadata_div = ``;
            for (var i = 0; i < mediaInfoTracks.length; i++) {

                var mediaType = mediaInfo.media.track[i]['@type'];
                if (mediaType === 'General') {
                    fileSize = mediaInfoTracks[i].FileSize ? mediaInfoTracks[i].FileSize : '';
                }
                if (mediaType === 'Video') {
                    Height = mediaInfo.media.track[i].Height ? mediaInfo.media.track[i].Height : '';
                    Width = mediaInfo.media.track[i].Width ? mediaInfo.media.track[i].Width : '';
                    BitRate = mediaInfo.media.track[i].BitRate ? mediaInfo.media.track[i].BitRate : '';
                }
                if (mediaType === 'Audio') {
                    Title = mediaInfo.media.track[i].Title ? mediaInfo.media.track[i].Title : ''
                }
            }

            metadata_div = `<div class="row">
            <div class="col-sm-3">Title</div>
            <div class="col-sm-9">${Title}</div>
            <div class="col-sm-3">FileSize</div>
            <div class="col-sm-9">${fileSize}</div>
            <div class="col-sm-3">Height</div>
            <div class="col-sm-9">${Height}</div>
            <div class="col-sm-3">Width</div>
            <div class="col-sm-9">${Width}</div>
            <div class="col-sm-3">BitRate</div>
            <div class="col-sm-9">${BitRate}</div>
            </div>`

            $('#media-info').html(metadata_div)
        }
    } else {
        $('#media-info').html("No media information available")
    }
}


// add audio file input group
$(document).on("click", "#add-audio-btn", function (e) {

    $('.add-audio-form-div').append('<div class="form-group row"> <div class="col-md-4"> <select class="form-control language dropMenu language-code-dropMenu" id="language" name="language"> <option value="" selected="selected">Select</option> <option value="ENG">English</option> <option value="HIN">Hindi</option> <option value="MAR">Marathi</option> <option value="TEL">Telugu</option> <option value="KAN">Kannada</option> <option value="BEN">Bengali</option> <option value="TAM">Tamil</option> </select> </div> <div class="col-md-6"> <input type="file" class="dropify form-control" data-height="30" id="files" name="files" accept="audio/*" /> </div> <div class="col-md-2 px-1"> <i class="ti-arrows-vertical audio-sort-handle"></i> <i class="mdi mdi-delete audio-delete"></i></div> </div>')

    // initialise dropify and select 2 plugin
    $(".dropify").dropify({ messages: { default: "Drag and drop a file here or click", replace: "Drag and drop or click to replace", remove: "Remove", error: "Ooops, something wrong appended." }, error: { fileSize: "The file size is too big (1M max)." } });
    $(".select2").select2()
    e.preventDefault()
})


// To upload audio files
$(document).ready(function () {

    // audio files upload >>
    $("#add-audio-files-form").bootstrapValidator({

        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            language: {
                validators: {
                    notEmpty: {
                        message: 'The language is required!'
                    }
                }
            },
            files: {
                validators: {
                    notEmpty: {
                        message: 'The file is required!'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {

        var formData = new FormData($(this)[0]);
        var language = [];

        var languageSelects = document.querySelectorAll('.language-code-dropMenu');
        languageSelects.forEach(function (el, i) {
            language[i] = el.value;
        })

        let params = {
            asset_id: $('#edit-asset-div').attr("data-asset_id"),
            language: language,
        }
        //  console.log(JSON.stringify(params))

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                $(".modal_progress_btn").click()
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $(".progress-div").css("display", "block")
                        $("#percent_complete").text(percentComplete + " %");
                        $(".progress-bar-video").css("width", percentComplete + '%');

                        if (percentComplete === 100) {
                            console.log("*****COMPLETE DONEEEEE******");
                        }

                    }
                }, false);

                return xhr;
            },
            url: '/v1/video/uploadAudio/' + JSON.stringify(params),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).then(function (data) {

            if (data.status == 200) {

                toastr.success('Audio Files Uploaded Successfully.');
                setTimeout(function () {
                    window.location.href = '/v1/video/advance-transcoding'
                }, 3000);

            } else {
                console.log("Oops! AUDIO Save ERROR >  " + data.message)

            }
        })

        e.preventDefault()
    })
})



// To transcode into default profile with added languages
$(document).on("click", ".transcode-asset-btn", function (e) {

    var asset_id = $('#transcode-asset-div').attr('data-asset_id')

    // var islanguageuploaded = $(`#${asset_id}`).attr("data-islanguageuploaded");
    // if (islanguageuploaded === 'true') {

    var video_meta_data = $(`#${asset_id}`).attr("data-video_meta_data")
    video_meta_data = JSON.parse(video_meta_data);
    var languages = new Array();

    $('#sortable_available_audio_tracks').find('.audio-track-div').each(function () {

        var track_type = $(this).attr("data-track_type");
        if (track_type === "default") {
            languages.push({
                'track': $(this).attr("data-track_number"),
                'type': `default`
            });
        } else {
            languages.push({
                'name': $(this).attr("data-track_code"),
                'url': `s3://dev-skandha-vod/disney-low-resolution/${asset_id}/${$(this).attr("data-track_name")}`,
                "type": "custom"
            });
        }
    });

    var params = {
        assetid: asset_id,
        video_url: $(`#${asset_id}`).attr("data-video_path"),
        languages: languages,
        Height: video_meta_data.Height,
        Width: video_meta_data.Width,
        Bitrate: video_meta_data.BitRate,
        save_as: "hindi"
    }
    console.log("transcode-asset params >> " + JSON.stringify(params));

    $.post('/v1/video/transcode-asset-disney',
        params,
        function (data, status) {
            if (data.status === 200) {
                toastr.success(data.message);

                // to refresh table
                setTimeout(function () {
                    listS3_PreviewAssets();
                    setTimeout(function () {
                        var job_id = $(`#${asset_id}`).children('.asset-status').attr('id');

                        console.log(`getMediaConvertStatus called for >> ${job_id} >> after 15 seconds >>`);
                        if (job_id) {
                            getMediaConvertStatus(job_id)
                        }
                    }, 10000);
                }, 5000);

            } else {
                toastr.error(data.message);
            }
        })
    // } else {
    //     toastr.info('Please Upload Audio Files');
    // }
})


// Initiate Merged Asset Player
function initiateMergedAssetPlayer(player_id, url) {

    return new Promise((resolve, reject) => {
        if (mergedAssetPlayer !== undefined) {
            mergedAssetPlayer.reset()
        }

        mergedAssetPlayer = videojs(player_id)
        mergedAssetPlayer.src({
            type: 'video/mp4',
            src: url
        });
        mergedAssetPlayer.load();
        resolve()
    })
}


// ##################### View Merged Asset ########################
$(document).on("click", ".play-merged-asset", function () {

    var status = $(this).closest('.master-asset').attr("data-status");
    var assetid = $(this).closest('.master-asset').attr("data-assetid");

    if (status === 'COMPLETE') {
        //initiate player
        var merged_video_url = $(this).closest('.master-asset').attr("data-url");
        initiateMergedAssetPlayer('merged-asset-player', merged_video_url).then(() => {
            console.log("merged asset player is ready")
            $('#play_merged_asset_modal').modal('show');
        })

        // get Azure meta data
        
        getazuremetadata(assetid);

    } else {
        toastr.info("Preview Not Available");
    }

})

// create meta data (AZURE SNS TRIGGER)
$(document).on("click", ".create-meta-data", function () {

    var asset_id = $(this).closest('tr').attr('id');
    var s3path = $(this).attr("data-s3path");

    if (s3path != null) {
        var params = {
            assetid: asset_id,
            s3path: s3path
        }
        $.post('/v1/video/create-meta-data-azure',
            params,
            function (data, status) {
                if (data.status === 200) {
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            })
    } else {
        toastr.error("Not Available");
    }
})


// get meta data (AZURE GET METADATA)
var azureData = '';
function getazuremetadata(asset_id) {

    var params = {
        assetid: asset_id
    }

    $.post('/v1/video/get-meta-data-azure',
        params,
        function (data, status) {
            if (data.status === 200) {
                //console.log("getazuremetadata " + JSON.stringify(data))
                azureData = data.data.Item.final_data.M;
            } else {
                toastr.error(data.message);
            }
        })
}


// Video Indexer UI Formatting
function formatVideoIndexer(params) {

    var _currentTime = params.currentTime

    //console.log("FINALLY >> " + JSON.stringify(params))


    if (azureData !== '') {

        //var transcript = azureData.transcript ? azureData.transcript.S : '';
        var faces = azureData.faces ? azureData.faces.L : '';
        // var labels = azureData.labels ? azureData.labels.L : '';

        // if (transcript !== '' && transcript.length > 1) {
        //     //console.log("transcript >>> " + JSON.stringify(transcript))
        //     for (var i = 0; i < transcript.length; i++) {

        //     }
        // }
        //if (faces !== '' && faces.length > 1) {

        // for (var j = 0; j < faces.length; j++) {

        //     var start = '';
        //     start = faces[j].M.instances.L[0].M.start.S;

        //     var _start = '';
        //     _start = _start.substring(0, _start.lastIndexOf("."));

        //     if (start !== '') {
        //         console.log("_start >>> " + _start)
        //     }
        //     if (_start !== '') {
        //         console.log("_start >>> " + _start)
        //     }


        // }

        //var tuiu = faces[3].M.instances.L[0].M.end.S
        //moment().format('h:mm:ss')

        //console.log("faces.M.instances.L[0].M.start >>> " + faces.M.instances.L[0].M.start)

        // for (var j = 0; j < faces.length; j++) {

        //     if (_currentTime === faces.M.instances.L[0].M.start) {
        //         $('#tbody-video-indexer').append(`<tr><td>a</td><td>Otto</td><td>@mdo</td></tr>`)
        //     }

        // }
        // }
        // if (labels !== '' && labels.length > 1) {
        //     //console.log("labels >>> " + JSON.stringify(labels))
        //     for (var k = 0; k < labels.length; k++) {

        //     }
        // }

    }
    else {
        $('#video_indexer_assets_table').html("No media information available")
    }
}


// Video Player Progress Event for Video Indexer
var _merged_asset_player = document.getElementById("merged-asset-player");
_merged_asset_player.ontimeupdate = function () {

    currentTime = Math.round(_merged_asset_player.currentTime);
    startTime = new Date(currentTime * 1000).toISOString().substr(11, 8);

    duration = Math.round(_merged_asset_player.duration);

    let params = {
        currentTime: startTime,
        duration: duration
    }
    formatVideoIndexer(params)
};


// getAsset
function getAsset(asset_id) {

    return new Promise((resolve, reject) => {
        $.post('/v1/video/listS3ContentMetadatatDynamodb',
            {
                asset_id: asset_id
            },
            function (data, status) {
                if (data.status == 200) {
                    let _data = data.data;
                    resolve(_data)
                } else if (data.status == 400) {
                    reject()
                }
            })
    });
}


// Asset Package Selection
$(document).on("click", ".processing-list-asset-tbl-row", function (e) {
    $('.processing-list-asset-tbl-row').removeClass("active");
    $(this).addClass("active");

    // ########## child rows code ########### //
    var tr = $(this).closest('tr');
    var row = list_s3_assets_table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    } else {
        let asset_id = $(".processing-list-asset-tbl-row.active").attr('id');

        // getAsset details
        getAsset(asset_id).then((asset_data) => {

            // to close the child of row which is already open
            if (list_s3_assets_table.row('.shown').length) {
                var _tr = $('tr.shown');
                var _row = list_s3_assets_table.row(_tr);
                _row.child.hide();
                _tr.removeClass('shown');
            }

            formatAssetDetails(asset_data).then((assetDetails) => {
                // Open this row
                row.child(assetDetails).show();
                tr.addClass('shown');
            })

        })
    }

})


function _formateAudioTracks(asset_data) {
    return new Promise((resolve, reject) => {

        let asset_id = asset_data.assetid.S;
        let audioTracksArr = asset_data.audio_info ? asset_data.audio_info.L : null;

        // formate extracted DefaultAudioTracks in EDIT section
        var audio_meta_data = $(`#${asset_id}`).attr('data-audio_meta_data');

        if (audio_meta_data != null && audio_meta_data != 'undefined' && audioTracksArr != null) {

            var audio_tracks = JSON.parse(audio_meta_data)
            $('.available-audio-tracks-div').empty();

            // show extratced default audio tracks
            formateDefaultAudioTracks(audio_tracks).then(() => {
                // show already uploadedAudioTracks in Edit section
                formateUploadedAudioTracks(audioTracksArr)
            })

        } else if (audio_meta_data != null && audio_meta_data != 'undefined') {

            var audio_tracks = JSON.parse(audio_meta_data)
            $('.available-audio-tracks-div').empty();

            // show extratced default audio tracks
            formateDefaultAudioTracks(audio_tracks)

        } else if (audioTracksArr != null) {
            $('.available-audio-tracks-div').empty();
            // show already uploadedAudioTracks in Edit section
            formateUploadedAudioTracks(audioTracksArr)
        }

        if (audioTracksArr != null) {

            var options = '<div class="audio-tracks-container mt-1" style="display:none"> <div class="row">';
            for (var i = 0; i <= audioTracksArr.length; i++) {
                options += `<div class="col-md-12"><ul class="list-group list-group-flush"><li class="list-group-item audio-asset" data-url="https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/disney-low-resolution/${asset_id}/${audioTracksArr[i].M.fileName.S}">
                <div class="row">
                <div class="col-md-6"><i class="ti-music-alt">&nbsp${audioTracksArr[i].M.langauge.S}</i>.${getFileExtension(audioTracksArr[i].M.fileName.S)}</div><div class="col-md-6 text-right">
                <button type="button" class="btn btn-bordred-dark waves-effect waves-light btn-xs play-audio-track-btn" data-toggle="modal" data-target="#audioPlay">Play</button>
                </div></i></ul></div>`;

                if (i === (audioTracksArr.length - 1)) {
                    options += `</div></div>`;
                    resolve(options)
                }
            }

        }
        else {
            resolve(null)
        }

    });
}



// change src of audio player
$(document).on("click", ".play-audio-track-btn", function () {
    var audioSource = document.getElementById('audioSource');
    audioSource.src = $(this).closest('.audio-asset').attr('data-url');
    audioPlayer.load(); //call this to just preload the audio without playing
    //audioPlayer.play(); //call this to play the song right away
})


// show extracted default audio tracks
function formateDefaultAudioTracks(audioTracksArr) {
    return new Promise((resolve, reject) => {

        var options_tracks = ''
        for (var j = 0; j < audioTracksArr.length; j++) {
            if (audioTracksArr[j].Title) {
                options_tracks += `<div class="form-group row audio-track-div" data-track_number=${j + 1} data-track_type=default><div class="col-md-4"> <select class="form-control audio_tracks" disabled> <option  selected="selected">Track_${audioTracksArr[j].Title}</option></select> </div> <div class="col-md-6"><input type="text" class="form-control" value="Default Track ${audioTracksArr[j].typeorder}" disabled></div> <div class="col-md-2 px-1"> <i class="ti-arrows-vertical audio-sort-handle" title="sort"></i> <i class="mdi mdi-delete audio-delete" title="delete"></i> </div> </div>`
            } else {
                options_tracks += `<div class="form-group row audio-track-div" data-track_number=${j + 1} data-track_type=default><div class="col-md-4"> <select class="form-control audio_tracks" disabled> <option  selected="selected">Track_${audioTracksArr[j].typeorder}</option></select> </div> <div class="col-md-6"><input type="text" class="form-control" value="Default Track ${audioTracksArr[j].typeorder}" disabled></div> <div class="col-md-2 px-1"> <i class="ti-arrows-vertical audio-sort-handle" title="sort"></i> <i class="mdi mdi-delete audio-delete" title="delete"></i> </div> </div>`
            }

            if (j === (audioTracksArr.length - 1)) {
                $('.available-audio-tracks-div').append(options_tracks);
                resolve()
            }
        }
    })
}


// formate already UploadedAudioTracks
function formateUploadedAudioTracks(audioTracksArr) {
    //console.log("audioTracks >> " + JSON.stringify(audioTracksArr))
    var options = ``
    for (var i = 0; i < audioTracksArr.length; i++) {

        languageCode = audioTracksArr[i].M.langauge.S;
        fileName = audioTracksArr[i].M.fileName.S;
        options += `<div class="form-group row audio-track-div" data-track_name=${fileName} data-track_code=${languageCode} data-track_type="custom"> <div class="col-md-4"> <select class="form-control language dropMenu" disabled> <option value="${languageCode}" selected="selected">${languageCode}</option></select> </div> <div class="col-md-6"><input type="text" id="default_audio${i}" class="form-control" value="${fileName}" disabled></div> <div class="col-md-2 px-1"> <i class="ti-arrows-vertical audio-sort-handle" title="sort"></i> <i class="mdi mdi-delete audio-delete" title="delete"></i> </div> </div>`

        if (i === (audioTracksArr.length - 1)) {
            $('.available-audio-tracks-div').append(options)
        }
    }
}


// to remove upload audio file row
$(document).on("click", ".audio-delete", function () {
    $(this).closest('.row').remove();
})


function formatAssetDetails(asset_data) {
    //console.log("asset_data > " + 
    // JSON.stringify(asset_data))

    return new Promise((resolve, reject) => {

        asset_data = asset_data.Item;
        //let asset_name = asset_data.element.file_name ?  asset_data.element.file_name:'';
        let assetid = asset_data.assetid.S;
        let S3_multi_lang_url = asset_data.S3_multi_lang_url;
        let previewStatus = asset_data.status ? asset_data.status.S : 'Processing';
        let mergedStatus = asset_data.multi_lang_job_status ? asset_data.multi_lang_job_status.S : 'Processing';
        // let audio_info= asset_data.audio_info ?JSON.stringify(asset_data.audio_info): null;
        let master = '';
        let preview = '';
        let formateThumbnails = '';
        let formatedAssetDetails = '';
        let audioTracksFolder = '';
        if (asset_data.preview_url) {
            preview = `<li class="list-group-item preview-asset" data-url='${asset_data.preview_url.S}' data-status='${previewStatus}'><div class="row">
            <div class="col-md-6"><i class="mdi mdi-video"></i>preview.mp4</div>
            <div class="col-md-6 text-right">
             <div class="dropdown"> 
             <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
             <i class="fe-settings noti-icon"></i> 
             </a> 
             <div class="dropdown-menu dropdown-menu-right">
             <a href="#" class="dropdown-item play-preview-asset">Play</a>
              <a href="#" class="dropdown-item getlink-preview-asset">Get link</a>
             </div>
             </div></li>`;
            formatedAssetDetails += preview;
        } else {
            preview = ''
        }

        if (mergedStatus !== 'PROGRESSING' && asset_data.S3_multi_lang_url) {
            master = `<li class="list-group-item master-asset" data-url='${asset_data.S3_multi_lang_url.S}' data-status='${mergedStatus}' data-assetid='${assetid}'><div class="row"><div class="col-md-6"><i class="mdi mdi-video"></i>master.mp4</div>
            <div class="col-md-6 text-right">
            <div class="dropdown"> 
             <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
             <i class="fe-settings noti-icon"></i> 
             </a> 
             <div class="dropdown-menu dropdown-menu-right">
             <a href="#" class="dropdown-item play-merged-asset">Play</a>
              <a href="#" class="dropdown-item getlink-master-asset">Get link</a>
            </div></div></li>`;
            formatedAssetDetails += master;
        } else {
            master = ' '
        }

        // audio tracks
        if (asset_data.status.S === 'COMPLETE') {
            _formateAudioTracks(asset_data).then((audioTracks) => {
                if (audioTracks != null) {

                    // audioCount
                    let audioTracksArr = asset_data.audio_info ? asset_data.audio_info.L : null;
                    var audioCount = audioTracksArr.length;
                    formateAudioTracks = audioTracks;
                    formatedAssetDetails += formateAudioTracks;
                    audioTracksFolder = `<li class="list-group-item audio-tracks-folder" data_audio_info ='${JSON.stringify(asset_data.audio_info.L)}'><i class="mdi mdi-folder"></i>Audio tracks(${audioCount})</li></ul> ${formateAudioTracks}`

                } else {
                    audioTracksFolder = ``
                }

                resolve(`<ul class="list-group list-group-flush">${preview}${master}${audioTracksFolder}`)
            })
        } else {
            resolve('<div class="asset-details"></div>')
        }

    })
}


// audio tracks folder toggle
$(document).on("click", ".audio-tracks-folder", function () {
    $(".audio-tracks-container").toggle();
})


// preview video
$(document).on("click", ".play-preview-asset", function () {

    var preview_url = $(this).closest('.preview-asset').attr("data-url");
    var status = $(this).closest('.preview-asset').attr("data-status");

    if (status === 'COMPLETE') {
        var languages = $(this).attr('data-audio_info');
        var params = {
            player_id: 'preview-asset-player',
            url: preview_url,
            languages: languages
        }

        initiatePreviewAssetPlayer(params).then(() => {
            console.log("Preview Player is ready");

            $('#previewVideo').modal('show');
        })
    } else {
        toastr.info("Preview Not Available");
    }

})

// getlink preview asset
$(document).on("click", ".getlink-preview-asset", function () {

    var url = $(this).closest('li').attr('data-url');
    var asset_name = url.substring(url.lastIndexOf('/') + 1);
    var id = $(this).closest('tr').prev().attr('id');
    params = {
        key: `/${id}/${asset_name}`  // key => path of file
    }
    $.post('/v1/video/getlink-preview',
        params,
        function (data, status) {
            if (data.status === 200) {
                $('#getlinkurl').val(data.data);
                $('#getlink').modal('show');

            } else {
                toastr.error(data.message);
            }
        })

})

// getlink master asset
$(document).on("click", ".getlink-master-asset", function () {

    var url = $(this).parents('li').attr('data-url');
    var asset_name = url.substring(url.lastIndexOf('/') + 1);
    var id = $(this).closest('tr').prev().attr('id');
    params = {
        key: `/${id}/${asset_name}`  // key => path of file
    }
    $.post('/v1/video/getlink-preview',
        params,
        function (data, status) {
            if (data.status === 200) {
                $('#getlinkurl').val(data.data);
                $('#getlink').modal('show');

            } else {
                toastr.error(data.message);
            }
        })
})


function copy() {

    var copyText = document.getElementById("getlinkurl");
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");

}


// stop audio player
function stopAudioPlayer() {
    audioPlayer.pause();
    audioPlayer.currentTime = 0;
}


// getFileExtension
function getFileExtension(filename) {
    return filename.split('.').pop();
}

// get Media convert status by job_id
function getMediaConvertStatus(job_id) {
    $.get(`/v1/video/getMediaConvertStatus/${job_id}`,
        function (data, status) {
            if (data.status === 200) {
                var _job_id = data.data.Job.Id;
                var _status = data.data.Job.Status;
                console.log(`getMediaConvertStatus response for >> ${_job_id} >> ${_status}`);
                $(`#${_job_id}`).html('<span class="badge badge-success">Available</span>');


                // send notification

                var params = {

                    name: $(`#${_job_id}`).siblings('.asset-name').text(),
                    description: "Media conversion completed",
                    //  action:"Transcoded",

                }
                saveNotification(params);

            } else {
                setTimeout(function () {
                    console.log(`getMediaConvertStatus called for >> ${job_id} >> after 5 seconds >>`);
                    getMediaConvertStatus(job_id)
                }, 5000);
            }
        });
}