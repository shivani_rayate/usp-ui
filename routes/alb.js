var express = require('express');
var router = express.Router();



// var username = '736b5558-327f-4081-858b-b27b35446ffd'
// var passw = '2db2a834-6bbf-4b78-a972-e60aac7936f7'

var Buffer = require('buffer/').Buffer

const Alb = require("../workers/alb");
const albObj = new Alb.AlbClass();

let server = require('../config/server_config.js');

const AWS = require("aws-sdk");
const request = require('request');


var lambda = new AWS.Lambda();
let awsconfig = require('../config/aws_config.js');

const cognito = require('../app/utilities/user_management/cognito');

router.get('/', function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('alb', { userData: userData, userName: userName, userRole: userRole });

});


// create_channel_on_alb
router.post('/create_channel_on_alb', (req, res, next) => {

  var channelName = req.body.channelName;

  var primary_listner_arn = req.body.Listener;

  var primary_target_arn = req.body.targetArn;
  
  var TargetName = req.body.TargetName;

  const randomInt = require('random-int');
  var channelpriority = randomInt(37, 1000);
  

  var params = {
    channelName: channelName,
    primary_listner_arn: primary_listner_arn,
    primary_target_arn: primary_target_arn,
    channelpriority: channelpriority
    
  }
  
  cognito.Albrule(params).then((response) => {
    if (response) {
     
      var saveparams = {
        RuleArn: response.Rules[0].RuleArn,
        channelName: channelName,
        TargetName: TargetName
      }
    
      albObj.save(saveparams).then((_response) => {
        if (_response) {

          res.json({
            status: 200,
            message: 'channel Saved successfuly',
            data: _response,
          })
        }
        else {
          console.log("Operation Failed")
        }

      })

    } else {
      res.json({
        status: 403,
        message: response.message,
        data: response.data,
      })
    }
  })

})




router.post('/alb_channel_listing1', (req, res, next) => {
var params ={
  channelName:req.body.channelName
}
  albObj.findone(params).then((data) => {
    if (data) {

      res.json({
        status: 200,
        message: 'channels fetched successfully',
        data: data,
      })
    }
  }).catch((err) => {
    console.log(`error: ${err}`);
    res.json({
      status: 401,
      message: 'channels fetched failed',
      data: null,
    });
  });

})

router.get('/alb_channel_listing', (req, res, next) => {

  albObj.findall().then((data) => {
    if (data) {

      res.json({
        status: 200,
        message: 'channels fetched successfully',
        data: data,
      })
    }
  }).catch((err) => {
    console.log(`error: ${err}`);
    res.json({
      status: 401,
      message: 'channels fetched failed',
      data: null,
    });
  });

})
router.post('/delete_primaryChannel_from_alb', (req, res, next) => {

  var RuleArn = req.body.RuleArn_http;

  console.log(JSON.stringify(RuleArn))

  var delete_params_http = {
    RuleArn: RuleArn


  };


  cognito.DeleteRuleARN(delete_params_http).then((response) => {
    if (response) {

      var deleteparams = {
        RuleArn: RuleArn

      }
      albObj.deleteChannel(deleteparams).then((data) => {
        res.json({
          status: 200,
          message: 'Delete-channel successfully',
          data: data,
        });

      });

    }

  })
})

module.exports = router;