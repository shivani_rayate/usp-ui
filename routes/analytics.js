var express = require('express');
var router = express.Router();

const Channel = require("../workers/channels");
const channelObj = new Channel.ChannelClass();

const backupChannel = require("../workers/backupchannels");
const backupchannelObj = new backupChannel.BackupChannelClass();

let server = require('../config/server_config.js');
let BASE = server.base.BackUp;
let BASE_Primary = server.base.Primary;

const AWS = require("aws-sdk");
const request = require('request');



router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('analytics', {userData: userData, userName: userName, userRole: userRole });

});


router.get('/backupchannel_listing', (req, res, next) => {

    backupchannelObj.findall().then((data) => {
        if (data) {
            console.log('backupchannel_listing fetched successfully');
            res.json({
                status: 200,
                message: 'backupchannel_listing fetched successfully',
                data: data,
            })
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'channels fetched failed',
            data: null,
        });
    });

})




module.exports = router;