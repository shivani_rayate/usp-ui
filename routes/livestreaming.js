var express = require('express');
var router = express.Router();
var axios = require('axios');

const Channel = require("../workers/channels");
const channelObj = new Channel.ChannelClass();

const backupChannel = require("../workers/backupchannels");
const backupchannelObj = new backupChannel.BackupChannelClass();

var username = '736b5558-327f-4081-858b-b27b35446ffd'
var passw = '2db2a834-6bbf-4b78-a972-e60aac7936f7'

var Buffer = require('buffer/').Buffer

const userActivity = require("../workers/userActivity");
const userActivityObj = new userActivity.userActivityClass();

let server = require('../config/server_config.js');
let BASE = server.base.BackUp;
let BASE_Primary = server.base.Primary;
const fs = require('fs');
const AWS = require("aws-sdk");
const request = require('request');
// var parseXML = require("xml-parse-from-string")
var xml2js = require('xml2js');
var parser = new xml2js.Parser();

var parseString = require('xml2js').parseString;
const nodemailer = require("nodemailer");

var awsSesMail = require('aws-ses-mail');

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const xhr = new XMLHttpRequest();
var utf8 = require('utf8');

var lambda = new AWS.Lambda();
let awsconfig = require('../config/aws_config.js');
let primary_listner_arn = awsconfig.development.Primary_listener;
let primary_target_arn = awsconfig.development.Primary_target;
let primary_listner_https = awsconfig.development.Primary_listner_https;
let primary_target_https = awsconfig.development.Primary_target_https;
let functionname = awsconfig.development.FunctionName;

let secondary_listner_arn = awsconfig.development.Secondary_listner;
let secondary_target_arn = awsconfig.development.Secondary_target;
let secondary_listner_https = awsconfig.development.Secondary_listner_https;
let secondary_target_https = awsconfig.development.Secondary_target_https;


router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('livestreaming', { userData: userData, userName: userName, userRole: userRole });

});



router.post('/create_Channel_On_Primary', (req, res, next) => {

    var xmlData = req.body.xmlData;
    var xmlFileName = req.body.channelName;
    var channelName = req.body.channelName;
    var ServerIp = req.body.serverIp;
    var hlsNoAudioOnly = req.body.hlsNoAudioOnly;
    var PrimarytargetGroup = req.body.PrimarytargetGroup;

    var url = `http://${ServerIp}/${channelName}/${xmlFileName}.isml`
    console.log("url" + JSON.stringify(url))

    request.post({
        url: `http://${ServerIp}:3001/${channelName}/${xmlFileName}.isml`,
        method: "POST",
        headers: {
            'Content-Type': 'application/xml',
        },
        body: xmlData,
    },
        function (error, response, body) {

            if (response.statusCode === 201) {

                // const randomInt = require('random-int');
                // var channelpriority = randomInt(37, 1000);
                //Listner http port 80
                // let elb_primary_params = {
                //     FunctionName: functionname, /* required */
                //     Payload: JSON.stringify({ "listener-arn": primary_listner_arn, "target-arn": PrimarytargetGroup, "channel-priority": channelpriority, "channel-path": "/" + channelName + "/*" }),
                //     Payload: JSON.stringify({ "listener-arn": primary_listner_arn, "target-arn": PrimarytargetGroup, "channel-priority": channelpriority, "channel-path": "/" + channelName + "/*" })
                // }

                // lambda.invoke(elb_primary_params, function (err, data) {
                // if (err) {
                //     console.log(err, err.stack); // an error occurred
                // }
                // else {
                //     var array = data.Payload;
                //     array = JSON.parse(array);
                //     var rules = array.Rules;
                //     var RuleArn = rules[0].RuleArn;

                //Listner https port 443
                //  let elb_primary_https_params = {
                //     FunctionName: functionname, /* required */
                //     Payload: JSON.stringify({ "listener-arn": primary_listner_arn, "target-arn": PrimarytargetGroup, "channel-priority": channelpriority, "channel-path": "/" + channelName + "/*" }),
                //     Payload: JSON.stringify({ "listener-arn": primary_listner_https, "target-arn": PrimarytargetGroup, "channel-priority": channelpriority, "channel-path": "/" + channelName + "/*" })
                // }

                //  lambda.invoke(elb_primary_https_params, function (err, data) {
                // if (err) {
                //     console.log(err, err.stack); // an error occurred
                // }
                // else {
                // var array1 = data.Payload
                // array1 = JSON.parse(array1)
                // var rules1 = array1.Rules;
                // var RuleArn_http = rules1[0].RuleArn;
                let params = {
                    url: url,
                    channelName: req.body.channelName ? req.body.channelName : null,
                    creator: req.body.creator ? req.body.creator : null,
                    lookaheadFragments: req.body.lookaheadFragments ? req.body.lookaheadFragments : null,
                    dvrWindowLength: req.body.dvrWindowLength ? req.body.dvrWindowLength : null,
                    archiveSegmentLength: req.body.archiveSegmentLength ? req.body.archiveSegmentLength : null,
                    archiving: req.body.archiving ? req.body.archiving : null,
                    archiveLength: req.body.archiveLength ? req.body.archiveLength : null,
                    restartOnEncoderReconnect: req.body.restartOnEncoderReconnect ? req.body.restartOnEncoderReconnect : null,
                    issMinimumFragmentLength: req.body.issMinimumFragmentLength ? req.body.issMinimumFragmentLength : null,
                    hlsMinimumFragmentLength: req.body.hlsMinimumFragmentLength ? req.body.hlsMinimumFragmentLength : null,
                    ServerIp: req.body.serverIp ? req.body.serverIp : null,
                    serverName: req.body.serverName ? req.body.serverName : null,
                    flag: req.body._flag,
                    state: req.body.state ? req.body.state : null,
                    terminationPolicy: req.body.terminationPolicy,
                    // RuleArn_https: RuleArn_http,
                    hlsNoAudioOnly: req.body.hlsNoAudioOnly,
                    // RuleArn_http: RuleArn
                }
                channelObj.save(params).then((_response) => {
                    if (_response) {
                        res.json({
                            status: 200,
                            message: 'channel Saved successfuly',
                            data: _response,
                        })
                    }
                    else {
                        console.log("Operation Failed")
                    }
                    console.log("body >> " + body);
                    console.log(error);
                });
                // }
                // });

            }
            // });
            // }

        })
})

router.post('/stateWiseEmailSend', (req, res, next) => {

    const params = {
        createdChannelName: req.body.createdChannelName ? req.body.createdChannelName : null,
        serverIp: req.body.serverIp ? req.body.serverIp : null,
        description: req.body.description ? req.body.description : null
    }

    var sesMail = new awsSesMail();
    var sesConfig = {
        accessKeyId: 'AKIA2IYDDSCDRUKXHUT5',
        secretAccessKey: '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',
        region: 'ap-south-1'
    };

    sesMail.setConfig(sesConfig);

    var options = {
        from: 'pshinde@skandha.in',
        to: ['pvitonde@skandha.in'],
        subject: 'USP Live',
        content: `${params.createdChannelName}  ${params.description}`
    };

    sesMail.sendEmail(options, function (data) {
        console.log(data);
    });


})

// Create Channel On Backup Server then MongoDB 
router.post('/create_Channel_On_Backup', (req, res, next) => {

    var xmlData = req.body.xmlData;
    var xmlFileName = req.body.xmlFileName;
    var channelName = req.body.channelName;
    var backupserverIp = req.body.backupserverIp;
    var hlsNoAudioOnly = req.body.hlsNoAudioOnly;
    var BackupTargetGroup = req.body.BackupTargetGroup;

    var url = `http://${backupserverIp}/${channelName}/${xmlFileName}.isml`

    request.post({
        url: `http://${backupserverIp}:3001/${channelName}/${xmlFileName}.isml`,
        method: "POST",
        headers: {
            'Content-Type': 'application/xml',
        },
        body: xmlData,
    },
        function (error, response, body) {
            if (response.statusCode === 201) {
                // const randomInt = require('random-int');
                // var channelpriority = randomInt(37, 1000);
                //Listner http port 80
                // let elb_secondary_params = {
                //     FunctionName: functionname, /* required */
                //     Payload: JSON.stringify({ "listener-arn": secondary_listner_arn, "target-arn": BackupTargetGroup, "channel-priority": channelpriority, "channel-path": "/" + channelName + "/*" }),

                // }

                // lambda.invoke(elb_secondary_params, function (err, data) {
                // if (err) {
                //     console.log(err, err.stack); // an error occurred
                // }
                // else {

                //     var array = data.Payload;
                //     array = JSON.parse(array);
                //     var rules = array.Rules;
                //     var RuleArn = rules[0].RuleArn;
                //     //Listner https port 443
                //     let elb_secondary_https_params = {
                //         FunctionName: functionname, /* required */
                //         Payload: JSON.stringify({ "listener-arn": secondary_listner_https, "target-arn": BackupTargetGroup, "channel-priority": channelpriority, "channel-path": "/" + channelName + "/*" })
                //     }
                // lambda.invoke(elb_secondary_https_params, function (err, data) {
                //     if (err) {
                //         console.log(err, err.stack); // an error occurred
                //     }
                //     else {

                // var array1 = data.Payload;
                // array1 = JSON.parse(array1);
                // var rules1 = array1.Rules;
                // var RuleArn_http = rules1[0].RuleArn;
                let params = {
                    url: url,
                    channelName: req.body.channelName ? req.body.channelName : null,
                    creator: req.body.creator ? req.body.creator : null,
                    lookaheadFragments: req.body.lookaheadFragments ? req.body.lookaheadFragments : null,
                    dvrWindowLength: req.body.dvrWindowLength ? req.body.dvrWindowLength : null,
                    archiveSegmentLength: req.body.archiveSegmentLength ? req.body.archiveSegmentLength : null,
                    archiving: req.body.archiving ? req.body.archiving : null,
                    archiveLength: req.body.archiveLength ? req.body.archiveLength : null,
                    restartOnEncoderReconnect: req.body.restartOnEncoderReconnect ? req.body.restartOnEncoderReconnect : null,
                    issMinimumFragmentLength: req.body.issMinimumFragmentLength ? req.body.issMinimumFragmentLength : null,
                    hlsMinimumFragmentLength: req.body.hlsMinimumFragmentLength ? req.body.hlsMinimumFragmentLength : null,
                    backupserverIp: req.body.backupserverIp ? req.body.backupserverIp : null,
                    backupserverName: req.body.backupserverName ? req.body.backupserverName : null,
                    terminationPolicy: req.body.terminationPolicy,
                    // RuleArn_https: RuleArn_http,
                    hlsNoAudioOnly: req.body.hlsNoAudioOnly,
                    // RuleArn_http: RuleArn,
                    state: req.body.state ? req.body.state : null

                }

                backupchannelObj.save(params).then((_response) => {
                    if (_response) {

                        res.json({
                            status: 200,
                            message: 'channel Saved successfuly',
                            data: _response,
                        })
                    }
                    else {
                        console.log("Operation Failed")
                    }
                    console.log("body >> " + body);
                    console.log(error);
                });

                // }
                // });
                // }
                // });
            }
        });
})







//Primary Channel Delete On Server Then MongoDB
router.post('/channel-delete', (req, res, next) => {
    const params = {
        channelName: req.body.channelName,
    }
    var channelName = req.body.channelName;
    var serverIp = req.body.serverip;
    var RuleArn_https = req.body.RuleArn_https
    var RuleArn_http = req.body.RuleArn_http

    request.delete({
        url: `http://${serverIp}:3001/${channelName}/${channelName}.isml`,
        method: "DELETE",
    },
        function (error, response, body) {
            if (response.statusCode === 200) {
                // For http
                var delete_params_http = {
                    FunctionName: 'QA-USP-ALB-Remove-Rule', /* required */
                    Payload: JSON.stringify({ "rule-arn": RuleArn_http }),

                };

                lambda.invoke(delete_params_http, function (err, data) {
                    if (err) {
                        console.log(err, err.stack); // an error occurred
                    }
                    else {
                        // For https 
                        var delete_params_https = {
                            FunctionName: 'QA-USP-ALB-Remove-Rule', /* required */
                            Payload: JSON.stringify({ "rule-arn": RuleArn_https }),
                        };

                        lambda.invoke(delete_params_https, function (err, data) {
                            if (err) {
                                console.log(err, err.stack); // an error occurred
                            }
                            else {

                                channelObj.deleteChannel(params).then((data) => {
                                    res.json({
                                        status: 200,
                                        message: 'Delete-channel successfully',
                                        data: data,
                                    });

                                });

                            }
                        });
                    }
                });

            } else {
                console.log("Operation Failed")
            }
            console.log(body);
            console.log(error);
        });
    var sesMail = new awsSesMail();
    var sesConfig = {
        accessKeyId: 'AKIA2IYDDSCDRUKXHUT5',
        secretAccessKey: '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',
        region: 'ap-south-1'
    };

    sesMail.setConfig(sesConfig);

    var options = {
        from: 'pshinde@skandha.in',
        to: ['pvitonde@skandha.in'],
        subject: 'USP Primary channel delete',
        content: `${params.channelName} channel has been deleted`
    };

    sesMail.sendEmail(options, function (data) {
        console.log(data);
    });
})

//Primary Channel Delete On Server Then MongoDB
router.post('/backup-channel-delete', (req, res, next) => {
    const params = {
        channelName: req.body.channelName,
    }
    var channelName = req.body.channelName;
    var serverIp = req.body.serverip;
    var RuleArn_https = req.body.RuleArn_https
    var RuleArn_http = req.body.RuleArn_http

    request.delete({
        url: `http://${serverIp}:3001/${channelName}/${channelName}.isml`,
        method: "DELETE",
    },
        function (error, response, body) {
            if (response.statusCode === 200) {
                // For http
                var delete_params_http = {
                    FunctionName: 'QA-USP-ALB-Remove-Rule', /* required */
                    Payload: JSON.stringify({ "rule-arn": RuleArn_http }),

                };

                lambda.invoke(delete_params_http, function (err, data) {
                    if (err) {
                        console.log(err, err.stack); // an error occurred
                    }
                    else {

                        // For https 
                        var delete_params_https = {
                            FunctionName: 'QA-USP-ALB-Remove-Rule', /* required */
                            Payload: JSON.stringify({ "rule-arn": RuleArn_https }),
                        };

                        lambda.invoke(delete_params_https, function (err, data) {
                            if (err) {
                                console.log(err, err.stack); // an error occurred
                            }
                            else {
                                //  console.log(data);
                                backupchannelObj.deleteChannel(params).then((data) => {
                                    res.json({
                                        status: 200,
                                        message: 'Delete-channel successfully',
                                        data: data,
                                    });


                                });
                            }
                        });
                    }
                })



            } else {
                console.log("Operation Failed")
            }
            console.log(body);
            console.log(error);
        });
    var sesMail = new awsSesMail();
    var sesConfig = {
        accessKeyId: 'AKIA2IYDDSCDRUKXHUT5',
        secretAccessKey: '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',
        region: 'ap-south-1'
    };

    sesMail.setConfig(sesConfig);

    var options = {
        from: 'pshinde@skandha.in',
        to: ['pvitonde@skandha.in'],
        subject: 'USP Backup channel delete',
        content: `${params.channelName} channel has been deleted`
    };

    sesMail.sendEmail(options, function (data) {
        console.log(data);
    });
})

// State Update In MongoDb
router.post('/updateStateInMongoDb', (req, res, next) => {

    const params = {
        xmlFileName: req.body.channel_Name ? req.body.channel_Name : null,
        state: req.body._state ? req.body._state : null,
    }
    channelObj.findOneAndUpdate(params).then((_response) => {
        if (_response) {

            res.json({
                status: 200,
                message: 'Updated channel Saved successfuly',
                data: _response,
            });
            var sesMail = new awsSesMail();
            var sesConfig = {
                accessKeyId: 'AKIA2IYDDSCDRUKXHUT5',
                secretAccessKey: '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',
                region: 'ap-south-1'
            };

            sesMail.setConfig(sesConfig);

            var options = {
                from: 'pshinde@skandha.in',
                to: ['pvitonde@skandha.in', 'noc@skandha.in'],
                subject: 'USP Primary channel update state',
                content: `${params.xmlFileName} has been updated & state is ${params.state}`
            };

            sesMail.sendEmail(options, function (data) {
                console.log(data);
            });
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'Updated channel Save failed',
            data: null,
        });
    });

})

// State Update In MongoDb
router.post('/updateBackupStateInMongoDb', (req, res, next) => {

    const params = {
        xmlFileName: req.body.channel_Name ? req.body.channel_Name : null,
        state: req.body._state ? req.body._state : null,
    }
    backupchannelObj.findOneAndUpdate(params).then((_response) => {
        if (_response) {
            res.json({
                status: 200,
                message: 'Updated channel Saved successfuly',
                data: _response,
            });

            var sesMail = new awsSesMail();
            var sesConfig = {
                accessKeyId: 'AKIA2IYDDSCDRUKXHUT5',
                secretAccessKey: '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',
                region: 'ap-south-1'
            };

            sesMail.setConfig(sesConfig);

            var options = {
                from: 'pshinde@skandha.in',
                to: ['pvitonde@skandha.in', 'noc@skandha.in'],
                subject: 'USP Backup channel update state',
                content: `${params.xmlFileName} has been updated & state is ${params.state}`
            };

            sesMail.sendEmail(options, function (data) {
                console.log(data);
            });

        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'Updated channel Save failed',
            data: null,
        });
    });

})


// Get  Primary Channel Data from MongoDb 
router.post('/update-channel', (req, res, next) => {

    const params = {
        channelName: req.body.channelName
    };

    channelObj.findDataBychannel(params).then((data) => {
        res.json({
            status: 200,
            message: 'Metadata Loaded successfully',
            data: data,
        });
    });

})

// Get Specifice Primary Channel Data from MongoDb 
router.post('/update-backup-channel', (req, res, next) => {
    const params = {
        channelName: req.body.channelName
    };
    backupchannelObj.findDataBychannel(params).then((data) => {
        res.json({
            status: 200,
            message: 'Metadata Loaded successfully',
            data: data,
        });
    });

})

// Get Channel Listing By IP From MongoDb
router.post('/channel-listing-byIp', (req, res, next) => {
    const params = {
        ServerIp: req.body.ServerIp
    };
    channelObj.findDataByIp(params).then((data) => {
        res.json({
            status: 200,
            message: 'channel listing by ip fetched successfully',
            data: data,
        });
    });
})

// Get All Backup Channel Data from MongoDb
router.get('/channel_name', (req, res, next) => {

    backupchannelObj.findall().then((data) => {
        if (data) {
            res.json({
                status: 200,
                message: 'channels fetched successfully',
                data: data,
            })
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'channels fetched failed',
            data: null,
        });
    });

})


// Get Backup Channel from MongoDb 
router.post('/find-backup-channel', (req, res, next) => {

    const params = {
        channelName: req.body.channelName
    };
    backupchannelObj.findDataBychannel(params).then((data) => {
        res.json({
            status: 200,
            message: 'find channel data successfully',
            data: data,
        });
    });

})


// Update Channel On Server Then MongoDB
router.post('/update-channel-params', (req, res, next) => {

    var xmlUpdatedData = req.body.xmlUpdatedData;
    var xmlFileName = req.body.xmlFileName;
    var UpdatedchannelName = req.body.UpdatedchannelName;
    var UpdatedServerIp = req.body.UpdatedServerIp;
    request.put({
        url: `http://${UpdatedServerIp}:3001/${UpdatedchannelName}/${xmlFileName}.isml`,
        method: "PUT",
        headers: {
            'Content-Type': 'application/xml',
        },
        body: xmlUpdatedData,
    },
        function (error, response, body) {

            if (response.statusCode === 200) {

                const params = {

                    xmlFileName: req.body.xmlFileName ? req.body.xmlFileName : null,
                    creator: req.body.Updatedcreator ? req.body.Updatedcreator : null,
                    lookaheadFragments: req.body.UpdatedlookaheadFragments ? req.body.UpdatedlookaheadFragments : null,
                    dvrWindowLength: req.body.UpdateddvrWindowLength ? req.body.UpdateddvrWindowLength : null,
                    archiveSegmentLength: req.body.UpdatedarchiveSegmentLength ? req.body.UpdatedarchiveSegmentLength : null,
                    archiving: req.body.Updatedarchiving ? req.body.Updatedarchiving : null,
                    archiveLength: req.body.UpdatedarchiveLength ? req.body.UpdatedarchiveLength : null,
                    restartOnEncoderReconnect: req.body.UpdatedrestartOnEncoderReconnect ? req.body.UpdatedrestartOnEncoderReconnect : null,
                    issMinimumFragmentLength: req.body.UpdatedissMinimumFragmentLength ? req.body.UpdatedissMinimumFragmentLength : null,
                    hlsMinimumFragmentLength: req.body.UpdatedhlsMinimumFragmentLength ? req.body.UpdatedhlsMinimumFragmentLength : null,
                    terminationPolicy: req.body.updateterminationPolicy ? req.body.updateterminationPolicy : null,
                    hlsNoAudioOnly: req.body.UpdatedhlsNoAudioOnly ? req.body.UpdatedhlsNoAudioOnly : null

                }

                channelObj.findOneAndUpdate(params).then((_response) => {
                    if (_response) {
                        res.json({
                            status: 200,
                            message: 'Updated channel Saved successfuly',
                            data: _response,
                        })
                    }
                }).catch((err) => {
                    console.log(`error: ${err}`);
                    res.json({
                        status: 401,
                        message: 'Updated channel Save failed',
                        data: null,
                    });
                });
            }
            // console.log(response.statusCode);
            console.log(body);
            console.log(error);
        });

})

// Update Channel On Server Then MongoDB
router.post('/primaryupdateonmongo', (req, res, next) => {

    const params = {

        xmlFileName: req.body.xmlFileName ? req.body.xmlFileName : null,
        flag: req.body._flag ? req.body._flag : null,
    }

    channelObj.findOneAndUpdate(params).then((_response) => {
        if (_response) {
            res.json({
                status: 200,
                message: 'Updated channel Saved successfuly',
                data: _response,
            })
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'Updated channel Save failed',
            data: null,
        });
    });


})


// Update Channel On Server Then MongoDB
router.post('/userAcitivity', (req, res, next) => {

    const _params = {
        channelName: req.body.createdChannelName ? req.body.createdChannelName : null,
        userName: req.session.userName,
        activity: req.body.activity ? req.body.activity : null,
        ip_address: req.body.ip_address ? req.body.ip_address : null,
    }

    userActivityObj.save(_params).then((_response) => {
        if (_response) {
            res.json({
                status: 200,
                message: 'User Activity Saved successfuly',
                data: _response,
            })
        }
    })

})


// Update channel On Backup Server ANd MongoDb
router.post('/update-backup-channel-params', (req, res, next) => {
    var xmlUpdatedData = req.body.xmlUpdatedData;
    var xmlFileName = req.body.xmlFileName;
    var UpdatedchannelName = req.body.UpdatedchannelName;
    var UpdatedBackupServerIp = req.body.UpdatedBackupServerIp

    request.put({
        url: `http://${UpdatedBackupServerIp}:3001/${UpdatedchannelName}/${xmlFileName}.isml`,
        method: "PUT",
        headers: {
            'Content-Type': 'application/xml',
        },
        body: xmlUpdatedData,
    },
        function (error, response, body) {

            if (response.statusCode === 200) {
                var params = {
                    xmlFileName: req.body.xmlFileName ? req.body.xmlFileName : null,
                    creator: req.body.Updatedcreator ? req.body.Updatedcreator : null,
                    lookaheadFragments: req.body.UpdatedlookaheadFragments ? req.body.UpdatedlookaheadFragments : null,
                    dvrWindowLength: req.body.UpdateddvrWindowLength ? req.body.UpdateddvrWindowLength : null,
                    archiveSegmentLength: req.body.UpdatedarchiveSegmentLength ? req.body.UpdatedarchiveSegmentLength : null,
                    archiving: req.body.Updatedarchiving ? req.body.Updatedarchiving : null,
                    archiveLength: req.body.UpdatedarchiveLength ? req.body.UpdatedarchiveLength : null,
                    restartOnEncoderReconnect: req.body.UpdatedrestartOnEncoderReconnect ? req.body.UpdatedrestartOnEncoderReconnect : null,
                    issMinimumFragmentLength: req.body.UpdatedissMinimumFragmentLength ? req.body.UpdatedissMinimumFragmentLength : null,
                    hlsMinimumFragmentLength: req.body.UpdatedhlsMinimumFragmentLength ? req.body.UpdatedhlsMinimumFragmentLength : null,
                    terminationPolicy: req.body.updateterminationPolicy ? req.body.updateterminationPolicy : null,
                    hlsNoAudioOnly: req.body.UpdatedhlsNoAudioOnly ? req.body.UpdatedhlsNoAudioOnly : null
                }

                backupchannelObj.findOneAndUpdate(params).then((_response) => {

                    if (_response) {
                        res.json({
                            status: 200,
                            message: 'Updated channel Saved successfuly',
                            data: _response,
                        })
                    }
                }).catch((err) => {
                    console.log(`error: ${err}`);
                    res.json({
                        status: 401,
                        message: 'Updated channel Save failed',
                        data: null,
                    });
                });
            }

            console.log(body);
            console.log(error);
        });

})


router.get('/channel_listing', (req, res, next) => {

    channelObj.findall().then((data) => {
        if (data) {

            res.json({
                status: 200,
                message: 'channels fetched successfully',
                data: data,
            })
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'channels fetched failed',
            data: null,
        });
    });

})


// Get Channel State Listing
router.post('/get_channel_state', (req, res, next) => {

    var channelName = req.body.channel_Name;
    var serverIp = req.body.serverip;
    request.get({
        url: `http://${serverIp}/${channelName}/${channelName}.isml/state`,
        method: "GET",
    },
        function (error, response, body) {

            if (!error && response.statusCode === 200) {

                var xml = response.body;

                parseString(xml, function (err, result) {

                    res.json({
                        status: 200,
                        message: 'channel get state successfuly',
                        data: result,
                    })
                })
            } else {
                res.json({
                    status: 401,
                    message: 'channel get state  failed',
                    data: null,
                })
            }
        }
    )
});


// Get Channel State Listing
router.post('/get_backup_state_of_channel', (req, res, next) => {

    var channelName = req.body.backup_channel_Name
    var serverIp = req.body.serverip

    request.get({
        url: `http://${serverIp}/${channelName}/${channelName}.isml/state`,
        method: "GET",
    },
        function (error, response, body) {

            if (!error && response.statusCode == 200) {
                var xml = response.body
                parseString(xml, function (err, result) {
                    res.json({
                        status: 200,
                        message: 'channel get state successfuly',
                        data: result,
                    })
                })
            } else {
                res.json({
                    status: 401,
                    message: 'channel get state  failed',
                    data: null,
                })
            }
        }
    )
});


// Channel Statistics From Server
router.post('/channel_statistics', (req, res, next) => {

    var channelName = req.body.channel_Name;
    var serverIp = req.body.serverip;

    request.get({
        url: `http://${serverIp}/${channelName}/${channelName}.isml/statistics`,
        method: "GET",
    },
        function (error, response, body) {
            if (response.statusCode === 200) {
                var xml = response.body
                parseString(xml, function (err, result) {
                    res.json({
                        status: 200,
                        message: 'channel get successfuly',
                        data: result,
                    })
                });
            } else {
                console.log('channel get failed' + error);
                res.json({
                    status: 401,
                    message: 'channel update Save failed',
                    data: null,
                })
            }
        }
    )
});


// Channel Statistics From Server
router.post('/backup_channel_statistics', (req, res, next) => {

    var channelName = req.body.backup_channel_Name;
    var serverIp = req.body.serverip;
    request.get({
        url: `http://${serverIp}/${channelName}/${channelName}.isml/statistics`,
        method: "GET",
    },
        function (error, response, body) {
            if (response.statusCode === 200) {
                var xml = response.body
                parseString(xml, function (err, result) {
                    res.json({
                        status: 200,
                        message: 'channel get successfuly',
                        data: result,
                    })
                });
            } else {
                console.log('channel get failed' + error);
                res.json({
                    status: 401,
                    message: 'channel update Save failed',
                    data: null,
                })
            }
        }
    )
});


// Get Channel Listing By Termination Policy From MongoDb
router.post('/channel-Listing_by_TerminationPolicy', (req, res, next) => {
    const params = {
        Termination_Policy: req.body.Termination_Policy
    };
    channelObj.findDataByTerminationPolicy(params).then((data) => {
        res.json({
            status: 200,
            message: 'channel listing by findDataByTerminationPolicy fetched successfully',
            data: data,
        });
    });
})


router.post('/whatsapp', (req, res, next) => {

    var arr = req.body.number ? req.body.number : null
    var channelName = req.body.channelName ? req.body.channelName : null;
    var description = req.body.description ? req.body.description : null;
    var message = {
        "channel": "whatsapp",
        "source": "+917045699465",
        "destination": [
            arr
        ],
        "content": {
            "text": `${channelName} ${description}`
        },
        "events_url": "https://events.example.com/message"
    }
    var meg = JSON.stringify(message)
    console.log(" \n \n meg  >>" + JSON.stringify(meg))
    request.post({
        url: `https://api.karix.io/message/`,
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + new Buffer(username + ':' + passw).toString('base64')
        },
        body: meg
    },
        function (error, response, body) {
            //console.log("\n \n response"+JSON.stringify(response))
            if (response) {
                res.json({
                    status: 200,
                    message: 'Notification send successfully',
                    data: response,
                });
            }
            // console.log("\nbody >>> "+JSON.stringify(body))
            console.log(error)
        })

});

router.post('/SlackMsg', (req, res, next) => {
    var channelName = req.body.channelName ? req.body.channelName : null;
    var description = req.body.description ? req.body.description : null;

    const slackToken = awsconfig.development.Slack.slackToken;
    run().catch(err => console.log(err));

    async function run() {
        const url = 'https://slack.com/api/chat.postMessage';
        const response = await axios.post(url, {
            channel: '#project_notification',
            text: `${channelName}${description}`
        }, { headers: { authorization: `Bearer ${slackToken}` } });
        console.log('Done', response.data);
    }
})

module.exports = router;
