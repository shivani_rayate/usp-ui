var express = require('express');
var router = express.Router();

const AWS = require("aws-sdk");
const request = require('request');


const route53 = require('../app/utilities/route53');

router.get('/', function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('route53', { userData: userData, userName: userName, userRole: userRole });

});



/* GET listResourceRecordSets . */
router.get('/listResourceRecordSets', function (req, res, next) {

  console.log("\n \n \n in router of listResourceRecordSets")
   
     let params
   
     route53.listResourceRecordSets(params).then((response) => {
   
       //RESPONSE
       if (response) {
   
       //  console.log("listInputs" + JSON.stringify(response))
   
         res.json({
           status: 200,
           message: "listResourceRecordSets",
           data: response
         })
       } else {
         res.json({
           status: 400,
           message: "listResourceRecordSets fetched failed",
           data: null
         })
       }
     });
   });






   /* POST changeResourceRecordSets . */
router.post('/changeResourceRecordSets', function (req, res, next) {

  console.log("\n \n \n in router of changeResourceRecordSets")
   
     var params = {

      weight : req.body.weight,
      SetIdentifier : req.body.SetIdentifier,
      ResourceRecordValue : req.body.ResourceRecordValue,
      Type : req.body.Type,
      name : req.body.name,
      TTL : req.body.TTL
     }
   
     route53.changeResourceRecordSets(params).then((response) => {
   
       //RESPONSE
       if (response) {
   
       //  console.log("listInputs" + JSON.stringify(response))
   
         res.json({
           status: 200,
           message: "changeResourceRecordSets",
           data: response
         })
       } else {
         res.json({
           status: 400,
           message: "changeResourceRecordSets fetched failed",
           data: null
         })
       }
     });
   });




module.exports = router;