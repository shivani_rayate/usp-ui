var express = require('express');
var router = express.Router();

const ntp = require('ntp2');

const AWS = require("aws-sdk");

const ses = new AWS.SES();
var cron = require('node-cron');
const Channel = require("../workers/channels");
const channelObj = new Channel.ChannelClass();

router.get('/', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('index',  {userData: userData, userName: userName, userRole: userRole });
});


router.get('/', function (req, res, next) {
    res.render('transcoder', {});
});



/* GET instance listing. */
router.get('/instance_listing', function (req, res, next) {

    let instance = require('../app/utilities/instance');
    
    let params
   params = {
      Filters: [
          {
              Name: "tag:Identifier",
              Values: ['USP'],
          }
      ]
  };
    instance.instanceListing(params).then((response) => {
  
      //RESPONSE
      if (response) {
        res.json({
          status: 200,
          message: "instances fetched successfully",
          data: response
        })
      } else {
        res.json({
          status: 400,
          message: "instances fetched failed",
          data: null
        })
      }
    });
  });
  

/* GET instance by primary Tag */
router.get('/instance_listing_serevr', function (req, res, next) {
  let instance = require('../app/utilities/instance');
  
  // let params = {}
  let params
 params = {
    Filters: [
        {
            Name: "tag:primary",
            Values: ['primary']
          //  Values: ['USP Live - Primary','USP Apache Server BackUp']
           
           
        },
       
       
    ]
};
  instance.instanceListing(params).then((response) => {
    //RESPONSE
    if (response) {
      res.json({
        status: 200,
        message: "instances fetched successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "instances fetched failed",
        data: null
      })
    }
  });
});

  router.post('/channel-listing-byIp', (req, res, next) => {

    const params = {
        ServerIp: req.body.ServerIp
    };
    channelObj.findDataByIp(params).then((data) => {
        res.json({
            status: 200,
            message: 'channel listing by ip fetched successfully',
            data: data,
        });
    });
})


router.get('/instance_listing_serevr', function (req, res, next) {

  let instance = require('../app/utilities/instance');
  
  // let params = {}
  let params
 params = {
    Filters: [
        {
            Name: "tag:primary",
            Values: ['primary']
          //  Values: ['USP Live - Primary','USP Apache Server BackUp']
           
           
        },
       
       
    ]
};


  instance.instanceListing(params).then((response) => {

    //RESPONSE
    if (response) {


      res.json({
        status: 200,
        message: "instances fetched successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "instances fetched failed",
        data: null
      })
    }
  });
});

router.get('/server-listing-backup', function (req, res, next) {

  let instance = require('../app/utilities/instance');
  
  // let params = {}
  let params
 params = {
    Filters: [
        {
            Name: "tag:backup",
            Values: ['backup']
          //  Values: ['USP Live - Primary','USP Apache Server BackUp']
           
           
        }
       
    ]
};


  instance.instanceListing(params).then((response) => {

    //RESPONSE
    if (response) {

      res.json({
        status: 200,
        message: "instances fetched successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "instances fetched failed",
        data: null
      })
    }
  });
});

module.exports = router;
