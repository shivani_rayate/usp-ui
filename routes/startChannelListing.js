var express = require('express');
var router = express.Router();

const ntp = require('ntp2');

const AWS = require("aws-sdk");

const ses = new AWS.SES();
var cron = require('node-cron');
const Channel = require("../workers/channels");
const channelObj = new Channel.ChannelClass();
const request = require('request');

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const xhr = new XMLHttpRequest();
var utf8 = require('utf8');

var xml2js = require('xml2js');
var parser = new xml2js.Parser();

var parseString = require('xml2js').parseString;

router.get('/', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('startChannelListing',  {userData: userData, userName: userName, userRole: userRole });
});




router.get('/channel_listing', (req, res, next) => {

  channelObj.findall().then((data) => {
      if (data) {
          res.json({
              status: 200,
              message: 'channels fetched successfully',
              data: data,
          })
      }
  }).catch((err) => {
      console.log(`error: ${err}`);
      res.json({
          status: 401,
          message: 'channels fetched failed',
          data: null,
      });
  });

})


// Get Channel State Listing
router.post('/get_channel_state', (req, res, next) => {


  var channelName = req.body.channel_Name
  var serverIp = req.body.serverip

  request.get({
      url: `http://${serverIp}/${channelName}/${channelName}.isml/state`,
      method: "GET",
  },
      function (error, response, body) {

          if (!error && response.statusCode == 200) {
              var xml = response.body
              parseString(xml, function (err, result) {
                  res.json({
                      status: 200,
                      message: 'channel get state successfuly',
                      data: result,
                  })
              })
          } else {
  
              res.json({
                  status: 401,
                  message: 'channel get state  failed',
                  data: null,
              })
          }
      }
  )
});



module.exports = router;
