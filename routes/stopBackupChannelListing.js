var express = require('express');
var router = express.Router();

const ntp = require('ntp2');

const AWS = require("aws-sdk");

const ses = new AWS.SES();
var cron = require('node-cron');
const Channel = require("../workers/channels");
const channelObj = new Channel.ChannelClass();
const request = require('request');

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const xhr = new XMLHttpRequest();
var utf8 = require('utf8');

var xml2js = require('xml2js');
var parser = new xml2js.Parser();

var parseString = require('xml2js').parseString;

router.get('/', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('stopBackupChannelListing',  {userData: userData, userName: userName, userRole: userRole });
});

router.get('/startBackupChannelListing', function (req, res, next) {

    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
      res.render('startBackupChannelListing',  {userData: userData, userName: userName, userRole: userRole });
  });


 





module.exports = router;
