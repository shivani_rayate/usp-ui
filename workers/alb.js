const Alb = require('../app/models/alb_channel');


class AlbCls {
    AlbsCls() { }

    save(params) {
     
        const albDocument = new Alb({
            channelName: params.channelName ? params.channelName : null,
            
            RuleArn: params.RuleArn,

            TargetName:params.TargetName
         
        });
        
        return new Promise((resolve, reject) => {
            albDocument.save((err, albdata) => {
                if (err) {
                    console.error(`Error :: Mongo channel Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: albdata,
                    };
                    resolve(response);
                }
            });
        });
    }


    // Get All Data 
    findall() {
        return new Promise((resolve, reject) => {
            Alb.find({})
                .sort({ created_at: -1 })
                .exec((err, channel) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all channel has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(channel);
                    }
                });
        });
    }


    // Delete Channel
    deleteChannel(params) {
        return new Promise((resolve, reject) => {
            const obj = {};
            Alb.deleteOne({ RuleArn: params.RuleArn })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Channel SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }

      // Find Data By Ip
      findone(params) {
        return new Promise((resolve, reject) => {
            Alb.find({ channelName: params.channelName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }
}

module.exports = {
    AlbClass: AlbCls,
};