const backupChannel = require('../app/models/backupchannel');

class ChannelsCls {
    ChannelsCls() { }

    save(params) {

        const channelDocument = new backupChannel({
            channelName: params.channelName ? params.channelName : null,
            creator: params.creator ? params.creator : null,
            lookaheadFragments: params.lookaheadFragments ? params.lookaheadFragments : null,
            dvrWindowLength: params.dvrWindowLength ? params.dvrWindowLength : null,
            archiveSegmentLength: params.archiveSegmentLength ? params.archiveSegmentLength : null,
            archiving: params.archiving ? params.archiving : null,
            archiveLength: params.archiveLength ? params.archiveLength : null,
            restartOnEncoderReconnect: params.restartOnEncoderReconnect ? params.restartOnEncoderReconnect : null,
            issMinimumFragmentLength: params.issMinimumFragmentLength ? params.issMinimumFragmentLength : null,
            hlsMinimumFragmentLength: params.hlsMinimumFragmentLength ? params.hlsMinimumFragmentLength : null,
            url: params.url ? params.url : null,
            backupserverIp: params.backupserverIp ? params.backupserverIp : null,
            backupserverName: params.backupserverName ? params.backupserverName : null,
            terminationPolicy : params.terminationPolicy,
            RuleArn_http: params.RuleArn_http,
            RuleArn_https:params.RuleArn_https,
            hlsNoAudioOnly: params.hlsNoAudioOnly,
            state : params.state ? params.state : null

        });
        return new Promise((resolve, reject) => {
            channelDocument.save((err, channeldata) => {
                if (err) {
                    console.error(`Error :: Mongo channel Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: channeldata,
                    };
                    resolve(response);
                }
            });
        });
    }

    findDataBychannel(params) {
        return new Promise((resolve, reject) => {
            backupChannel.findOne({ channelName: params.channelName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

    findall() {
        return new Promise((resolve, reject) => {
            backupChannel.find({})
                .sort({ created_at: -1 })
                .exec((err, channel) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all channel has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(channel);
                    }
                });
        });
    }

    // Delete Channel
    deleteChannel(params) {
        return new Promise((resolve, reject) => {
            const obj = {};
            backupChannel.deleteOne({ channelName: params.channelName })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Channel SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }

    // Update Channel
    findOneAndUpdate(params) {
        return new Promise((resolve, reject) => {
            backupChannel.findOne({
                channelName: params.xmlFileName,
            },
                (err, channeldata) => {
                    if (err) {
                        console.error(`Error :: Mongo Save Error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {

                        if (params.channelName != null) {
                            channeldata.channelName = params.channelName;
                        }
                        if (params.creator != null) {
                            channeldata.creator = params.creator;
                        }
                        if (params.lookaheadFragments != null) {
                            channeldata.lookaheadFragments = params.lookaheadFragments;
                        }

                        if (params.dvrWindowLength != null) {
                            channeldata.dvrWindowLength = params.dvrWindowLength;
                        }

                        if (params.archiveSegmentLength != null) {
                            channeldata.archiveSegmentLength = params.archiveSegmentLength;
                        }

                        if (params.archiving != null) {
                            channeldata.archiving = params.archiving;
                        }

                        if (params.archiveLength != null) {
                            channeldata.archiveLength = params.archiveLength;
                        }
                        if (params.issMinimumFragmentLength != null) {
                            channeldata.issMinimumFragmentLength = params.issMinimumFragmentLength;
                        }
                        if (params.hlsMinimumFragmentLength != null) {
                            channeldata.hlsMinimumFragmentLength = params.hlsMinimumFragmentLength;
                        }
                        if (params.hlsNoAudioOnly != null) {
                            channeldata.hlsNoAudioOnly = params.hlsNoAudioOnly;
                        }
                        if (params.restartOnEncoderReconnect != null) {
                            channeldata.restartOnEncoderReconnect = params.restartOnEncoderReconnect;
                        }
                        if (params.terminationPolicy != null) {
                            channeldata.terminationPolicy = params.terminationPolicy;
                        }
                        if (params.state != null) {
                            channeldata.state = params.state;
                        }
                        // if (params.flag != null) {
                        //     channeldata.flag = params.flag;
                        // }
                        channeldata.save((errUpdate, response) => {
                            if (errUpdate) {
                                console.log(`ERR IN UPDATE> ${JSON.stringify(errUpdate)}`);
                                reject(errUpdate);
                            } else {
                                resolve(response);
                            }
                        });
                    }

                });

        });
    }


}

module.exports = {
    BackupChannelClass: ChannelsCls,
};