const Channel = require('../app/models/create_channel');


class ChannelsCls {
    ChannelsCls() { }

    save(params) {
        const channelDocument = new Channel({
            channelName: params.channelName ? params.channelName : null,
            creator: params.creator ? params.creator : null,
            lookaheadFragments: params.lookaheadFragments ? params.lookaheadFragments : null,
            dvrWindowLength: params.dvrWindowLength ? params.dvrWindowLength : null,
            archiveSegmentLength: params.archiveSegmentLength ? params.archiveSegmentLength : null,
            archiving: params.archiving ? params.archiving : null,
            archiveLength: params.archiveLength ? params.archiveLength : null,
            restartOnEncoderReconnect: params.restartOnEncoderReconnect ? params.restartOnEncoderReconnect : null,
            issMinimumFragmentLength: params.issMinimumFragmentLength ? params.issMinimumFragmentLength : null,
            hlsMinimumFragmentLength: params.hlsMinimumFragmentLength ? params.hlsMinimumFragmentLength : null,
            url: params.url ? params.url : null,
            ServerIp: params.ServerIp ? params.ServerIp : null,
            serverName: params.serverName ? params.serverName : null,
            flag: params.flag,
            state : params.state ? params.state : null,
            terminationPolicy : params.terminationPolicy,
            RuleArn_http: params.RuleArn_http,
            hlsNoAudioOnly:params.hlsNoAudioOnly,
            RuleArn_https:params.RuleArn_https

        });
        return new Promise((resolve, reject) => {
            channelDocument.save((err, channeldata) => {
                if (err) {
                    console.error(`Error :: Mongo channel Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: channeldata,
                    };
                    resolve(response);
                }
            });
        });
    }


    // Get All Data 
    findall() {
        return new Promise((resolve, reject) => {
            Channel.find({})
                .sort({ created_at: -1 })
                .exec((err, channel) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all channel has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(channel);
                    }
                });
        });
    }


    // Find Channel By Channel Name
    findDataBychannel(params) {
        return new Promise((resolve, reject) => {
            Channel.findOne({ channelName: params.channelName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }


    // Find Data By Ip
    findDataByIp(params) {
        return new Promise((resolve, reject) => {
            Channel.find({ ServerIp: params.ServerIp })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }



     // Find Data By Termination Policy
     findDataByTerminationPolicy(params) {
        return new Promise((resolve, reject) => {
            Channel.find({ terminationPolicy: params.Termination_Policy })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }


    // Delete Channel
    deleteChannel(params) {
        return new Promise((resolve, reject) => {
            const obj = {};
            Channel.deleteOne({ channelName: params.channelName })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Channel SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }


    // Update Channel
    findOneAndUpdate(params) {
        return new Promise((resolve, reject) => {
            Channel.findOne({
                channelName: params.xmlFileName,
            },
                (err, channeldata) => {
                    if (err) {
                        console.error(`Error :: Mongo Save Error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {

                        if (params.channelName != null) {
                            channeldata.channelName = params.channelName;
                        }
                        if (params.creator != null) {
                            channeldata.creator = params.creator;
                        }
                        if (params.lookaheadFragments != null) {
                            channeldata.lookaheadFragments = params.lookaheadFragments;
                        }

                        if (params.dvrWindowLength != null) {
                            channeldata.dvrWindowLength = params.dvrWindowLength;
                        }

                        if (params.archiveSegmentLength != null) {
                            channeldata.archiveSegmentLength = params.archiveSegmentLength;
                        }

                        if (params.archiving != null) {
                            channeldata.archiving = params.archiving;
                        }

                        if (params.archiveLength != null) {
                            channeldata.archiveLength = params.archiveLength;
                        }
                        if (params.issMinimumFragmentLength != null) {
                            channeldata.issMinimumFragmentLength = params.issMinimumFragmentLength;
                        }
                        if (params.hlsMinimumFragmentLength != null) {
                            channeldata.hlsMinimumFragmentLength = params.hlsMinimumFragmentLength;
                        }
                        if (params.hlsNoAudioOnly != null) {
                            channeldata.hlsNoAudioOnly = params.hlsNoAudioOnly;
                        }
                        if (params.restartOnEncoderReconnect != null) {
                            channeldata.restartOnEncoderReconnect = params.restartOnEncoderReconnect;
                        }
                        if (params.flag != null) {
                            channeldata.flag = params.flag;
                        }
                        if (params.terminationPolicy != null) {
                            channeldata.terminationPolicy = params.terminationPolicy;
                        }
                        if (params.state != null) {
                            channeldata.state = params.state;
                        }
                        channeldata.save((errUpdate, response) => {
                            if (errUpdate) {
                                console.log(`ERR IN UPDATE> ${JSON.stringify(errUpdate)}`);
                                reject(errUpdate);
                            } else {
                                resolve(response);
                            }
                        });
                    }

                });

        });
    }

    findExictingALBAndUpdate(params) {
        return new Promise((resolve, reject) => {
            Channel.findOne({
                channelName: params.xmlFileName,
            },
                (err, channeldata) => {
                    if (err) {
                        console.error(`Error :: Mongo Save Error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {

                        if (params.RuleArn_http != null) {
                            channeldata.RuleArn_http = params.RuleArn_http;
                        }

                        if (params.RuleArn_https != null) {
                            channeldata.RuleArn_https = params.RuleArn_https;
                        }
                        
                       
                        channeldata.save((errUpdate, response) => {
                            if (errUpdate) {
                                console.log(`ERR IN UPDATE> ${JSON.stringify(errUpdate)}`);
                                reject(errUpdate);
                            } else {
                                resolve(response);
                            }
                        });
                    }

                });

        });
    }

}

module.exports = {
    ChannelClass: ChannelsCls,
};