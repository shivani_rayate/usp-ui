const userActivity = require('../app/models/userActivity');


class userActivityCls {
userActivityCls() { }

save(_params) {
    const userActivityDocument = new userActivity({
        channelName: _params.channelName ? _params.channelName : null,
        userName: _params.userName ? _params.userName : null,
        activity: _params.activity ? _params.activity : null,
        ip_address: _params.ip_address ? _params.ip_address : null,
    });
    return new Promise((resolve, reject) => {
        userActivityDocument.save((err, channeldata) => {
            if (err) {
                console.error(`Error :: Mongo channel Save Error :: ${JSON.stringify(err)}`);
                reject(err);
            } else {
                const response = {
                    code: 200,
                    message: channeldata,
                };
                resolve(response);
            }
        });
    });
}

}


module.exports = {
    userActivityClass: userActivityCls,
};